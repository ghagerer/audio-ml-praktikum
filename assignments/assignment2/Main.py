import BerlinData
import SemaineData
import FauData
import SpeakerData
import Configuration
import iVector
import bob
import UBMData
import numpy as np
from Configuration import Config
import iVector2
import sys

type='ssh'


Configuration.setConfiguration(type,'UBM') # local or ssh
# UBMData.processAudioDataset()
# UBMData.createChanneledLearningDataset()
[ubmsamples,ubmnames]=UBMData.readData()


Configuration.setConfiguration(type,'Speaker') # local or ssh, Berlin, Fau or Semaine or Speakers
# SpeakerData.processAudioDataset()
# SpeakerData.createCleanLearningDataset()
# SpeakerData.createChanneledLearningDataset()
[cleannames, cleansamples,cleanannos]=SpeakerData.readClean()
[speakers,files,samples, data_vad]=SpeakerData.readChanneled()
print set(cleanannos), set(speakers), len(samples)

# Configuration.setConfiguration(type,'Berlin') # local or ssh, Berlin, Fau or Semaine
# BerlinData.processAudioDataset()
# BerlinData.createCleanLearningDataset()
# BerlinData.createChanneledLearningDataset()
# [cleannames, cleansamples,cleanannos]=BerlinData.readClean()
# [channelednames, channeledsamples,channeledannos]=BerlinData.readChanneled()
# print set(cleanannos), set(channeledannos), len(cleansamples)
#
#
# Configuration.setConfiguration(type,'Semaine') # local or ssh
# SemaineData.processAudioDataset()
# SemaineData.createCleanLearningDataset()
# SemaineData.createChanneledLearningDataset()
# [cleannames, cleansamples,cleanannos]=SemaineData.readClean()
# [channelednames, channeledsamples,channeledannos]=SemaineData.readChanneled()
# cleanannos= cleanannos[:,2]
# channeledannos= channeledannos[:,2]
# print set(cleanannos), set(channeledannos), len(cleansamples)
#
#
# Configuration.setConfiguration(type,'Fau') # local or ssh
# FauData.processAudioDataset()
# FauData.createCleanLearningDataset()
# FauData.createChanneledLearningDataset()
# [cleannames, cleansamples,cleanannos]=FauData.readClean()
# [channelednames, channeledsamples,channeledannos]=FauData.readChanneled()
# print set(cleanannos), set(channeledannos), len(cleansamples)


endtrain= 0.75*len(samples)
ubmsamples=ubmsamples[:,:Config.num_features]#:sizetrain
ubmannos=np.random.randint(10, size= len(ubmsamples))
ubmvadannos = np.random.randint(2, size= len(ubmsamples))
trainsamples=samples[:endtrain,:Config.num_features]
trainanonos=speakers[:endtrain]
tranvadannos = data_vad[:endtrain]
testsamples=samples[endtrain+1:,:Config.num_features]
testanonos=speakers[endtrain+1:]
testvadannos = data_vad[endtrain+1:]
testfiles =files[endtrain+1:]

# ubm =iVector2.ubm(ubmsamples)
# ubm = iVector2.ubm_cuda(ubmsamples)
ubm = bob.machine.GMMMachine(bob.io.HDF5File(Config.model_dir+'ubm_'+str(Config.num_mixtures_ubm)+'_htc.hdf5', 'r'))
print ubm
ubm_models = iVector2.train_by_category(ubm, ubmsamples,ubmannos,ubmvadannos)
ivec_machine, whitening_machine, lda_machine, wccn_machine = iVector2.init_machines(ubm, ubm_models)
# ivec_machine = bob.machine.IVectorMachine(bob.io.HDF5File(Config.model_dir+'ivec_'+str(Config.num_mixtures_ubm)+'.hdf5', 'r'))
# whitening_machine  = bob.machine.LinearMachine(bob.io.HDF5File(Config.model_dir+'white_'+str(Config.num_mixtures_ubm)+'.hdf5', 'r'))
# lda_machine  = bob.machine.LinearMachine(bob.io.HDF5File(Config.model_dir+'lda_'+str(Config.num_mixtures_ubm)+'.hdf5', 'r'))
# wccn_machine  = bob.machine.LinearMachine(bob.io.HDF5File(Config.model_dir+'wccn_'+str(Config.num_mixtures_ubm)+'.hdf5', 'r'))

emotion_models = iVector2.train_by_category(ubm, trainsamples,trainanonos,tranvadannos)
emotion_ivecs = iVector2.ivecs_from_stats(emotion_models,ivec_machine, whitening_machine)
emotion_ldas = iVector2.ldas_from_ivecs(emotion_ivecs,lda_machine)
emotion_wccns = iVector2.wccn_from_lda(emotion_ivecs,wccn_machine)
average_training_data_by_emotion = iVector2.average_by_emotion(emotion_wccns)


testsamples =iVector2.mixtests(testsamples,testfiles)
test_sample_windows, test_label_windows = iVector2.mixed_windows_for_test(testsamples,testanonos, testvadannos)
impostor = iVector2.wccn_from_testsamples(ubmsamples,ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine)

confusionmatrix=np.asmatrix([[0,0],[0,0]])
for i in range(len(test_sample_windows)):
    test_wccn = iVector2.wccn_from_testsamples(test_sample_windows[i],ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine)
    confusion_i = iVector2.test_wccn_on_models_against_impostor (test_wccn, test_label_windows[i],impostor,average_training_data_by_emotion)
    confusionmatrix+=confusion_i
print confusionmatrix

confusionmatrix=np.asmatrix([[0,0],[0,0]])
ubmcuda =   np.load(Config.model_dir+'cudaubm'+'.npz')
emotiongmms= iVector2.train_emotion_gmms_cuda(ubmcuda, cleansamples,cleanannos,'Test')#Config.set)
emogmms = iVector.read_emotion_gmms(Config.model_dir,'Test')
for i in range(len(test_sample_windows)):
    confusion_i = iVector2.test_gmm_on_models_against_impostor (test_sample_windows[i], test_label_windows[i],ubm,emotiongmms)
    confusionmatrix+=confusion_i
print confusionmatrix




# mit nvidia-smi checken ob jemand anderes rechnet

# Split Data per emotion
# Create 4 75 Train/ 25 Test splits
# Convolve Train and Test according to impulse responses (von naboo)
# Train und Test sets auf Alderaan speichern
# Measure Unweighted Average Recall (UAR)
# Comoute models on ssh and save them.
# CMN UBM testen ob noetig/ in Code schauen
# Windowing auch bei trainngsdaten und CMN short time dazu
# Schritt fuer schritt windows in gmmmap und ivect, dann ivectoren averagen (in code)
# windowing/averaging in einigen files andere nicht

# SPATER:
# Semaine nach emotion chunks spliten und in wavs specihern je 1 ordner pro emotion
# Dateiname von Semaine in Emotionodnern sollte speaker ids und emotion und session ID
#  Berlin und Fau in Ordner pro eemotion, und aber nciht umbenenne
# Features wie relative lautstaerke und pitch und energy aus mfccs
# Similarity swischen gleichen gmms (z.B. neutral) in berlin und semaine
# Tuerkische Daten mit englischen Modellen


# DONE:
# Integrate Aihams changes
# impulse responses (von naboo)
# Config: ubm path, ubm dimensions,
# Change: Convolve to scikit_fft
# Calculate ubm from timit buckeye gmm dimensions 265-512
# Create MFCCS and split into windows of 256 frames
# Use CMN for each window, save per emotion
# Create one map adapted gmm per emotion (acc_stats)
# Create Projection space for each ivector (with map gmm data)
# Create iVectors for each map adapted gmm (with map gmm data)
# Whiten and normalize ivec
# Create lda projection und vector
# Create wccn projection und vector
# For eacht test window:
# Create map adaption, Projection spaces and vectors
# cosine similarity: zwischen 5 ivecs und test ivec, choose larges similarity