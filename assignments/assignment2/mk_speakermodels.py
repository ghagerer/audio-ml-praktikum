from features import mfcc
import scipy.io.wavfile as wav
import audiotools
import math
import random
import pydub
import os
from sklearn import svm
from sklearn.metrics import confusion_matrix
from sklearn import cross_validation
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.mixture import GMM
import numpy as np
import math
import time
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import threading
import multiprocessing
from sklearn.tree import DecisionTreeClassifier
import itertools
import pickle
import logging
import csv
import joblib
import argparse

logging.basicConfig(filename=os.getcwd()+'/debug.log', level=logging.DEBUG, format='%(asctime)s %(message)s')


'''Script for generating the speaker models'''

# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('--gmm-components', nargs=1)
args = parser.parse_args()
gmm_settings = {}
gmm_settings['components'] = 10
if args.gmm_components is not None:
    gmm_settings['components'] = args.gmm_components[0]
print "GMM settings: "
print gmm_settings

print "Opening speaker data"
with open('speakers.pkl', 'rb') as handle:
    speakers = pickle.load(handle)

print "Instantiate models"
models = {}
for s in speakers:
    models[s] = GMM(n_components=gmm_settings['components'])

print "Preparing data"
train_data = {}
test_data = {}

for s in speakers:
    # first sort out parts without speech
    all_features = 0
    for file in speakers[s].itervalues():
        idx = [i for i,val in enumerate(file['anos']) if val==1]
        all_features += len(idx)
    
    # sort files randomly
    filenames = speakers[s].keys()
    random.shuffle(filenames)
    tmp = {}
    for f in filenames:
        tmp[f] = speakers[s][f]
    speakers[s] = tmp
    
    # now divide data in training and test data
    proportion = 0
    train_mfccs = []
    test_mfccs = {}
    for filename,file in speakers[s].iteritems():
        idx = [i for i,val in enumerate(file['anos']) if val==1]
        # don't take all features for training, leave some for testing
        if float(proportion+len(idx))/all_features < 0.75:
            proportion += len(idx)
            # use for train data
            train_mfccs += file['mfccs'][idx].tolist()
        else:
            # add to test data
            test_mfccs[filename] = {'mfccs': file['mfccs'], 'anos': file['anos']}
            
    test_data[s] = test_mfccs
    train_data[s] = np.asarray(train_mfccs)
    #print "Training proportion: "+str(float(proportion)/all_features)


print "Starting training"
def train(m):
    return m,models[m].fit(train_data[m])
models = dict(ProcessPoolExecutor().map(train, models.keys()))


print "Writing speaker models"
with open('speaker_models.pkl', 'wb') as handle:
    pickle.dump(models,handle)

'''
print 'Reading speaker models'
with open('speaker_models.pkl', 'rb') as handle:
    models = pickle.load(handle)
'''
# now test/evaluate the speaker models against UBM
print "Reading UBM"
ubm = joblib.load('ubm.pkl')


def speaker_eval(speaker, gmm, ubm, window_size=256):
    """Subroutine to to the window-wise classification & comparison"""
    
    speaker_successes = 0
    speaker_tries = 0
    #logging.info('speaker:')
    #logging.info(str(speaker))
    for filename,data in speaker.iteritems():
        logging.info('Filename: '+filename)
        #logging.info('data:')
        #logging.info(speaker[filename])

        mfccs = data['mfccs']
        anos = data['anos']
        n = len(anos)

        #logging.info('frames in file: '+str(n))

        # generate windows with step size 1
        mfcc_windows = np.asarray([mfccs[i:window_size] for i in range(n)])
        ano_windows = np.asarray([anos[i:window_size] for i in range(n)])
        m = len(ano_windows)
        
        # iteration over the windows
        for i in range(m):
            # if length of window ==0 -> drop it
            if len(ano_windows[i]) == 0:
                break
            ones = float(sum(ano_windows[i]))
            speech_ratio = ones/len(ano_windows[i])
            # if there's not enough speech within window -> drop it
            if speech_ratio < 0.5:
                break
            speech_idx = [j for j,ano in enumerate(ano_windows[i]) if ano==1]
            samples = np.asarray(mfcc_windows[i][speech_idx])
            scores = gmm.score(samples)
            score = sum(scores) / len(scores) # stay in the logarithm dimension
            ubm_scores = ubm.score(samples)
            ubm_score = sum(ubm_scores) / len(scores)
            if score > ubm_score:
                speaker_successes += 1
            speaker_tries += 1
    return speaker_successes/float(speaker_tries)

'''
def evaluate(speakers_data, speaker_models, ubm, window_size=256)
    """Checks how good the speaker models work on the given test data"""

    successes = 0
    tries = 0
    success_rates = {}

    for s in speakers_data:
        (speaker_successes,speaker_tries) = speaker_eval(speakers_data[s], speaker_models[s], ubm)
        success_rates[s] = float(speaker_successes) / speaker_tries
        successes += speaker_successes
        tries += speaker_tries
    success_rates['overall'] = float(successes) / tries
    return success_rates

evaluate(test_data, models, ubm)
'''


def eval_mapper(s):
    return s,speaker_eval(test_data[s], models[s], ubm)

print 'Starting parallelized evaluation'
success_rates = dict(ProcessPoolExecutor().map(eval_mapper, test_data.keys()))

success_rates['overall'] = sum(success_rates.values()) / float(len(success_rates))

print "Success rates:"
print success_rates

# add gmm settings to results for evaluation
success_rates.update(gmm_settings)

csv_file = "success_rates.csv"
print "Writing data to "+csv_file

# write header if file does not exist
with open(csv_file, 'wb') as handle:
    w = csv.DictWriter(handle, success_rates.keys())
    w.writeheader()
    w.writerow(success_rates)