from features import mfcc
import scipy.io.wavfile as wav
import audiotools
import math
import random
import pydub
import os
from sklearn import svm
from sklearn.metrics import confusion_matrix
from sklearn import cross_validation
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.mixture import GMM
import numpy as np
import math
import time
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import threading
import multiprocessing
from sklearn.tree import DecisionTreeClassifier
import itertools
import pickle


'''Script for generating the speaker models'''

print "Opening speaker data"
with open('speakers.pkl', 'rb') as handle:
    speakers = pickle.load(handle)

models = {}
data = {}


print "Preparing data"
for s in speakers:
    models[s] = GMM(10)
    mfccs = []
    for filename in speakers[s]:
        idx = [i for i,val in enumerate(speakers[s][filename]['anos']) if val==1]
        mfccs += speakers[s][filename]['mfccs'][idx].tolist()
    data[s] = np.asarray(mfccs)

print "Starting training"
def train(m):
    return m,models[m].fit(data[m])
models = dict(ProcessPoolExecutor(4).map(train, models.keys()))


print "Writing speaker models"
with open('speaker_models.pkl', 'wb') as handle:
    pickle.dump(models,handle)

