import os
from enum import Enum

class Config:
    # PATH CONFIGS SSH or LOCAL
    type='local'
    set="Berlin"
    code_dir = os.path.abspath(os.getcwd()) + '/'
    data_dir = code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Berlin-EMO')
    speech_dir = data_dir + 'wav/'

    model_dir = code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Models')
    support_dir = code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Support')
    noise_dir = support_dir + 'train/'
    noise_int_dir = support_dir + 'noise_int/'
    ir_dir = support_dir + 'impulse_responses/16kHz/wavs/'
    ref_noise = support_dir + 'ref_pink.wav'
    label_dir=speech_dir.replace('wav','labels/CEICES')+'/'+'chunk_labels_4cl_aibo_chunk_set.txt'

    working_dir = data_dir
    normalize_dir = working_dir + 'normalized/'
    noised_dir = working_dir + 'cleanData/'
    convolute_dir = working_dir + 'channeledData/'
    channeledData= 'channeledData'
    cleanData= 'cleanData'
    ubmData= 'ubmData'


    # EMOTION CONFIGS

    class emotionCodes(Enum):
        Happy = 1
        Bored = 2
        Neutral = 3
        Sad = 4
        Disgusted = 5
        Angry = 6
        Anxious = 7


    emotionDict = {
            # active: fast speaking rate,
            # passive: slow speaking rate,
            # normal: normal speaking rate
            'W': emotionCodes.Angry, #index-0 # active-0
            'A': emotionCodes.Anxious, #index-1 # active-0
            'L': emotionCodes.Bored , #index-2 # passive-2
            'E': emotionCodes.Disgusted, #index-3 # active-0
            'F': emotionCodes.Happy, #index-4 # normal-1
            'N': emotionCodes.Neutral, #index-5 # normal-1
            'T': emotionCodes.Sad #index-6 # passive-2
        }

    # COMMON CONFIGS
    num_mixtures_ubm=256 # 512
    frames_per_utterance = 256 #256 roughly equals 2.5s
    ivector_dim = 100 # nicht unter 50
    lda_subspace_dim = 100 # 200 /plda 100/200
    k_fold=4
    n_jobs_kmeans = 10
    gmm_cvtype = 'diag'
    num_features=36
    n_iter_em = 10
    winlen = 0.025
    winstep = 0.01


def setConfiguration(type,set):
    setConfigEmotion(set);
    if type=='local':
        setLocalConfig(set);
        Config.type='local'
    else:
        if type=='ssh':
            setSSHConfig(set);
            Config.type='ssh'
        else:
            raise Exception


def setLocalConfig(set):
    Config.code_dir = os.path.abspath(os.getcwd()) + '/'
    if set=='Berlin':
        Config.data_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Berlin-EMO')
    else:
        if set == 'Fau':
            Config.data_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Fau')
            Config.speech_dir = Config.data_dir + 'wav/'
            Config.label_dir= Config.speech_dir+'/'+'chunk_labels_4cl_aibo_chunk_set.txt'
        else:
            if set=='Semaine':
                Config.data_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Semaine')
            else:
                if set == 'UBM':
                    Config.data_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/UBM')
                else:
                    if set == 'Speaker':
                        Config.data_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Speaker')
                    else:
                        raise Exception
    Config.speech_dir = Config.data_dir + 'wav/'

    Config.model_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Models')
    Config.support_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Support')
    Config.noise_dir = Config.support_dir + 'train/'
    Config.noise_int_dir = Config.support_dir + 'noise_int/'
    Config.ir_dir = Config.support_dir + 'impulse_responses/16kHz/wavs/'
    Config.ref_noise = Config.support_dir + 'ref_pink.wav'

    Config.working_dir = Config.data_dir
    Config.normalize_dir = Config.working_dir + 'normalized/'
    Config.noised_dir =Config.working_dir + 'cleanData/'
    Config.convolute_dir = Config.working_dir + 'channeledData/'
    Config.channeledData= 'channeledData'
    Config.cleanData= 'cleanData'
    Config.n_jobs_kmeans=1
    Config.ubmData= 'ubmData'



def setSSHConfig(set):
    Config.code_dir = os.path.abspath(os.getcwd()) + '/'
    Config.data_dir = '/mnt/tatooine/data/'
    if set=='Berlin':
        Config.speech_dir = Config.data_dir+'emotion/Berlin-EMO/wav/'
    else:
        if set == 'Fau':
            Config.speech_dir = Config.data_dir+'emotion/FAU-Aibo-Emotion-Corpus/wav/'
            Config.label_dir= Config.speech_dir.replace('wav','labels/CEICES')+'/'+'chunk_labels_4cl_aibo_chunk_set.txt'
        else:
            if set=='Semaine':
                Config.speech_dir = Config.data_dir+'emotion/Semaine/Sessions/'
            else:
                if set=='UBM':
                    Config.speech_dir = '/mnt/tatooine/data/vad_speaker_recog/TIMIT_Buckeye/ubm/'
                else:
                    if set=='Speaker':
                        Config.speech_dir = '/mnt/tatooine/data/vad_speaker_recog/TIMIT_Buckeye/speaker_model/'
                    else:
                        raise Exception

    Config.noise_dir = Config.data_dir+'noise/noise_equal_concat/train/'
    Config.ir_dir = '/mnt/naboo/impulse_responses/'
    Config.ref_noise = Config.data_dir+'ref_pink.wav'

    dataset_dir= '/mnt/alderaan/schaefer/Datasets/'
    Config.support_dir = dataset_dir+'Support/'
    Config.noise_int_dir = Config.support_dir+ 'noise_int/'

    Config.model_dir = dataset_dir +'Models/'
    Config.working_dir = dataset_dir+set+'/'
    Config.normalize_dir = Config.working_dir + 'normalized/'
    Config.noised_dir = Config.working_dir + 'cleanData/'
    Config.convolute_dir = Config.working_dir + 'channeledData/'
    Config.channeledData= 'channeledData'
    Config.cleanData= 'cleanData'
    Config.ubmData= 'ubmData'

def setConfigEmotion(set):
    Config.set=set
    if set=='Berlin':
        class emotionCodesBerlin(Enum):
            Happy = 1
            Bored = 2
            Neutral = 3
            Sad = 4
            Disgusted = 5
            Angry = 6
            Anxious = 7

        emotionDictBerlin = {
                # active: fast speaking rate,
                # passive: slow speaking rate,
                # normal: normal speaking rate
                'W': emotionCodesBerlin.Angry, #index-0 # active-0
                'A': emotionCodesBerlin.Anxious, #index-1 # active-0
                'L': emotionCodesBerlin.Bored , #index-2 # passive-2
                'E': emotionCodesBerlin.Disgusted, #index-3 # active-0
                'F': emotionCodesBerlin.Happy, #index-4 # normal-1
                'N': emotionCodesBerlin.Neutral, #index-5 # normal-1
                'T': emotionCodesBerlin.Sad #index-6 # passive-2
            }

        Config.emotionCodes=emotionCodesBerlin
        Config.emotionDict=emotionDictBerlin
        Config.winlen = 0.025
        Config.winstep = 0.01
    else:
        if set == 'Fau':
            class emotionCodesFau(Enum):
                Motherese = 1
                Neutral = 2
                Empathic = 3
                Angry = 4

                # labelMap = {
                #     'J': {'user_state': "joyful", 'description': "the child enjoys Aibo's action and/or notices that \
                #                  something is funny", 'group': 'normal'},
                #     'S': {'user_state': "surprised", 'description': "the child is (positively) surprised, because obviously \
                #                  she/he did not expect Aibo to react that way", 'group': 'normal'},
                #     'M': {'user_state': "motherese", 'description': "the child addresses Aibo in the way mothers/parents \
                #                  address their babies (also called 'infant-directed speech') \
                #                  - either because Aibo is well-behaving or because the child \
                #                  wants Aibo to obey; this is the positive equivalent to \
                #                  'reprimanding'", 'group': 'normal'},
                #     'N': {'user_state': "neutral", 'description': "default user state, not belonging to any other category;\
                #                  not labeled explicitly", 'group': 'normal'},
                #     'O': {'user_state': "other", 'description': "not neutral but not belonging to any of the other \
                #                  categories, i.e. some other spurious emotions", 'group': 'normal'},
                #     'B': {'user_state': "bored", 'description': "the child is (momentarily) not interested in the \
                #                  interaction with Aibo", 'group': 'passive'},
                #     'E': {'user_state': "emphatic", 'description': "the child speaks in a pronounced, accentuated, sometimes \
                #                  hyper-articulated way but without showing any emotion", 'group': 'active'},
                #     'H': {'user_state': "helpless", 'description': "the child hesitates, seems not to know what to tell Aibo \
                #                  next; can be marked by disfluencies and/or filled pauses", 'group': 'passive'},
                #     'T': {'user_state': "touchy", 'description': "the child is slightly irritated; pre-stage of anger", 'group': 'active'},
                #     'R': {'user_state': "reprimanding", 'description': "the child is reproachful, reprimanding, 'wags the finger'; \
                #                  this is the negative equivalent of 'motherese", 'group': 'active'},
                #     'A': {'user_state': "angry", 'description': " the child is clearly angry, annoyed, speaks in a loud voice", 'group': 'active'},
                #     'X': {'user_state': "unknown", 'description': "the word is not labeled ", 'group': 'normal'},
                # }

            label_group_fourclass = {
                'A': emotionCodesFau.Angry,  # active
                'E': emotionCodesFau.Empathic,  # active
                'N': emotionCodesFau.Neutral,  # normal
                'M': emotionCodesFau.Motherese  # normal
            }
            Config.emotionCodes=emotionCodesFau
            Config.emotionDict=label_group_fourclass
            Config.winlen = 0.025
            Config.winstep = 0.01
        else:
            if set=='Semaine':

                class emotionCodesSemaine(Enum):
                    Happy = 1
                    Relaxed = 2
                    Neutral = 3
                    Sad = 4
                    Angry = 5

                Config.emotionCodes=emotionCodesSemaine
                Config.winlen = 0.02
                Config.winstep = 0.02
            else:
                if set == "UBM":
                    class speakerCodes(Enum):
                        FPLS0= 1
                        FPMY0= 2
                        FRAM1= 3
                        FREH0= 4
                        FREW0= 5
                        FRJB0= 6
                        FRLL0= 7
                        FRNG0= 8
                        FSAG0= 9
                        FSAH0= 10
                        FSAK0= 11
                        FSBK0= 12
                        FSCN0= 13
                        FSDC0= 14
                        FSDJ0= 15
                        FSEM0= 16
                        FSGF0= 17
                        FSJK1= 18
                        FSJS0= 19
                        FSJW0= 20
                        FSKC0= 21
                        FSKL0= 22
                        FSKP0= 23
                        FSLB1= 24
                        FSLS0= 25
                        FSMA0= 26
                        FSMM0= 27
                        FSMS1= 28
                        FSPM0= 29
                        FSRH0= 30
                        FSSB0= 31
                        FSXA0= 32
                        FTAJ0= 33
                        FTBR0= 34
                        FTBW0= 35
                        FTLG0= 36
                        FTLH0= 37
                        FTMG0= 38
                        FUTB0= 39
                        FVFB0= 40
                        FVKB0= 41
                        FVMH0= 42
                        MABC0= 43
                        MABW0= 44
                        MADC0= 45
                        MADD0= 46
                        MAEB0= 47
                        MAEO0= 48
                        MAFM0= 49
                        MAJC0= 50
                        MAJP0= 51
                        MAKB0= 52
                        MAKR0= 53
                        MAPV0= 54
                        MARC0= 55
                        MARW0= 56
                        MBAR0= 57
                        MBBR0= 58
                        MBCG0= 59
                        MBDG0= 60
                        MBEF0= 61
                        MBGT0= 62
                        MBJK0= 63
                        MBJV0= 64
                        MBMA0= 65
                        MBMA1= 66
                        MBML0= 67
                        MBNS0= 68
                        MBOM0= 69
                        MBPM0= 70
                        MBSB0= 71
                        MBTH0= 72
                        MBWM0= 73
                        MBWP0= 74
                        MCAE0= 75
                        MCAL0= 76
                        MCDC0= 77
                        MCDD0= 78
                        MCDR0= 79
                        MCEF0= 80
                        MCEM0= 81
                        MCEW0= 82
                        MCHH0= 83
                        MCHL0= 84
                        MCLK0= 85
                        MCLM0= 86
                        MCMB0= 87
                        MCMJ0= 88
                        MCPM0= 89
                        MCRC0= 90
                        MCRE0= 91
                        MCSH0= 92
                        MCSS0= 93
                        MCTH0= 94
                        MCTM0= 95
                        MCTT0= 96
                        MCTW0= 97
                        MCXM0= 98
                        MDAB0= 99
                        MDAC0= 100
                        MDAC2= 101
                        MDAS0= 102
                        MDAW1= 103
                        MDBB0= 104
                        MDBB1= 105
                        MDBP0= 106
                        MDCD0= 107
                        MDCM0= 108
                        MDDC0= 109
                        MDED0= 110
                        MDEF0= 111
                        MDEM0= 112
                        MDHL0= 113
                        MDHS0= 114
                        MDJM0= 115
                        MDKS0= 116
                        MDLB0= 117
                        MDLC0= 118
                        MDLC1= 119
                        MDLC2= 120
                        MDLD0= 121
                        MDLF0= 122
                        MDLH0= 123
                        MDLM0= 124
                        MDLR0= 125
                        MDLR1= 126
                        MDLS0= 127
                        MDMA0= 128
                        MDMT0= 129
                        MDNS0= 130
                        MDPB0= 131
                        MDPK0= 132
                        MDPS0= 133
                        MDRB0= 134
                        MDRD0= 135
                        MDRM0= 136
                        MDSC0= 137
                        MDSJ0= 138
                        MDSS0= 139
                        MDSS1= 140
                        MDTB0= 141
                        MDVC0= 142
                        MDWA0= 143
                        MDWD0= 144
                        MDWH0= 145
                        MDWK0= 146
                        MDWM0= 147
                        MEAL0= 148
                        MEDR0= 149
                        MEFG0= 150
                        MEGJ0= 151
                        MEJL0= 152
                        MEJS0= 153
                        MERS0= 154
                        MESD0= 155
                        MESG0= 156
                        MESJ0= 157
                        MEWM0= 158
                        MFER0= 159
                        MFGK0= 160
                        MFMC0= 161
                        MFRM0= 162
                        MFWK0= 163
                        MFXS0= 164
                        MFXV0= 165
                        MGAF0= 166
                        MGAG0= 167
                        MGAK0= 168
                        MGAR0= 169
                        MGAW0= 170
                        MGES0= 171
                        MGJC0= 172
                        MGJF0= 173
                        MGLB0= 174
                        MGMM0= 175
                        MGRL0= 176
                        MGRP0= 177
                        MGRT0= 178
                        MGSH0= 179
                        MGSL0= 180
                        MGWT0= 181
                        MGXP0= 182
                        MHBS0= 183
                        MHIT0= 184
                        MHJB0= 185
                        MHMG0= 186
                        MHMR0= 187
                        MHPG0= 188
                        MHRM0= 189
                        MHXL0= 190
                        MILB0= 191
                        MJAC0= 192
                        MJAE0= 193
                        MJAI0= 194
                        MJAR0= 195
                        MJBG0= 196
                        MJBR0= 197
                        MJDA0= 198
                        MJDC0= 199
                        MJDE0= 200
                        MJDG0= 201
                        MJDH0= 202
                        MJDM0= 203
                        MJDM1= 204
                        MJEB0= 205
                        MJEB1= 206
                        MJEE0= 207
                        MJES0= 208
                        MJFC0= 209
                        MJFH0= 210
                        MJFR0= 211
                        MJHI0= 212
                        MJJB0= 213
                        MJJG0= 214
                        MJJJ0= 215
                        MJJM0= 216
                        MJKR0= 217
                        MJLB0= 218
                        MJLG1= 219
                        MJLN0= 220
                        MJLS0= 221
                        MJMA0= 222
                        MJMD0= 223
                        MJMM0= 224
                        MJMP0= 225
                        MJPG0= 226
                        MJPM0= 227
                        MJPM1= 228
                        MJRA0= 229
                        MJRF0= 230
                        MJRG0= 231
                        MJRH0= 232
                        MJRH1= 233
                        MJRK0= 234
                        MJRP0= 235
                        MJSR0= 236
                        MJSW0= 237
                        MJTC0= 238
                        MJTH0= 239
                        MJVW0= 240
                        MJWG0= 241
                        MJWS0= 242
                        MJWT0= 243
                        MJXA0= 244
                        MJXL0= 245
                        MKAG0= 246
                        MKAH0= 247
                        MKAJ0= 248
                        MKAM0= 249
                        MKCH0= 250
                        MKCL0= 251
                        MKDB0= 252
                        MKDD0= 253
                        MKDR0= 254
                        MKDT0= 255
                        MKES0= 256
                        MKJL0= 257
                        MKJO0= 258
                        MKLN0= 259
                        MKLR0= 260
                        MKLS0= 261
                        MKLS1= 262
                        MKLT0= 263
                        MKLW0= 264
                        MKRG0= 265
                        MKXL0= 266
                        MLBC0= 267
                        MLEL0= 268
                        MLIH0= 269
                        MLJB0= 270
                        MLJC0= 271
                        MLJH0= 272
                        MLLL0= 273
                        MLNS0= 274
                        MLNT0= 275
                        MLSH0= 276
                        MMAA0= 277
                        MMAB0= 278
                        MMAB1= 279
                        MMAG0= 280
                        MMAM0= 281
                        MMAR0= 282
                        MMBS0= 283
                        MMCC0= 284
                        MMDB0= 285
                        MMDB1= 286
                        MMDG0= 287
                        MMDH0= 288
                        MMDM0= 289
                        MMDM1= 290
                        MMDM2= 291
                        MMDS0= 292
                        MMEA0= 293
                        MMEB0= 294
                        MMGC0= 295
                        MMGG0= 296
                        MMGK0= 297
                        MMJB1= 298
                        MMJR0= 299
                        MMLM0= 300
                        MMPM0= 301
                        MMRP0= 302
                        MMSM0= 303
                        MMVP0= 304
                        MMWB0= 305
                        MMWH0= 306
                        MMWS0= 307
                        MMWS1= 308
                        MMXS0= 309
                        MNET0= 310
                        MNJM0= 311
                        MNLS0= 312
                        MNTW0= 313
                        MPAB0= 314
                        MPAM0= 315
                        MPAM1= 316
                        MPAR0= 317
                        MPCS0= 318
                        MPDF0= 319
                        MPEB0= 320
                        MPFU0= 321
                        MPGH0= 322
                        MPGL0= 323
                        MPGR0= 324
                        MPGR1= 325
                        MPLB0= 326
                        MPPC0= 327
                        MPRB0= 328
                        MPRD0= 329
                        MPRK0= 330
                        MPRT0= 331
                        MPSW0= 332
                        MPWM0= 333
                        MRAB0= 334
                        MRAB1= 335
                        MRAI0= 336
                        MRAM0= 337
                        MRAV0= 338
                        MRBC0= 339
                        MRCG0= 340
                        MRCS0= 341
                        MRCW0= 342
                        MRCZ0= 343
                        MRDD0= 344
                        MRDM0= 345
                        MRDS0= 346
                        MREB0= 347
                        MREE0= 348
                        MREH1= 349
                        MREM0= 350
                        MRES0= 351
                        MREW1= 352
                        MRFK0= 353
                        MRFL0= 354
                        MRGG0= 355
                        MRGM0= 356
                        MRGS0= 357
                        MRHL0= 358
                        MRJB1= 359
                        MRJH0= 360
                        MRJM0= 361
                        MRJM1= 362
                        MRJM3= 363
                        MRJM4= 364
                        MRJO0= 365
                        MRJR0= 366
                        MRJS0= 367
                        MRJT0= 368
                        MRKM0= 369
                        MRKO0= 370
                        MRLD0= 371
                        MRLJ0= 372
                        MRLJ1= 373
                        MRLK0= 374
                        MRLR0= 375
                        MRMB0= 376
                        MRMG0= 377
                        MRMH0= 378
                        MRML0= 379
                        MRMS0= 380
                        MRMS1= 381
                        MROA0= 382
                        MRPC0= 383
                        MRPC1= 384
                        MRPP0= 385
                        MRRE0= 386
                        MRRK0= 387
                        MRSO0= 388
                        MRSP0= 389
                        MRTC0= 390
                        MRTJ0= 391
                        MRTK0= 392
                        MRVG0= 393
                        MRWA0= 394
                        MRWS0= 395
                        MRWS1= 396
                        MRXB0= 397
                        MSAH1= 398
                        MSAS0= 399
                        MSAT0= 400
                        MSAT1= 401
                        MSDB0= 402
                        MSDH0= 403
                        MSDS0= 404
                        MSEM1= 405
                        MSES0= 406
                        MSFH0= 407
                        MSFH1= 408
                        MSFV0= 409
                        MSJK0= 410
                        MSJS1= 411
                        MSLB0= 412
                        MSMC0= 413
                        MSMR0= 414
                        MSMS0= 415
                        MSRG0= 416
                        MSRR0= 417
                        MSTF0= 418
                        MSTK0= 419
                        MSVS0= 420
                        MTAA0= 421
                        MTAB0= 422
                        MTAS0= 423
                        MTAS1= 424
                        MTAT0= 425
                        MTAT1= 426
                        MTBC0= 427
                        MTCS0= 428
                        MTDB0= 429
                        MTDP0= 430
                        MTDT0= 431
                        MTEB0= 432
                        MTER0= 433
                        MTHC0= 434
                        MTJG0= 435
                        MTJM0= 436
                        MTJS0= 437
                        MTJU0= 438
                        MTKD0= 439
                        MTKP0= 440
                        MTLB0= 441
                        MTLC0= 442
                        MTLS0= 443
                        MTML0= 444
                        MTMN0= 445
                        MTMR0= 446
                        MTMT0= 447
                        MTPF0= 448
                        MTPG0= 449
                        MTPP0= 450
                        MTPR0= 451
                        MTRC0= 452
                        MTRR0= 453
                        MTRT0= 454
                        MTWH0= 455
                        MTWH1= 456
                        MTXS0= 457
                        MVJH0= 458
                        MVLO0= 459
                        MVRW0= 460
                        MWAC0= 461
                        MWAD0= 462
                        MWAR0= 463
                        MWBT0= 464
                        MWCH0= 465
                        MWDK0= 466
                        MWEM0= 467
                        MWEW0= 468
                        MWGR0= 469
                        MWJG0= 470
                        MWRE0= 471
                        MWRP0= 472
                        MWSB0= 473
                        MWSH0= 474
                        MWVW0= 475
                        MZMB0= 476
                        s02= 477
                        s03= 478
                        s04= 479
                        s05= 480
                        s08= 481
                        s09= 482
                        s11= 483
                        s13= 484
                        s14= 485
                        s15= 486
                        s17= 487
                        s21= 488
                        s22= 489
                        s23= 490
                        s24= 491
                        s25= 492
                        s26= 493
                        s27= 494
                        s29= 495
                        s30= 496
                        s31= 497
                        s34= 498
                        s35= 499
                        s36= 500
                        s38= 501
                        s39= 502
                        s40= 503


                    speakerDict = {
                        'FPLS0': speakerCodes.FPLS0,
                        'FPMY0': speakerCodes.FPMY0,
                        'FRAM1': speakerCodes.FRAM1,
                        'FREH0': speakerCodes.FREH0,
                        'FREW0': speakerCodes.FREW0,
                        'FRJB0': speakerCodes.FRJB0,
                        'FRLL0': speakerCodes.FRLL0,
                        'FRNG0': speakerCodes.FRNG0,
                        'FSAG0': speakerCodes.FSAG0,
                        'FSAH0': speakerCodes.FSAH0,
                        'FSAK0': speakerCodes.FSAK0,
                        'FSBK0': speakerCodes.FSBK0,
                        'FSCN0': speakerCodes.FSCN0,
                        'FSDC0': speakerCodes.FSDC0,
                        'FSDJ0': speakerCodes.FSDJ0,
                        'FSEM0': speakerCodes.FSEM0,
                        'FSGF0': speakerCodes.FSGF0,
                        'FSJK1': speakerCodes.FSJK1,
                        'FSJS0': speakerCodes.FSJS0,
                        'FSJW0': speakerCodes.FSJW0,
                        'FSKC0': speakerCodes.FSKC0,
                        'FSKL0': speakerCodes.FSKL0,
                        'FSKP0': speakerCodes.FSKP0,
                        'FSLB1': speakerCodes.FSLB1,
                        'FSLS0': speakerCodes.FSLS0,
                        'FSMA0': speakerCodes.FSMA0,
                        'FSMM0': speakerCodes.FSMM0,
                        'FSMS1': speakerCodes.FSMS1,
                        'FSPM0': speakerCodes.FSPM0,
                        'FSRH0': speakerCodes.FSRH0,
                        'FSSB0': speakerCodes.FSSB0,
                        'FSXA0': speakerCodes.FSXA0,
                        'FTAJ0': speakerCodes.FTAJ0,
                        'FTBR0': speakerCodes.FTBR0,
                        'FTBW0': speakerCodes.FTBW0,
                        'FTLG0': speakerCodes.FTLG0,
                        'FTLH0': speakerCodes.FTLH0,
                        'FTMG0': speakerCodes.FTMG0,
                        'FUTB0': speakerCodes.FUTB0,
                        'FVFB0': speakerCodes.FVFB0,
                        'FVKB0': speakerCodes.FVKB0,
                        'FVMH0': speakerCodes.FVMH0,
                        'MABC0': speakerCodes.MABC0,
                        'MABW0': speakerCodes.MABW0,
                        'MADC0': speakerCodes.MADC0,
                        'MADD0': speakerCodes.MADD0,
                        'MAEB0': speakerCodes.MAEB0,
                        'MAEO0': speakerCodes.MAEO0,
                        'MAFM0': speakerCodes.MAFM0,
                        'MAJC0': speakerCodes.MAJC0,
                        'MAJP0': speakerCodes.MAJP0,
                        'MAKB0': speakerCodes.MAKB0,
                        'MAKR0': speakerCodes.MAKR0,
                        'MAPV0': speakerCodes.MAPV0,
                        'MARC0': speakerCodes.MARC0,
                        'MARW0': speakerCodes.MARW0,
                        'MBAR0': speakerCodes.MBAR0,
                        'MBBR0': speakerCodes.MBBR0,
                        'MBCG0': speakerCodes.MBCG0,
                        'MBDG0': speakerCodes.MBDG0,
                        'MBEF0': speakerCodes.MBEF0,
                        'MBGT0': speakerCodes.MBGT0,
                        'MBJK0': speakerCodes.MBJK0,
                        'MBJV0': speakerCodes.MBJV0,
                        'MBMA0': speakerCodes.MBMA0,
                        'MBMA1': speakerCodes.MBMA1,
                        'MBML0': speakerCodes.MBML0,
                        'MBNS0': speakerCodes.MBNS0,
                        'MBOM0': speakerCodes.MBOM0,
                        'MBPM0': speakerCodes.MBPM0,
                        'MBSB0': speakerCodes.MBSB0,
                        'MBTH0': speakerCodes.MBTH0,
                        'MBWM0': speakerCodes.MBWM0,
                        'MBWP0': speakerCodes.MBWP0,
                        'MCAE0': speakerCodes.MCAE0,
                        'MCAL0': speakerCodes.MCAL0,
                        'MCDC0': speakerCodes.MCDC0,
                        'MCDD0': speakerCodes.MCDD0,
                        'MCDR0': speakerCodes.MCDR0,
                        'MCEF0': speakerCodes.MCEF0,
                        'MCEM0': speakerCodes.MCEM0,
                        'MCEW0': speakerCodes.MCEW0,
                        'MCHH0': speakerCodes.MCHH0,
                        'MCHL0': speakerCodes.MCHL0,
                        'MCLK0': speakerCodes.MCLK0,
                        'MCLM0': speakerCodes.MCLM0,
                        'MCMB0': speakerCodes.MCMB0,
                        'MCMJ0': speakerCodes.MCMJ0,
                        'MCPM0': speakerCodes.MCPM0,
                        'MCRC0': speakerCodes.MCRC0,
                        'MCRE0': speakerCodes.MCRE0,
                        'MCSH0': speakerCodes.MCSH0,
                        'MCSS0': speakerCodes.MCSS0,
                        'MCTH0': speakerCodes.MCTH0,
                        'MCTM0': speakerCodes.MCTM0,
                        'MCTT0': speakerCodes.MCTT0,
                        'MCTW0': speakerCodes.MCTW0,
                        'MCXM0': speakerCodes.MCXM0,
                        'MDAB0': speakerCodes.MDAB0,
                        'MDAC0': speakerCodes.MDAC0,
                        'MDAC2': speakerCodes.MDAC2,
                        'MDAS0': speakerCodes.MDAS0,
                        'MDAW1': speakerCodes.MDAW1,
                        'MDBB0': speakerCodes.MDBB0,
                        'MDBB1': speakerCodes.MDBB1,
                        'MDBP0': speakerCodes.MDBP0,
                        'MDCD0': speakerCodes.MDCD0,
                        'MDCM0': speakerCodes.MDCM0,
                        'MDDC0': speakerCodes.MDDC0,
                        'MDED0': speakerCodes.MDED0,
                        'MDEF0': speakerCodes.MDEF0,
                        'MDEM0': speakerCodes.MDEM0,
                        'MDHL0': speakerCodes.MDHL0,
                        'MDHS0': speakerCodes.MDHS0,
                        'MDJM0': speakerCodes.MDJM0,
                        'MDKS0': speakerCodes.MDKS0,
                        'MDLB0': speakerCodes.MDLB0,
                        'MDLC0': speakerCodes.MDLC0,
                        'MDLC1': speakerCodes.MDLC1,
                        'MDLC2': speakerCodes.MDLC2,
                        'MDLD0': speakerCodes.MDLD0,
                        'MDLF0': speakerCodes.MDLF0,
                        'MDLH0': speakerCodes.MDLH0,
                        'MDLM0': speakerCodes.MDLM0,
                        'MDLR0': speakerCodes.MDLR0,
                        'MDLR1': speakerCodes.MDLR1,
                        'MDLS0': speakerCodes.MDLS0,
                        'MDMA0': speakerCodes.MDMA0,
                        'MDMT0': speakerCodes.MDMT0,
                        'MDNS0': speakerCodes.MDNS0,
                        'MDPB0': speakerCodes.MDPB0,
                        'MDPK0': speakerCodes.MDPK0,
                        'MDPS0': speakerCodes.MDPS0,
                        'MDRB0': speakerCodes.MDRB0,
                        'MDRD0': speakerCodes.MDRD0,
                        'MDRM0': speakerCodes.MDRM0,
                        'MDSC0': speakerCodes.MDSC0,
                        'MDSJ0': speakerCodes.MDSJ0,
                        'MDSS0': speakerCodes.MDSS0,
                        'MDSS1': speakerCodes.MDSS1,
                        'MDTB0': speakerCodes.MDTB0,
                        'MDVC0': speakerCodes.MDVC0,
                        'MDWA0': speakerCodes.MDWA0,
                        'MDWD0': speakerCodes.MDWD0,
                        'MDWH0': speakerCodes.MDWH0,
                        'MDWK0': speakerCodes.MDWK0,
                        'MDWM0': speakerCodes.MDWM0,
                        'MEAL0': speakerCodes.MEAL0,
                        'MEDR0': speakerCodes.MEDR0,
                        'MEFG0': speakerCodes.MEFG0,
                        'MEGJ0': speakerCodes.MEGJ0,
                        'MEJL0': speakerCodes.MEJL0,
                        'MEJS0': speakerCodes.MEJS0,
                        'MERS0': speakerCodes.MERS0,
                        'MESD0': speakerCodes.MESD0,
                        'MESG0': speakerCodes.MESG0,
                        'MESJ0': speakerCodes.MESJ0,
                        'MEWM0': speakerCodes.MEWM0,
                        'MFER0': speakerCodes.MFER0,
                        'MFGK0': speakerCodes.MFGK0,
                        'MFMC0': speakerCodes.MFMC0,
                        'MFRM0': speakerCodes.MFRM0,
                        'MFWK0': speakerCodes.MFWK0,
                        'MFXS0': speakerCodes.MFXS0,
                        'MFXV0': speakerCodes.MFXV0,
                        'MGAF0': speakerCodes.MGAF0,
                        'MGAG0': speakerCodes.MGAG0,
                        'MGAK0': speakerCodes.MGAK0,
                        'MGAR0': speakerCodes.MGAR0,
                        'MGAW0': speakerCodes.MGAW0,
                        'MGES0': speakerCodes.MGES0,
                        'MGJC0': speakerCodes.MGJC0,
                        'MGJF0': speakerCodes.MGJF0,
                        'MGLB0': speakerCodes.MGLB0,
                        'MGMM0': speakerCodes.MGMM0,
                        'MGRL0': speakerCodes.MGRL0,
                        'MGRP0': speakerCodes.MGRP0,
                        'MGRT0': speakerCodes.MGRT0,
                        'MGSH0': speakerCodes.MGSH0,
                        'MGSL0': speakerCodes.MGSL0,
                        'MGWT0': speakerCodes.MGWT0,
                        'MGXP0': speakerCodes.MGXP0,
                        'MHBS0': speakerCodes.MHBS0,
                        'MHIT0': speakerCodes.MHIT0,
                        'MHJB0': speakerCodes.MHJB0,
                        'MHMG0': speakerCodes.MHMG0,
                        'MHMR0': speakerCodes.MHMR0,
                        'MHPG0': speakerCodes.MHPG0,
                        'MHRM0': speakerCodes.MHRM0,
                        'MHXL0': speakerCodes.MHXL0,
                        'MILB0': speakerCodes.MILB0,
                        'MJAC0': speakerCodes.MJAC0,
                        'MJAE0': speakerCodes.MJAE0,
                        'MJAI0': speakerCodes.MJAI0,
                        'MJAR0': speakerCodes.MJAR0,
                        'MJBG0': speakerCodes.MJBG0,
                        'MJBR0': speakerCodes.MJBR0,
                        'MJDA0': speakerCodes.MJDA0,
                        'MJDC0': speakerCodes.MJDC0,
                        'MJDE0': speakerCodes.MJDE0,
                        'MJDG0': speakerCodes.MJDG0,
                        'MJDH0': speakerCodes.MJDH0,
                        'MJDM0': speakerCodes.MJDM0,
                        'MJDM1': speakerCodes.MJDM1,
                        'MJEB0': speakerCodes.MJEB0,
                        'MJEB1': speakerCodes.MJEB1,
                        'MJEE0': speakerCodes.MJEE0,
                        'MJES0': speakerCodes.MJES0,
                        'MJFC0': speakerCodes.MJFC0,
                        'MJFH0': speakerCodes.MJFH0,
                        'MJFR0': speakerCodes.MJFR0,
                        'MJHI0': speakerCodes.MJHI0,
                        'MJJB0': speakerCodes.MJJB0,
                        'MJJG0': speakerCodes.MJJG0,
                        'MJJJ0': speakerCodes.MJJJ0,
                        'MJJM0': speakerCodes.MJJM0,
                        'MJKR0': speakerCodes.MJKR0,
                        'MJLB0': speakerCodes.MJLB0,
                        'MJLG1': speakerCodes.MJLG1,
                        'MJLN0': speakerCodes.MJLN0,
                        'MJLS0': speakerCodes.MJLS0,
                        'MJMA0': speakerCodes.MJMA0,
                        'MJMD0': speakerCodes.MJMD0,
                        'MJMM0': speakerCodes.MJMM0,
                        'MJMP0': speakerCodes.MJMP0,
                        'MJPG0': speakerCodes.MJPG0,
                        'MJPM0': speakerCodes.MJPM0,
                        'MJPM1': speakerCodes.MJPM1,
                        'MJRA0': speakerCodes.MJRA0,
                        'MJRF0': speakerCodes.MJRF0,
                        'MJRG0': speakerCodes.MJRG0,
                        'MJRH0': speakerCodes.MJRH0,
                        'MJRH1': speakerCodes.MJRH1,
                        'MJRK0': speakerCodes.MJRK0,
                        'MJRP0': speakerCodes.MJRP0,
                        'MJSR0': speakerCodes.MJSR0,
                        'MJSW0': speakerCodes.MJSW0,
                        'MJTC0': speakerCodes.MJTC0,
                        'MJTH0': speakerCodes.MJTH0,
                        'MJVW0': speakerCodes.MJVW0,
                        'MJWG0': speakerCodes.MJWG0,
                        'MJWS0': speakerCodes.MJWS0,
                        'MJWT0': speakerCodes.MJWT0,
                        'MJXA0': speakerCodes.MJXA0,
                        'MJXL0': speakerCodes.MJXL0,
                        'MKAG0': speakerCodes.MKAG0,
                        'MKAH0': speakerCodes.MKAH0,
                        'MKAJ0': speakerCodes.MKAJ0,
                        'MKAM0': speakerCodes.MKAM0,
                        'MKCH0': speakerCodes.MKCH0,
                        'MKCL0': speakerCodes.MKCL0,
                        'MKDB0': speakerCodes.MKDB0,
                        'MKDD0': speakerCodes.MKDD0,
                        'MKDR0': speakerCodes.MKDR0,
                        'MKDT0': speakerCodes.MKDT0,
                        'MKES0': speakerCodes.MKES0,
                        'MKJL0': speakerCodes.MKJL0,
                        'MKJO0': speakerCodes.MKJO0,
                        'MKLN0': speakerCodes.MKLN0,
                        'MKLR0': speakerCodes.MKLR0,
                        'MKLS0': speakerCodes.MKLS0,
                        'MKLS1': speakerCodes.MKLS1,
                        'MKLT0': speakerCodes.MKLT0,
                        'MKLW0': speakerCodes.MKLW0,
                        'MKRG0': speakerCodes.MKRG0,
                        'MKXL0': speakerCodes.MKXL0,
                        'MLBC0': speakerCodes.MLBC0,
                        'MLEL0': speakerCodes.MLEL0,
                        'MLIH0': speakerCodes.MLIH0,
                        'MLJB0': speakerCodes.MLJB0,
                        'MLJC0': speakerCodes.MLJC0,
                        'MLJH0': speakerCodes.MLJH0,
                        'MLLL0': speakerCodes.MLLL0,
                        'MLNS0': speakerCodes.MLNS0,
                        'MLNT0': speakerCodes.MLNT0,
                        'MLSH0': speakerCodes.MLSH0,
                        'MMAA0': speakerCodes.MMAA0,
                        'MMAB0': speakerCodes.MMAB0,
                        'MMAB1': speakerCodes.MMAB1,
                        'MMAG0': speakerCodes.MMAG0,
                        'MMAM0': speakerCodes.MMAM0,
                        'MMAR0': speakerCodes.MMAR0,
                        'MMBS0': speakerCodes.MMBS0,
                        'MMCC0': speakerCodes.MMCC0,
                        'MMDB0': speakerCodes.MMDB0,
                        'MMDB1': speakerCodes.MMDB1,
                        'MMDG0': speakerCodes.MMDG0,
                        'MMDH0': speakerCodes.MMDH0,
                        'MMDM0': speakerCodes.MMDM0,
                        'MMDM1': speakerCodes.MMDM1,
                        'MMDM2': speakerCodes.MMDM2,
                        'MMDS0': speakerCodes.MMDS0,
                        'MMEA0': speakerCodes.MMEA0,
                        'MMEB0': speakerCodes.MMEB0,
                        'MMGC0': speakerCodes.MMGC0,
                        'MMGG0': speakerCodes.MMGG0,
                        'MMGK0': speakerCodes.MMGK0,
                        'MMJB1': speakerCodes.MMJB1,
                        'MMJR0': speakerCodes.MMJR0,
                        'MMLM0': speakerCodes.MMLM0,
                        'MMPM0': speakerCodes.MMPM0,
                        'MMRP0': speakerCodes.MMRP0,
                        'MMSM0': speakerCodes.MMSM0,
                        'MMVP0': speakerCodes.MMVP0,
                        'MMWB0': speakerCodes.MMWB0,
                        'MMWH0': speakerCodes.MMWH0,
                        'MMWS0': speakerCodes.MMWS0,
                        'MMWS1': speakerCodes.MMWS1,
                        'MMXS0': speakerCodes.MMXS0,
                        'MNET0': speakerCodes.MNET0,
                        'MNJM0': speakerCodes.MNJM0,
                        'MNLS0': speakerCodes.MNLS0,
                        'MNTW0': speakerCodes.MNTW0,
                        'MPAB0': speakerCodes.MPAB0,
                        'MPAM0': speakerCodes.MPAM0,
                        'MPAM1': speakerCodes.MPAM1,
                        'MPAR0': speakerCodes.MPAR0,
                        'MPCS0': speakerCodes.MPCS0,
                        'MPDF0': speakerCodes.MPDF0,
                        'MPEB0': speakerCodes.MPEB0,
                        'MPFU0': speakerCodes.MPFU0,
                        'MPGH0': speakerCodes.MPGH0,
                        'MPGL0': speakerCodes.MPGL0,
                        'MPGR0': speakerCodes.MPGR0,
                        'MPGR1': speakerCodes.MPGR1,
                        'MPLB0': speakerCodes.MPLB0,
                        'MPPC0': speakerCodes.MPPC0,
                        'MPRB0': speakerCodes.MPRB0,
                        'MPRD0': speakerCodes.MPRD0,
                        'MPRK0': speakerCodes.MPRK0,
                        'MPRT0': speakerCodes.MPRT0,
                        'MPSW0': speakerCodes.MPSW0,
                        'MPWM0': speakerCodes.MPWM0,
                        'MRAB0': speakerCodes.MRAB0,
                        'MRAB1': speakerCodes.MRAB1,
                        'MRAI0': speakerCodes.MRAI0,
                        'MRAM0': speakerCodes.MRAM0,
                        'MRAV0': speakerCodes.MRAV0,
                        'MRBC0': speakerCodes.MRBC0,
                        'MRCG0': speakerCodes.MRCG0,
                        'MRCS0': speakerCodes.MRCS0,
                        'MRCW0': speakerCodes.MRCW0,
                        'MRCZ0': speakerCodes.MRCZ0,
                        'MRDD0': speakerCodes.MRDD0,
                        'MRDM0': speakerCodes.MRDM0,
                        'MRDS0': speakerCodes.MRDS0,
                        'MREB0': speakerCodes.MREB0,
                        'MREE0': speakerCodes.MREE0,
                        'MREH1': speakerCodes.MREH1,
                        'MREM0': speakerCodes.MREM0,
                        'MRES0': speakerCodes.MRES0,
                        'MREW1': speakerCodes.MREW1,
                        'MRFK0': speakerCodes.MRFK0,
                        'MRFL0': speakerCodes.MRFL0,
                        'MRGG0': speakerCodes.MRGG0,
                        'MRGM0': speakerCodes.MRGM0,
                        'MRGS0': speakerCodes.MRGS0,
                        'MRHL0': speakerCodes.MRHL0,
                        'MRJB1': speakerCodes.MRJB1,
                        'MRJH0': speakerCodes.MRJH0,
                        'MRJM0': speakerCodes.MRJM0,
                        'MRJM1': speakerCodes.MRJM1,
                        'MRJM3': speakerCodes.MRJM3,
                        'MRJM4': speakerCodes.MRJM4,
                        'MRJO0': speakerCodes.MRJO0,
                        'MRJR0': speakerCodes.MRJR0,
                        'MRJS0': speakerCodes.MRJS0,
                        'MRJT0': speakerCodes.MRJT0,
                        'MRKM0': speakerCodes.MRKM0,
                        'MRKO0': speakerCodes.MRKO0,
                        'MRLD0': speakerCodes.MRLD0,
                        'MRLJ0': speakerCodes.MRLJ0,
                        'MRLJ1': speakerCodes.MRLJ1,
                        'MRLK0': speakerCodes.MRLK0,
                        'MRLR0': speakerCodes.MRLR0,
                        'MRMB0': speakerCodes.MRMB0,
                        'MRMG0': speakerCodes.MRMG0,
                        'MRMH0': speakerCodes.MRMH0,
                        'MRML0': speakerCodes.MRML0,
                        'MRMS0': speakerCodes.MRMS0,
                        'MRMS1': speakerCodes.MRMS1,
                        'MROA0': speakerCodes.MROA0,
                        'MRPC0': speakerCodes.MRPC0,
                        'MRPC1': speakerCodes.MRPC1,
                        'MRPP0': speakerCodes.MRPP0,
                        'MRRE0': speakerCodes.MRRE0,
                        'MRRK0': speakerCodes.MRRK0,
                        'MRSO0': speakerCodes.MRSO0,
                        'MRSP0': speakerCodes.MRSP0,
                        'MRTC0': speakerCodes.MRTC0,
                        'MRTJ0': speakerCodes.MRTJ0,
                        'MRTK0': speakerCodes.MRTK0,
                        'MRVG0': speakerCodes.MRVG0,
                        'MRWA0': speakerCodes.MRWA0,
                        'MRWS0': speakerCodes.MRWS0,
                        'MRWS1': speakerCodes.MRWS1,
                        'MRXB0': speakerCodes.MRXB0,
                        'MSAH1': speakerCodes.MSAH1,
                        'MSAS0': speakerCodes.MSAS0,
                        'MSAT0': speakerCodes.MSAT0,
                        'MSAT1': speakerCodes.MSAT1,
                        'MSDB0': speakerCodes.MSDB0,
                        'MSDH0': speakerCodes.MSDH0,
                        'MSDS0': speakerCodes.MSDS0,
                        'MSEM1': speakerCodes.MSEM1,
                        'MSES0': speakerCodes.MSES0,
                        'MSFH0': speakerCodes.MSFH0,
                        'MSFH1': speakerCodes.MSFH1,
                        'MSFV0': speakerCodes.MSFV0,
                        'MSJK0': speakerCodes.MSJK0,
                        'MSJS1': speakerCodes.MSJS1,
                        'MSLB0': speakerCodes.MSLB0,
                        'MSMC0': speakerCodes.MSMC0,
                        'MSMR0': speakerCodes.MSMR0,
                        'MSMS0': speakerCodes.MSMS0,
                        'MSRG0': speakerCodes.MSRG0,
                        'MSRR0': speakerCodes.MSRR0,
                        'MSTF0': speakerCodes.MSTF0,
                        'MSTK0': speakerCodes.MSTK0,
                        'MSVS0': speakerCodes.MSVS0,
                        'MTAA0': speakerCodes.MTAA0,
                        'MTAB0': speakerCodes.MTAB0,
                        'MTAS0': speakerCodes.MTAS0,
                        'MTAS1': speakerCodes.MTAS1,
                        'MTAT0': speakerCodes.MTAT0,
                        'MTAT1': speakerCodes.MTAT1,
                        'MTBC0': speakerCodes.MTBC0,
                        'MTCS0': speakerCodes.MTCS0,
                        'MTDB0': speakerCodes.MTDB0,
                        'MTDP0': speakerCodes.MTDP0,
                        'MTDT0': speakerCodes.MTDT0,
                        'MTEB0': speakerCodes.MTEB0,
                        'MTER0': speakerCodes.MTER0,
                        'MTHC0': speakerCodes.MTHC0,
                        'MTJG0': speakerCodes.MTJG0,
                        'MTJM0': speakerCodes.MTJM0,
                        'MTJS0': speakerCodes.MTJS0,
                        'MTJU0': speakerCodes.MTJU0,
                        'MTKD0': speakerCodes.MTKD0,
                        'MTKP0': speakerCodes.MTKP0,
                        'MTLB0': speakerCodes.MTLB0,
                        'MTLC0': speakerCodes.MTLC0,
                        'MTLS0': speakerCodes.MTLS0,
                        'MTML0': speakerCodes.MTML0,
                        'MTMN0': speakerCodes.MTMN0,
                        'MTMR0': speakerCodes.MTMR0,
                        'MTMT0': speakerCodes.MTMT0,
                        'MTPF0': speakerCodes.MTPF0,
                        'MTPG0': speakerCodes.MTPG0,
                        'MTPP0': speakerCodes.MTPP0,
                        'MTPR0': speakerCodes.MTPR0,
                        'MTRC0': speakerCodes.MTRC0,
                        'MTRR0': speakerCodes.MTRR0,
                        'MTRT0': speakerCodes.MTRT0,
                        'MTWH0': speakerCodes.MTWH0,
                        'MTWH1': speakerCodes.MTWH1,
                        'MTXS0': speakerCodes.MTXS0,
                        'MVJH0': speakerCodes.MVJH0,
                        'MVLO0': speakerCodes.MVLO0,
                        'MVRW0': speakerCodes.MVRW0,
                        'MWAC0': speakerCodes.MWAC0,
                        'MWAD0': speakerCodes.MWAD0,
                        'MWAR0': speakerCodes.MWAR0,
                        'MWBT0': speakerCodes.MWBT0,
                        'MWCH0': speakerCodes.MWCH0,
                        'MWDK0': speakerCodes.MWDK0,
                        'MWEM0': speakerCodes.MWEM0,
                        'MWEW0': speakerCodes.MWEW0,
                        'MWGR0': speakerCodes.MWGR0,
                        'MWJG0': speakerCodes.MWJG0,
                        'MWRE0': speakerCodes.MWRE0,
                        'MWRP0': speakerCodes.MWRP0,
                        'MWSB0': speakerCodes.MWSB0,
                        'MWSH0': speakerCodes.MWSH0,
                        'MWVW0': speakerCodes.MWVW0,
                        'MZMB0': speakerCodes.MZMB0,
                        's02': speakerCodes.s02,
                        's03': speakerCodes.s03,
                        's04': speakerCodes.s04,
                        's05': speakerCodes.s05,
                        's08': speakerCodes.s08,
                        's09': speakerCodes.s09,
                        's11': speakerCodes.s11,
                        's13': speakerCodes.s13,
                        's14': speakerCodes.s14,
                        's15': speakerCodes.s15,
                        's17': speakerCodes.s17,
                        's21': speakerCodes.s21,
                        's22': speakerCodes.s22,
                        's23': speakerCodes.s23,
                        's24': speakerCodes.s24,
                        's25': speakerCodes.s25,
                        's26': speakerCodes.s26,
                        's27': speakerCodes.s27,
                        's29': speakerCodes.s29,
                        's30': speakerCodes.s30,
                        's31': speakerCodes.s31,
                        's34': speakerCodes.s34,
                        's35': speakerCodes.s35,
                        's36': speakerCodes.s36,
                        's38': speakerCodes.s38,
                        's39': speakerCodes.s39,
                        's40': speakerCodes.s40,
                        }

                    Config.emotionCodes=speakerCodes
                    Config.emotionDict=speakerDict
                    Config.winlen = 0.025
                    Config.winstep = 0.01
                else:
                    if set=='Speaker':
                        class speakerCodes(Enum):
                            FSJG0_f= 1
                            MAHH0= 2
                            MCCS0= 3
                            MPMB0= 4
                            MTQC0= 5
                            s01_f= 6
                            s06= 7
                            s20_f= 8
                            s32= 9
                            s37_f= 10

                        speakerDict = {
                                'FSJG0_f': speakerCodes.FSJG0_f,
                                'MAHH0': speakerCodes.MAHH0,
                                'MCCS0': speakerCodes.MCCS0,
                                'MPMB0': speakerCodes.MPMB0,
                                'MTQC0': speakerCodes.MTQC0,
                                's01_f': speakerCodes.s01_f,
                                's06': speakerCodes.s06,
                                's20_f': speakerCodes.s20_f,
                                's32': speakerCodes.s32,
                                's37_f': speakerCodes.s37_f,
                            }

                        Config.emotionCodes=speakerCodes
                        Config.emotionDict=speakerDict
                        Config.winlen = 0.025
                        Config.winstep = 0.01
                    else:
                        raise Exception
