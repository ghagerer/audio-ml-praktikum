import numpy as np
import bob
import sys
import math
import itertools
from Configuration import Config
from sklearn.cluster import KMeans
import random
from collections import Counter



def ubm(ubmsamples):
    kmeans = bob.machine.KMeansMachine(Config.num_mixtures_ubm, Config.num_features)
    kmeansTrainer = bob.trainer.KMeansTrainer()
    kmeansTrainer.max_iterations = 5
    kmeansTrainer.convergence_threshold = 1e-5
    kmeansTrainer.train(kmeans, ubmsamples)
    init_mean = kmeans.means
    init_cov,init_weight= kmeans.get_variances_and_weights_for_each_cluster(ubmsamples)
    np.savez(Config.model_dir + 'cudaubm', init_cov=init_cov, init_mean=init_mean, init_weight=init_weight)
    ubm = bob.machine.GMMMachine(Config.num_mixtures_ubm,Config.num_features) # Create a machine with 2 Gaussian and feature dimensionality 3
    ubm.means = init_mean
    ubm.variances = init_cov
    ubm.weights = init_weight
    trainer = bob.trainer.ML_GMMTrainer(True, True, True) # update means/variances/weights at each iteration
    trainer.convergence_threshold = 1e-5
    trainer.max_iterations = Config.n_iter_em
    trainer.train(ubm, ubmsamples)
    ubm_file = 'ubm_' + str(Config.num_mixtures_ubm) + '.hdf5'
    ubm.save(bob.io.HDF5File(Config.model_dir + ubm_file, 'w' ))
    return ubm 


def ubm_cuda(ubmsamples):
    initKMeans = KMeans(n_clusters=Config.num_mixtures_ubm, init='k-means++', n_init=1, max_iter=5, \
        verbose=0, random_state=1, n_jobs=Config.n_jobs_kmeans)
    initKMeans.fit(ubmsamples)
    init_mean = initKMeans.cluster_centers_
    init_cov = np.zeros((Config.num_mixtures_ubm,Config.num_features,Config.num_features))
    for i in range(Config.num_mixtures_ubm):
        cluster_index = i
        samples_of_cluster = ubmsamples[initKMeans.labels_ == cluster_index]
        if len(samples_of_cluster) > 0:
            cov_of_cluster = np.cov(samples_of_cluster.T)
            diag_cov = cov_of_cluster * np.identity(Config.num_features)
        else:
            diag_cov = np.identity(Config.num_dimension)
        init_cov[i,:,:] = diag_cov
    init_mean = np.array(init_mean,dtype=np.float32)
    init_cov =  np.array(init_cov,dtype=np.float32)
    init_weight = np.array(np.ones(Config.num_mixtures_ubm) * (1/np.float32(Config.num_mixtures_ubm)),dtype=np.float32)
    print init_mean,init_cov,init_weight
    # kmeans = bob.machine.KMeansMachine(Config.num_mixtures_ubm, Config.num_features)
    # kmeansTrainer = bob.trainer.KMeansTrainer()
    # kmeansTrainer.max_iterations = 5
    # kmeansTrainer.convergence_threshold = 1e-5
    # kmeansTrainer.train(kmeans, ubmsamples)
    # init_mean = kmeans.means
    # init_cov,init_weight= kmeans.get_variances_and_weights_for_each_cluster(ubmsamples)
    from gmm_specializer.gmm import GMM
    gmm_fast_ubm = GMM(Config.num_mixtures_ubm, Config.num_features, cvtype=Config.gmm_cvtype, means=init_mean, covars=init_cov, weights=init_weight)
    gmm_fast_ubm.train(ubmsamples, max_em_iters=Config.n_iter_em)
    ubm = bob.machine.GMMMachine(Config.num_mixtures_ubm, Config.num_features)
    ubm.means = np.array(gmm_fast_ubm.get_all_component_means(), 'float64')
    ubm.weights = np.array(gmm_fast_ubm.get_all_component_weights(), 'float64')
    ubm.variances = np.array(gmm_fast_ubm.get_all_component_full_covariance(), 'float64').diagonal(axis1=1,axis2=2)
    ubm_file = 'ubm_' + str(Config.num_mixtures_ubm) + '.hdf5'
    ubm.save(bob.io.HDF5File(Config.model_dir + ubm_file, 'w' ))
    print ubm
    return ubm

def init_machines(ubm,proj_train_features):
    proj_train_features_flat = [item for sublist in proj_train_features for item in sublist]
    ivec_machine = bob.machine.IVectorMachine(ubm, Config.ivector_dim)
    ivec_machine.variance_threshold = 1e-5
    ivec_trainer = bob.trainer.IVectorTrainer(update_sigma=True, max_iterations=10)
    ivec_trainer.initialize(ivec_machine, proj_train_features_flat)
    ivec_trainer.train(ivec_machine, proj_train_features_flat)
    ivec_machine.save(bob.io.HDF5File(Config.model_dir+'ivec_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    ivectors_matrix  = []
    for gmmstats in proj_train_features_flat:
        projected_ivector = ivec_machine.forward(gmmstats)
        ivectors_matrix.append(projected_ivector)
    ivectors_matrix = np.vstack(ivectors_matrix)
    whitening_machine = bob.machine.LinearMachine(ivectors_matrix.shape[1],ivectors_matrix.shape[1])
    t = bob.trainer.WhiteningTrainer()
    t.train(whitening_machine, ivectors_matrix)
    whitening_machine.save(bob.io.HDF5File(Config.model_dir+'white_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    whitened_ivectors = []
    for proj_train_features_speaker in proj_train_features:
        speaker_ivectors = []
        for gmmstats in proj_train_features_speaker:
            projected_ivector = ivec_machine.forward(gmmstats)
            whitened_ivector = whitening_machine.forward(projected_ivector)
            normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
            speaker_ivectors.append(normalized_ivector)
        whitened_ivectors.append(np.vstack(speaker_ivectors))
	proj_train_features = whitened_ivectors
    t = bob.trainer.FisherLDATrainer(strip_to_rank=False)
    lda_machine, __eig_vals = t.train(proj_train_features)
    lda_machine.resize(lda_machine.shape[0], Config.lda_subspace_dim)
    lda_machine.save(bob.io.HDF5File(Config.model_dir+'lda_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    lda_projected_ivectors = []
    for whitened_ivectors_speaker in proj_train_features:
        speaker_lda_ivectors = []
        for ivector in whitened_ivectors_speaker:
            lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
            lda_machine(ivector, lda_ivector)
            speaker_lda_ivectors.append(lda_ivector)
        lda_projected_ivectors.append(np.vstack(speaker_lda_ivectors))
    proj_train_features = lda_projected_ivectors
    t = bob.trainer.WCCNTrainer()
    wccn_machine = t.train(proj_train_features)
    wccn_machine.save(bob.io.HDF5File(Config.model_dir+'wccn_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    return ivec_machine, whitening_machine, lda_machine, wccn_machine


def train_by_category(ubm, cleansamples,cleanannos,cleanvadannos):
    proj_train_features = []
    for category in set(cleanannos):
        speaker_samples = cleansamples[cleanannos==category]
        speaker_ano =  cleanvadannos[cleanannos==category]
        proj_train_features_speaker = []
        num_utt = int(round(len(speaker_samples)/Config.frames_per_utterance))
        for utt_count in range(0, num_utt):
            data = speaker_samples[utt_count*Config.frames_per_utterance \
                : (utt_count+1)*Config.frames_per_utterance]
            data_ano = speaker_ano[utt_count*Config.frames_per_utterance \
                : (utt_count+1)*Config.frames_per_utterance]
            #only process window if more than half the frames are voiced
            if float(sum(data_ano))/len(data_ano) >= 0.5:
                #CMVN:
                tmp_std = np.std(data[data_ano==1], 0)
                data -= np.mean(data[data_ano==1], 0)
                data /= tmp_std
                proj_data = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
                ubm.acc_statistics(data[data_ano==1], proj_data)
                proj_train_features_speaker.append(proj_data)
        proj_train_features.append(proj_train_features_speaker)
    return proj_train_features


def ivecs_from_stats(emotion_stats,ivec_machine, whitening_machine):
    whitened_ivectors = []
    for proj_train_features_speaker in emotion_stats:
        speaker_ivectors = []
        for gmmstats in proj_train_features_speaker:
            projected_ivector = ivec_machine.forward(gmmstats)
            whitened_ivector = whitening_machine.forward(projected_ivector)
            normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
            speaker_ivectors.append(normalized_ivector)
        whitened_ivectors.append(np.vstack(speaker_ivectors))
    return whitened_ivectors

def ldas_from_ivecs(emotion_ivecs,lda_machine):
    lda_projected_ivectors = []
    for whitened_ivectors_speaker in emotion_ivecs:
        speaker_lda_ivectors = []
        for ivector in whitened_ivectors_speaker:
            lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
            lda_machine(ivector, lda_ivector)
            speaker_lda_ivectors.append(lda_ivector)
        lda_projected_ivectors.append(np.vstack(speaker_lda_ivectors))
    return lda_projected_ivectors

def wccn_from_lda(emotion_ivecs,wccn_machine):
    wccn_lda_ivectors = []
    for lda_ivector in emotion_ivecs:
        speaker_wccn_ivectors = []
        for ivector in lda_ivector:
            wccn_lda_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
            wccn_machine(ivector, wccn_lda_ivector)
            speaker_wccn_ivectors.append(wccn_lda_ivector)
        wccn_lda_ivectors.append(np.vstack(speaker_wccn_ivectors))
    return wccn_lda_ivectors

def average_by_emotion(emotion_wccns):
    average_ivecs= []
    for speaker_ivecs in emotion_wccns:
        average_ivecs.append(np.mean(speaker_ivecs,0))
    return average_ivecs

def mixed_windows_for_test(testsamples,testanonos, testvadannos):
    test_sample_windows=[]
    test_label_windows = []
    num_utt = int(round(len(testsamples)/Config.frames_per_utterance))
    for utt_count in range(0, num_utt):
        data = testsamples[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        data_ano = testanonos[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        data_vad = testvadannos[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        #only process window if more than half the frames are voiced
        if float(sum(data_vad))/len(data_vad) >= 0.5:
            #CMVN:
            tmp_std = np.std(data[data_vad==1], 0)
            data -= np.mean(data[data_vad==1], 0)
            data /= tmp_std
            test_sample_windows.append(data[data_vad==1])
            test_label_windows.append(data_ano[data_vad==1])
    return test_sample_windows, test_label_windows



def wccn_from_testsamples(testsamples,ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine):
    gmmstats = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
    ubm.acc_statistics(testsamples, gmmstats)
    projected_ivector = ivec_machine.forward(gmmstats)
    whitened_ivector = whitening_machine.forward(projected_ivector)
    normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
    lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
    lda_machine(normalized_ivector, lda_ivector)
    wccn_lda_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
    wccn_machine(lda_ivector, wccn_lda_ivector)
    return wccn_lda_ivector


def test_wccn_on_models(test_wccn, test_label, average_training_data_by_emotion):
    max = -sys.maxint - 1
    for model in average_training_data_by_emotion:
        score = abs(cosine_distance(model, test_wccn))
        if score>max:
            max=score
            index = np.where(average_training_data_by_emotion == model)
            category = index[0][0]+1
    return (category==round(np.mean(test_label)))

def test_wccn_on_models_against_impostor(test_wccn, test_label, impostor_model,average_training_data_by_emotion):
    fp=0
    tp=0
    fn=0
    tn=0
    for i in range(len(average_training_data_by_emotion)):
        score = abs(cosine_distance(average_training_data_by_emotion[i], test_wccn))
        impostor_score = abs(cosine_distance(impostor_model, test_wccn))
        real_speaker = Counter(test_label).most_common(1)[0][0]
        speaker_int = Config.emotionDict.get(real_speaker)
        if speaker_int is None:
            speaker_int = -2
        if not isinstance( speaker_int, ( int, long ) ):
            speaker_int=real_speaker
        print (score>impostor_score) , i+6, speaker_int
        if score>impostor_score:
            if i+6==speaker_int:
                tp+=1
            else:
                fp+=1
        if score<=impostor_score:
            if i+6==speaker_int:
                fn+=1
            else:
                tn+=1
    return np.asmatrix([[tp,fp],[fn,tn]])

def cosine_distance(a, b):
    if len(a) != len(b):
        raise ValueError, "a and b must be same length"
    numerator = sum(tup[0] * tup[1] for tup in itertools.izip(a,b))
    denoma = sum(avalue ** 2 for avalue in a)
    denomb = sum(bvalue ** 2 for bvalue in b)
    result = numerator / (math.sqrt(denoma)*math.sqrt(denomb))
    return result


def train_emotion_gmms(ubm, cleansamples,cleanannos,setname):
    emotion_models =[]
    for i in set(cleanannos):
        data= cleansamples[cleanannos == float(i)]
        if len(data)>0:
            relevance_factor = 4.
            trainer = bob.trainer.MAP_GMMTrainer(relevance_factor, True, True, True)
            trainer.convergence_threshold = 1e-5
            trainer.max_iterations = Config.n_iter_em
            trainer.set_prior_gmm(ubm)
            gmmAdapted = bob.machine.GMMMachine(Config.num_mixtures_ubm,Config.num_features) # Create a new machine for the MAP estimate
            trainer.train(gmmAdapted, data)
            gmmAdapted.save(bob.io.HDF5File(Config.model_dir + str(i)+setname+'gmm.hdf5', 'w' ))
            emotion_models.append(gmmAdapted)
    return emotion_models


def train_emotion_gmms_cuda(ubm, cleansamples,cleanannos,setname):
    emotion_models =[]
    init_mean = ubm['init_mean']
    init_cov = ubm['init_cov']
    init_weight = ubm['init_weight']
    for i in set(cleanannos):
        data= cleansamples[cleanannos == float(i)]
        if len(data)>0:
            from gmm_specializer.gmm import GMM
            gmm_fast_speaker = GMM(Config.num_mixtures_ubm, Config.num_features, cvtype=Config.gmm_cvtype, \
            means=init_mean, \
            covars=init_cov, \
            weights=init_weight)
            print gmm_fast_speaker
            gmm_fast_speaker.train(data,max_em_iters=1)
            gmmAdapted = bob.machine.GMMMachine(Config.num_mixtures_ubm, Config.num_features)
            gmmAdapted.means = np.array(gmm_fast_speaker.get_all_component_means(), 'float64')
            gmmAdapted.weights = np.array(gmm_fast_speaker.get_all_component_weights(), 'float64')
            gmmAdapted.variances = np.array(gmm_fast_speaker.get_all_component_full_covariance(), 'float64').diagonal(axis1=1,axis2=2)
            print gmmAdapted
            gmmAdapted.save(bob.io.HDF5File(Config.model_dir + str(i)+setname+'gmm.hdf5', 'w' ))
            emotion_models.append(gmmAdapted)
    return emotion_models



def read_emotion_gmms(path,set):
    emotion_models =[]
    for name, emotion in Config.emotionCodes.__members__.items():
        emo=emotion
        if not isinstance( emo, ( int, long ) ):
            emo=emotion.value
        emotion_models.append(bob.machine.GMMMachine(bob.io.HDF5File(path+str(emo)+set+'gmm.hdf5', 'r')))
    return emotion_models


def test_gmm_on_models_against_impostor(testsamples, test_label, impostor_model, gmm_models):
    fp=0
    tp=0
    fn=0
    tn=0
    for j in range(len(gmm_models)):
        impostor_score=0
        score=0
        for i in range(len(testsamples)):
            impostor_score = impostor_score+ impostor_model.log_likelihood(testsamples[i])/len(testsamples)
            score = score+ gmm_models[j].log_likelihood(testsamples[i])/len(testsamples)
        real_speaker = Counter(test_label).most_common(1)[0][0]
        if not isinstance( real_speaker, ( int, long ) ):
                    real_speaker=real_speaker.value
        print (score>impostor_score) , j+1, Config.emotionDict.get(real_speaker)
        if score>impostor_score:
            if j+1==Config.emotionDict.get(real_speaker):
                tp+=1
            else:
                fp+=1
        if score<=impostor_score:
            if j+1==Config.emotionDict.get(real_speaker):
                fn+=1
            else:
                tn+=1
    return np.asmatrix([[tp,fp],[fn,tn]])

def mixtests(testsamples,testfiles):
    shufflelist = []
    for file in set(testfiles):
        shufflelist.append(testsamples[testfiles==file])
    random.shuffle(shufflelist)
    return np.vstack(shufflelist)