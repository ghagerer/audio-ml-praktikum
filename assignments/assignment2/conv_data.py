from enum import Enum
import numpy as np
import pickle

class speakerCodes(Enum):
    FSJG0_f= 1
    MAHH0= 2
    MCCS0= 3
    MPMB0= 4
    MTQC0= 5
    s01_f= 6
    s06= 7
    s20_f= 8
    s32= 9
    s37_f= 10


data_load = np.load('channeledData.npz')
speakers = data_load['data_names']
files = data_load['data_files']
data_vad = data_load['data_vad']
samples = data_load['data_mfccs']

# rf data
data_vad = np.load('trainedAnnotations.npz')
data_vad = data_vad['data_vad']



speaker_names = set([speakerCodes(s).name for s in speakers])

a = {}
for s in speaker_names:
    a[s] = {}
    a[s] = files[speakers==speakerCodes[s].value]
    tmp = []
    for f in a[s]:
        tmp += [str(f)]
    a[s] = list(set(tmp))
    tmp = {}
    for f in a[s]:
        tmp[f] = {'anos':[], 'mfccs':[]}
    a[s] = tmp
    for f in a[s]:
        a[s][f]['anos'] = data_vad[long(f)==files]
        a[s][f]['mfccs'] = samples[long(f)==files]
    

speakers = a

with open('speakers.pkl','wb') as handle:
    pickle.dump(speakers,handle)

