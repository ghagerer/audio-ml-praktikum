from features import mfcc
import scipy.io.wavfile as wav
import audiotools
import math
import random
import pydub
import os
from sklearn import svm
from sklearn.metrics import confusion_matrix
from sklearn import cross_validation
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.mixture import GMM
import numpy as np
import math
import time
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import threading
import multiprocessing
from sklearn.tree import DecisionTreeClassifier
import itertools
import pickle
import logging
import csv
import joblib
import argparse
import json
from collections import Counter
import bob
import sys
import iVector2
from Configuration import Config
import Configuration


# given in number of frames
window_size = 256
step_size = 128

def speaker_anos(speaker, anos):
    return [int(speaker==a) for a in anos]

def speech_anos(anos):
    return [abs(int(''==a)-1) for a in anos]

def majority_speaker(anos):
    return Counter(anos).most_common(1)[0][0]


print "Reading UBM"
#ubm = joblib.load('ubm.pkl')
ubm = bob.machine.GMMMachine(bob.io.HDF5File('ubm_256_htc.hdf5', 'r'))


# initialization
Configuration.setConfiguration('local','Speaker')
data = np.load('ubmData2.npz')
ubmannos = np.array(data["ubm_names"])
ubmsamples = np.array(data["ubm_mfccs"][:,:Config.num_features])
ubmvadannos = np.array(data['data_vad'])
print len(ubmvadannos), len(ubmannos), ubmsamples.shape
ubm_models = iVector2.train_by_category(ubm, ubmsamples, ubmannos, ubmvadannos)
ivec_machine, whitening_machine, lda_machine, wccn_machine = iVector2.init_machines(ubm, ubm_models)
impostor = iVector2.wccn_from_testsamples(ubmsamples,ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine)


 
print "Opening cross validation data"
with open('cv_data.pkl', 'rb') as handle:
    cv_data = pickle.load(handle)

print len(cv_data)

# Method to parallelize training
def train(speaker):
    mfccs = cv_run['train'][speaker]
    labels = np.array([1]*len(mfccs))
    print str(mfccs.shape)
    print str(labels.shape)
    return speaker, iVector2.train_emotion_gmms(ubm, mfccs, labels, speaker)[0]
    #return speaker,GMM(10).fit(cv_run['train'][speaker])

# TODO: only run 1 time
models = []
confusion_matrices = []
for cv_run in cv_data:

    print 'Starting new cross validation run'

    confusion_matrices += [np.zeros(shape=(2,2))]

    print "Training all speaker models"
    '''
    # first train all speaker models for that run
    models += [{}]
    executor = ThreadPoolExecutor(1)
    models[-1] = dict(executor.map(train, cv_run['train'].keys()))
    '''
    
    # ivector training
    
    iteration = [("s01_f",6),("s06",7),("s20_f",8),("s32",9),("s37_f",10)]
    
    speaker_names = []
    mfcc_array = []
    for speaker_name,it in iteration:
        speaker_names += [speaker_name] * len(cv_run['train'][speaker_name])
        mfcc_array += list(cv_run['train'][speaker_name])
    vad_anos = [1]*len(mfcc_array)
    #print set(speaker_names)
    speaker_names = np.array(speaker_names)
    mfcc_array = np.array(mfcc_array)
    vad_anos = np.array(vad_anos)
    speaker_names = speaker_names
    mfcc_array = mfcc_array
    vad_anos = vad_anos
    emotion_models = iVector2.train_by_category(ubm, mfcc_array, speaker_names, vad_anos) # trainanos = data_names
    emotion_ivecs = iVector2.ivecs_from_stats(emotion_models,ivec_machine, whitening_machine)
    emotion_ldas = iVector2.ldas_from_ivecs(emotion_ivecs,lda_machine)
    emotion_wccns = iVector2.wccn_from_lda(emotion_ivecs,wccn_machine)
    average_training_data_by_emotion = iVector2.average_by_emotion(emotion_wccns)
    print 'len(average_training_data_by_emotion)=', len(average_training_data_by_emotion)
    # end ivector training
    
    
    # here begins the evaluation part
    mfccs = cv_run['test']['mfccs']
    anos = cv_run['test']['anos']
    n = len(mfccs)

    print "Creating windows for testing"
    # generate windows
    steps = range(0,n,step_size)
    mfcc_windows = np.asarray([mfccs[k:(k+window_size)] for k in steps])
    ano_windows = np.asarray([anos[k:(k+window_size)] for k in steps])
    
    empty_counter = 0
    for w in mfcc_windows:
        if len(w) == 0:
            empty_counter += 1
    print "empty_counter="+str(empty_counter)
    
    # m = amount of windows
    m = len(ano_windows)
    print 'len(ano_windows)='+str(len(ano_windows))

    print "Starting processing of all windows"
    # function for parallelization
    def process_window(j):
        current_anos = np.asarray(ano_windows[j])
        current_mfccs = np.asarray(mfcc_windows[j])

        # if length of window == 0 -> drop it
        if len(ano_windows[j]) == 0 and len(mfcc_windows[j]) == 0:
            return np.zeros(shape=(2,2))

        # how much percent of speech is in the current window
        speech = speech_anos(current_anos)
        ones = float(sum(speech))
        speech_ratio = ones/len(speech)

        # if there's not enough (<50%) speech within window -> drop it
        if speech_ratio < 0.5:
            return np.zeros(shape=(2,2))
        """
        # get speaker who speaks most here
        real_speaker = majority_speaker(current_anos)

        # score current window with ubm
        ubm_scores = ubm.score(current_mfccs)
        #ubm_scores = ubm.log_likelihood(current_mfccs)
        ubm_score = sum(ubm_scores) / len(ubm_scores)
        
        
        confusion_matrix = np.zeros(shape=(2,2))

        # now check for all speakers
        for speaker,gmm in models[-1].iteritems():

            # score current window with speaker model
            # compare afterwards which model fits better
            
            scores = gmm.score(current_mfccs)
            #scores = gmm.log_likelihood(current_mfccs)
            score = sum(scores) / len(scores) # average the logarithms

            if score > ubm_score:
                # speaker model fits better
                # is it the speaker from the speaker model?
                if speaker == real_speaker:
                    # true positive
                    confusion_matrix[0][0] += 1
                else:
                    # false negative
                    confusion_matrix[0][1] += 1
            else:
                # speaker model does not fit better than ubm
                if speaker == real_speaker:
                    # false positive
                    confusion_matrix[1][0] += 1
                else:
                    # true negative
                    confusion_matrix[1][1] += 1
        """
        
        test_wccn = iVector2.wccn_from_testsamples(current_mfccs, ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine)
        confusion_matrix = iVector2.test_wccn_on_models_against_impostor(test_wccn, current_anos, impostor, average_training_data_by_emotion)
        
        return confusion_matrix

    # iteration over the windows
    executor = ThreadPoolExecutor(4)
    matrices = list(executor.map(process_window, range(m)))
    
    for mt in matrices:
        confusion_matrices[-1] += mt
    
    print "Evaluation run finished, confusion:"
    print confusion_matrices[-1]
    

# make sum of all confusion matrices
overall = np.zeros(shape=(2,2))
for m in confusion_matrices:
    overall += m

confusion_matrices += [overall]

print confusion_matrices

np.savez('confusion_matrices.npz', confusion_matrices=confusion_matrices)
