from features import mfcc
import scipy.io.wavfile as wav
import audiotools
import math
import random
import pydub
import os
from sklearn import svm
from sklearn.metrics import confusion_matrix
from sklearn import cross_validation
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.mixture import GMM
import numpy as np
import math
import time
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import threading
import multiprocessing
from sklearn.tree import DecisionTreeClassifier
import itertools
import pickle
import logging
import csv
import joblib
import argparse
import json

logging.basicConfig(filename=os.getcwd()+'/debug.log', level=logging.DEBUG, format='%(asctime)s %(message)s')


'''Script for preparing the crossvalidation speaker data'''


print "Opening speaker data"
with open('speakers.pkl', 'rb') as handle:
    speakers = pickle.load(handle)


print "Preparing data"


# sort all files by filesize, descending order
by_length = lambda file: len(file['anos'])
for s in speakers:
    speakers[s] = sorted(speakers[s].values(), key=by_length, reverse=True)

# init cross validation data dictionary
cv_data = [{'train':{}, 'test':[]} for i in range(4)]

for s in speakers:

    # count all features for this person from all files
    n = float(0)
    for file in speakers[s]:
        n += len(file['anos'])

    n_test = 0
    j = 0
    idx = [0]
    # get 1/4th of the data for testing
    # j marks the beginning of a 1/4th split
    for file in speakers[s]:
        n_test += len(file['anos'])
        j += 1
        if n_test/n > 0.25:
            n_test = 0
            idx.append(j)

    # split into train and test data
    j = 0
    for k,j in enumerate(idx):
        start = j
        end = idx[k+1] if k!=len(idx)-1 else len(speakers[s])

        # 1/4 into test data
        files = speakers[s][start:end]
        for f in files:
            f['speaker'] = s
        cv_data[k]['test'] += files

        # the rest 3/4 put into train data
        cv_data[k]['train'][s] = []
        if start != 0:
            cv_data[k]['train'][s] += speakers[s][0:start]
        if end != len(speakers[s]):
            cv_data[k]['train'][s] += speakers[s][end:len(speakers[s])]
        mfccs = []
        for file in cv_data[k]['train'][s]:
            speech_idx = [j for j,ano in enumerate(file['anos']) if ano==1]
            mfccs += list(file['mfccs'][speech_idx])
        cv_data[k]['train'][s] = np.array(mfccs)


# arrange test data
for i,run in enumerate(cv_data):

    random.shuffle(run['test'])

    mfccs = []
    anos = []
    for file in run['test']:
        mfccs += list(file['mfccs'])
        anos += [file['speaker'] if a==1 else '' for a in file['anos']]

    run['test'] = {
        'mfccs': np.array(mfccs),
        'anos': anos
    }

    print ''
    print 'Test Run '+str(i)+':'
    print run['test']['mfccs'].shape
    print len(run['test']['anos'])

    print 'Train Run '+str(i)+':'
    for s in run['train']:
        print s+': '+str(len(run['train'][s]))


print 'Writing cross validation data'
with open('cv_data.pkl', 'wb') as handle:
    pickle.dump(cv_data,handle)