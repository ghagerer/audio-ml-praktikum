import SpeakerData
import Configuration
import UBMData
import numpy as np
from Configuration import Config
from sklearn.ensemble import RandomForestClassifier
import cPickle


type='ssh'

Configuration.setConfiguration(type,'UBM') # local or ssh
[ubm_speakers,ubm_files,ubm_samples, ubm_data_vad]=UBMData.readData()

Configuration.setConfiguration(type,'Speaker') # local or ssh, Berlin, Fau or Semaine or Speakers
[speakers,files,samples, data_vad]=SpeakerData.readChanneled()

try:
    with open('rf.pkl', 'rb') as f:
        clf = cPickle.load(f)
except:
    clf = RandomForestClassifier(n_estimators=20)
    clf.fit(ubm_samples[:,:Config.num_features], ubm_data_vad[:])
    with open('rf.pkl', 'wb') as f:
        cPickle.dump(clf, f)
    trained_annos = clf.predict(samples[:,:Config.num_features])
    np.savez('trainedAnnotations', data_vad=trained_annos)