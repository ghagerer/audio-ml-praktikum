# README #


## Speaker Recognition Modell trainieren ##

In speakers.pkl sind die Daten fürs Training der speaker recognition gespeichert.

Arrayformat:
speakers[Sprechername][Dateiname] = {'anos': [0,1,...], 'mfccs': [[...],[...],...]}

Sprechernamen sind die Namen der Verzeichnisse, wo die entsprechenden Sprachdateien von den jeweiligen Personen drinnen sind.

Öffnen der Daten:
with open('speakers.pkl', 'rb') as handle:
    speakers = pickle.load(handle)

In speaker_models.pkl sind erste anhand von speakers.pkl trainierte GMMs. Siehe mk_speakermodels.


## UBM trainieren ##

# imports
import numpy as np
from sklearn.mixture import GMM

# Daten fürs trainieren vorbereiten
data = np.load("data.npz")
ubm_anos = data["ubm_anos"]
ubm_mfccs = data["ubm_mfccs"]
# ids sind die Indexe dort, wo die annotations==1 sind
ids = [i for i,val in enumerate(ubm_anos) if val==1]

# GMM trainieren
g = GMM(...)
g.fit(ubm_mfccs[ids])
