import helpers
import os
import logging
from sklearn.tree import DecisionTreeClassifier as C
from copy import deepcopy as cp
import bob
import numpy as np


# Paths and Ressources
'''
data_dir = '/mnt/tatooine/data/'
speech_dir = data_dir+'vad_speaker_recog/TIMIT_Buckeye/vad/'
noise_dir = data_dir+'noise/noise_equal_concat/train/'
ir_dir = data_dir+'impulse_responses/16kHz/wavs16b/'
ref_noise = data_dir+'ref_pink.wav'

working_dir = '/mnt/alderaan/mlteam1/assignment1/'
normalize_dir = working_dir+'normalized/'
noise_int_dir = working_dir+'noise_int/'
noised_dir = working_dir+'noised/'
convolute_dir = working_dir+'convoluted/'
'''

#'''
# Paths and Ressources, for at home
data_dir = os.getcwd() + '/'
speech_dir = data_dir + 'FADG0/'
noise_dir = data_dir + 'train/'
noise_int_dir = data_dir + 'noise_int/'
ir_dir = data_dir + 'impulse_responses/16kHz/wavs/'
ref_noise = data_dir + 'ref_pink.wav'

working_dir = data_dir
normalize_dir = working_dir + 'normalized/'
noised_dir = working_dir + 'noised/'
convolute_dir = working_dir + 'convoluted/'
#'''


# Configure logging
logging_path = working_dir + 'debug.log'
logging.basicConfig(filename=logging_path, level=logging.DEBUG, format='%(asctime)s %(message)s')

'''
logging.info('Starting normalizing speech files...')
for subdir, dirs, files in os.walk(speech_dir):
    # iterate over all wavs in the subdirectories
    wavs = helpers.filtr(files, 'wav')
    for w in wavs:
        speech_file = subdir+'/'+w
        result_file = speech_file.replace(speech_dir, normalize_dir)
        logging.debug('speech_file='+speech_file)
        logging.debug('result_file='+speech_file)
        helpers.normalize_vs_noise(speech_file, ref_noise, result_file)
logging.info('Finished normalizing speech files!')



logging.info('')
logging.info('')
logging.info('Starting transformation of 64bit float noise files to 16bit int...')
# Transform 64bit float noise files to 16bit int
for subdir, dirs, files in os.walk(noise_dir):
    # iterate over all wavs in the subdirectories
    wavs = helpers.filtr(files, 'wav')
    for w in wavs:
        float_wav_file = subdir+'/'+w
        int_wav_file = float_wav_file.replace(noise_dir, noise_int_dir)
        logging.debug('float_wav_file ='+float_wav_file)
        logging.debug('int_wav_file='+int_wav_file)
        helpers.float2int(float_wav_file, int_wav_file)
logging.info('Finished transformation of data type!')


logging.info('')
logging.info('')
logging.info('Starting mixing speech files with random noise...')
# Mix speech files with random noise
for subdir, dirs, files in os.walk(normalize_dir):
    # iterate over all wavs in the subdirectories
    wavs = helpers.filtr(files, 'wav')
    for w in wavs:
        speech_file = subdir+'/'+w
        result_file = speech_file.replace(normalize_dir, noised_dir)
        # pick random noise file, must be 16bit int, 64bit int doesn't work!
        noise_candidates = os.listdir(noise_int_dir)
        noise_file = noise_int_dir+random.choice(noise_candidates)
        logging.debug('speech_file='+speech_file)
        logging.debug('noise_file ='+noise_file)
        logging.debug('result_file='+result_file)
        helpers.add_noise(speech_file, noise_file, result_file)
logging.info('Finished mixing speech files!')


logging.info('')
logging.info('')
logging.info('Starting convoluting noised speech with random IR...')
# Convolute noised speech files with random impulse reaction
for subdir, dirs, files in os.walk(noised_dir):
    # iterate over all wavs in the subdirectories
    wavs = helpers.filtr(files, 'wav')
    for w in wavs:
        speech_file = subdir+'/'+w
        result_file = speech_file.replace(noised_dir, convolute_dir)
        # pick random impulse reaction file
        ir_file = ir_dir+random.choice(os.listdir(ir_dir))
        logging.debug('speech_file='+speech_file)
        logging.debug('ir_file    ='+ir_file)
        logging.debug('result_file='+speech_file)
        helpers.convolute_ir(speech_file, ir_file, result_file)
logging.info('Finished convoluting speech!')


logging.info('')
logging.info('')
logging.info('Starting extraction of speech MFCCS & adaption of annotations...')

data_names = []
data_anos = []
data_mfccs = []
for subdir, dirs, files in os.walk(speech_dir):
    # iterate over all wavs in the subdirectories
    annotations = helpers.filtr(files, 'ano')
    for a in annotations:
        ano_file = subdir + '/' + a
        copy_dest = ano_file.replace(speech_dir, convolute_dir)
        logging.debug('ano_file =' + ano_file)
        logging.debug('copy_dest=' + copy_dest)

        # Read and process annotations file to the convoluted wav-files
        winlen = 0.025
        winstep = 0.01
        frame_annotations = helpers.process_ano(ano_file, winlen, winstep)

        # Write frame annotations to file
        f2 = open(copy_dest, 'w')
        for ano in frame_annotations:
            f2.writelines(ano + '\n')
        f2.close()

        # Calculate MFCCS for file
        frame_mfccs = helpers.process_speech(copy_dest.replace('.ano', '.wav'), winlen, winstep)

        # Append all data to array with filename as identifier
        data_names.append(copy_dest.replace('.ano', ''))
        data_anos += map(float, frame_annotations)
        for mfcc in frame_mfccs:
            data_mfccs.append(mfcc[1:13])

        # if length is not the same drop one anotation
        if len(data_mfccs) < len(data_anos):
            data_anos.pop()

logging.info('Finished copying speech annotations!')

a = np.asmatrix(data_mfccs)
mean_mfcc = a.mean(axis=0)  # mfccs means
mean_matrix = np.repeat(mean_mfcc[:, np.newaxis], len(data_mfccs), 0)
data_mfccs -= mean_matrix  # substract the means

# save MFCCs
np.savez(convolute_dir + 'data', data_names=data_names, data_anos=data_anos, data_mfccs=data_mfccs)
logging.info('Saved speech MFCCS!')

'''

folds = 10

logging.info('\n')
logging.info('Doing cross-validation splits.')
(samples, labels, kf, final_test_data) = helpers.crossval_split(convolute_dir + 'data.npz', folds)


gmm = bob.machine.GMMMachine(2,3)
sample = np.array([0.5, 4.5, 1.5])
m = bob.machine.IVectorMachine(gmm, 2)
m.t = np.array([[1.,2],[4,1],[0,3],[5,8],[7,10],[11,1]])
m.sigma = np.array([1.,2.,1.,3.,2.,4.])
gs = bob.machine.GMMStats(2,3)
gmm.acc_statistics(sample, gs)
w_ij = m.forward(gs)
print w_ij
print m.ubm

# logging.info('\n')
# logging.info('Starting cross validation training and testing...')
#
# tmp = {'avg_accuracy': 0, 'avg_train_time': 0, 'avg_test_time': 0, 'avg_train_samples': 0, 'avg_test_samples': 0}
#
# methods = {
#     # 'c_support_vector': {'avg_accuracy': 0, 'avg_train_time': 0, 'avg_test_time': 0},
#     # 'random_forests': {(10,): cp(tmp), (15,): cp(tmp), (20,): cp(tmp)},  # for each parameter config one conclusion
#     'knn': {(5,): cp(tmp)},
#     #'knn': {(3,): cp(tmp),(5,): cp(tmp), (10,): cp(tmp)},
#     #'naive_bayes': {(): cp(tmp)},
#     'adaboost': {(1,50): cp(tmp), (2,50): cp(tmp), (3,60): cp(tmp)}
# }
#
# for m in methods:
#     # run for each parameter configuration
#     for p in methods[m]:
#         results = helpers.apply_ml(kf, samples, labels, m, p)
#         for i in range(folds):
#             logging.info('------ ' + m + ' ----------')
#             logging.info('Parameter Setting: '+str(p))
#             logging.info('Confusion Matrix: ')
#             logging.info(str(results[i]['confusion_matrix'][0]))
#             logging.info(str(results[i]['confusion_matrix'][1]))
#             logging.info('Accuracy: ' + str(results[i]['accuracy']))
#             for key in methods[m][p]:
#                 methods[m][p][key] += results[i][key.replace('avg_','')]
#             logging.info('\n')
#             # Test
#             print helpers.test_performance(results[i]['clf'],final_test_data)
#         for key in methods[m][p]:
#             methods[m][p][key] /= float(folds)
#
#
# logging.info('')
# logging.info('Finished cross validation training and testing!')
#
#
#
#
# logging.info('\n')
# logging.info('Conclusion:')
# logging.info('Average Accuracies: ')
# for m in methods:
#     for p in methods[m]:
#         logging.info(m+' with '+str(p)+': '+str(methods[m][p]['avg_accuracy']))
# logging.info('')
# logging.info('Average training times for '+str(methods[m][p]['avg_train_samples'])+' samples: ')
# for m in methods:
#     for p in methods[m]:
#         logging.info(m+' with '+str(p)+': '+str(methods[m][p]['avg_train_time']))
# logging.info('')
# logging.info('Average classify times for '+str(methods[m][p]['avg_test_samples'])+' samples: ')
# for m in methods:
#     for p in methods[m]:
#         logging.info(m+' with '+str(p)+': '+str(methods[m][p]['avg_test_time']))
#
#
#
# logging.info('\n')
# logging.info('Finished the whole script successfully!')
