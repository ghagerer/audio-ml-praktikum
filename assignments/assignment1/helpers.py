'''
import librosa as lr
'''
from features import mfcc
import scipy.io.wavfile as wav
import audiotools
import math
import random
import pydub
import os
from sklearn import svm
from sklearn.metrics import confusion_matrix
from sklearn import cross_validation
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
import numpy as np
import math
import time
from concurrent.futures import ThreadPoolExecutor
import threading
import multiprocessing
from sklearn.tree import DecisionTreeClassifier


np.set_printoptions(threshold=np.nan)


def normalize_vs_noise(speech_file, noise_file, result_file):
    """Normalizes gain of speech_file wrt noise_file. Saves to result_file."""

    # Reference gain calculation for normalization
    pink = audiotools.open(noise_file)
    ref_vrms = list(audiotools.calculate_replay_gain([pink]))
    ref_val = ref_vrms[-1][1]

    # Gain calculation of example vad file
    vad1 = audiotools.open(speech_file)
    file1_vrms = list(audiotools.calculate_replay_gain([vad1]))
    file1_val = file1_vrms[-1][1]

    # Normalization of example vad file
    (rate, sig) = wav.read(speech_file)
    sig_normalized = np.asarray(sig*math.pow(10, (-(ref_val-file1_val)/20)), dtype=np.int16)

    # write wav file to disk
    makedirs(result_file)
    wav.write(result_file, rate, sig_normalized)


def add_noise(speech_file, noise_file, result_file):
    """Adds noise from noise_file to speech_file. Saves to result_file."""

    # pick random SNR = signal to noise ratio
    ratio = random.randrange(-2, 21)

    # signals from normalized wav
    signal = pydub.AudioSegment.from_wav(speech_file)

    # choose a random noice file
    # whichnoise = random.choice(noises)
    #whichnoise = noise_file

    # get signals from the random noice file
    noise = pydub.AudioSegment.from_wav(noise_file)

    # get the lenth of the normalized and noise .wav in seconds
    lengthsignal = signal.duration_seconds
    lengthnoise = noise.duration_seconds

    # generate noise
    # get a random noise part from within the noise file with the length of the normalized signals
    start_noise = random.randrange(0, math.ceil(lengthnoise-lengthsignal))
    noise_piece = noise[start_noise*1000:(start_noise+lengthsignal)*1000]

    # what do fade in and fade out do?
    noise_fade = noise_piece.fade_in(63).fade_out(63)

    # apply SNR from above to normalized .wav, boost it/ make it louder
    signal_boost = signal.apply_gain(ratio)  # same as signal-ratio?

    # overlay boosted normalized .wav with noise
    mixed_sound = signal_boost.overlay(noise_fade)

    # save the result
    makedirs(result_file)
    handle = mixed_sound.export(result_file, format="wav")
    handle.close()


def convolute_ir(speech_file, ir_file, result_file):
    """ Convolutes speech_file with impulse response from ir_file.
    Saves to result_file
    """

    # get a random impulse sound file
    # whichimpulse = random.choice(impulses)

    # get sampling rate and signals from impulse sound file
    (rate_imp, sig_imp) = wav.read(ir_file)

    # change data type to float64, more precise
    # sig_imp = np.asarray(sig_imp, dtype=np.float64)
    sig_imp = sig_imp.astype(np.float64)/65536.0

    # peak-normalization, sig_imp/maximum
    # maximum = max(sig_imp)
    # sig_imp_pn=np.asarray([x / maximum for x in sig_imp], dtype=np.float64)
    sig_imp_pn = sig_imp / max(sig_imp)

    # get noised sound file from before
    (rate_mixsound, sig_mixsound) = wav.read(speech_file)
    n = len(sig_mixsound)

    # take more precise data type for noised sound file
    # sig_mixsound=np.asarray(sig_mixsound, dtype=np.float64)
    sig_mixsound = sig_mixsound.astype(np.float64)/65536.0

    # convolve noised sound file with impulse reaction
    convolved_sample = np.convolve(sig_imp_pn, sig_mixsound)

    # save convolved sample, change data type to int16
    makedirs(result_file)
    wav.write(result_file, rate_mixsound, convolved_sample[:n])


def process_ano(ano_file, winlen, winstep):
    """"""
    annotation = []
    with open(ano_file, 'r') as f:
        annotation = f.readlines()
    annotation = map(int, annotation)
    annotation_mean=[]
    annotation_mean_sub=[]
    for el in annotation:
        annotation_mean_sub.append(el)
        if len(annotation_mean_sub)==winlen*16000:
            annotation_mean.append(math.ceil(np.average(annotation_mean_sub)))
            annotation_mean_sub=annotation_mean_sub[int(winstep*16000):]
    annotation_mean.append(math.ceil(np.average(annotation_mean_sub)))
    annotation_mean = map(str, annotation_mean)
    return annotation_mean


def process_speech(speech_file, winlen, winstep):
    (rate,sig)=wav.read(speech_file)
    return mfcc(sig,rate,winlen,winstep)


def makedirs(file_path):
    """Takes a path of a file and creates directory structure if not existent"""
    if not os.path.exists(os.path.dirname(file_path)):
        os.makedirs(os.path.dirname(file_path))


def filtr(files, filetype):
    """Filters a file list by the given filename ending to be accepted"""
    return filter(lambda d: 1 if d.endswith(filetype) else 0, files)


def float2int(float_wav_path, int_wav_path):
    """Transforms a 64bit float wav file to a 16bit int file"""
    (rate, sig) = wav.read(float_wav_path)
    sig = np.asarray(sig*65536.0, dtype=np.int16)
    wav.write(int_wav_path, rate, sig)


def int2float(int_wav_path, float_wav_path):
    """Transforms a 16bit int wav file to a 64bit float file"""
    (rate, sig) = wav.read(int_wav_path)
    sig = np.asarray(sig, dtype=np.float64)/65536.0
    wav.write(float_wav_path, rate, sig)


def zero_weight(y_train):
    zeros = len(y_train) - sum(y_train)
    factor_zeros = len(y_train)/float(zeros)
    return factor_zeros*1.0/100


def crossval_split(data_dir, folds, max_samples=200000):
    """Get data and train test splits for cross-validation"""

    data_load = np.load(data_dir)
    labels = data_load['data_anos']
    samples = data_load['data_mfccs']

    # print len(labels), len(samples)

    ones = int(sum(labels))
    zeros = int(len(labels) - ones)

    # get indices from where labels are 1 and 0
    ones_idx = np.where(labels)[0]
    zeros_idx = np.where(abs(labels-1))[0]

    # generate permutation
    l = len(ones_idx)
    perm_ones = random.sample(range(0,l), l)
    l = len(zeros_idx)
    perm_zeros = random.sample(range(0,l), l)

    # apply permutation to indexes
    ones_idx = [ones_idx[i] for i in perm_ones]
    zeros_idx = [zeros_idx[i] for i in perm_zeros]

    n_ones = int(math.floor(max_samples*0.5))
    n_zeros = int(math.floor(max_samples*0.5))

    # don't take too much samples, drop some when too many
    if zeros <= n_zeros:  # for local use only
        rest_ones_idx = ones_idx[zeros:]
        ones_idx = ones_idx[:zeros]
        # since no zeros are dropped there are no left for final testing
        rest_zeros_idx = []
    else:  # for server use
        rest_ones_idx = ones_idx[n_ones:]
        rest_zeros_idx = ones_idx[n_zeros:]
        ones_idx = ones_idx[:n_ones]
        zeros_idx = zeros_idx[:n_zeros]

    # put labels together
    zeros = np.zeros(len(zeros_idx))
    ones = np.ones(len(ones_idx))
    val_labels = np.append(zeros,ones)

    # put sample data together
    samples_zeros = [samples[i] for i in zeros_idx]
    samples_ones = [samples[i] for i in ones_idx]
    val_samples = np.concatenate((samples_zeros, samples_ones), axis=0)

    # put the rest together. this forms the final test data,
    # which can be applied after parameter optimization
    rest_zeros = np.zeros(len(rest_zeros_idx))
    rest_ones = np.ones(len(rest_ones_idx))
    final_labels = np.append(rest_zeros,rest_ones)
    rest_samples_zeros = [samples[i] for i in rest_zeros_idx]
    rest_samples_ones = [samples[i] for i in rest_ones_idx]
    if len(rest_zeros)>0:
        final_samples = np.concatenate((rest_samples_zeros, rest_samples_ones), axis=0)
    else:
        final_samples = np.array(rest_samples_ones)
    l = len(final_samples)
    perm_final = random.sample(range(0,l), l)
    # final_test_data = {
    #     'labels': final_labels[:max_samples],
    #     'samples': final_samples[:max_samples]
    # }

    final_test_data={
        'labels':[final_labels[i] for i in perm_final][:max_samples],
        'samples':[final_samples[i] for i in perm_final][:max_samples]
    }

    #print len(samples)
    #print len(labels)

    # Perform splits for cross validation
    kf = cross_validation.KFold(len(val_samples), n_folds=folds,shuffle=True)
    return val_samples, val_labels, kf, final_test_data


def apply_ml(kf, samples, labels, method, params=()):
    """Apply cross-validation according to kf (consecutive train test splitting)"""

    kf = list(kf)

    # define function for each step to parallelize it later
    def train_and_test(i):
        train_idx = kf[i][0]
        test_idx = kf[i][1]

        # calculate weights, for each sample
        # weights=(np.ones(len(y_train))-y_train)+np.ones(len(y_train))*1.1*(1-(sum(y_train)/len(y_train)))
        # factor_ones = 1
        # factor_zeros = helpers.zero_weight(y_train)
        # tmp1 = y_train*factor_ones
        # tmp2 = abs(y_train-1)*factor_zeros
        # weights = (tmp1+tmp2)

        # init classificator, dependend on given method
        clf = {
            'c_support_vector': lambda: svm.SVC(*params),
            'random_forests': lambda: RandomForestClassifier(*params),
            'knn': lambda: KNeighborsClassifier(*params),
            'naive_bayes': lambda: GaussianNB(*params),
            'adaboost': lambda: AdaBoost(*params)
        }[method]()

        # split MFCCs = samples
        X_train, X_test = samples[train_idx], samples[test_idx]

        # split annotations = labels
        y_train, y_test = labels[train_idx], labels[test_idx]

        # measure time for training and classifying
        start = time.time()

        # train classificator
        clf.fit(X_train, y_train)
        train_time = time.time() - start

        # apply trained classificator on test data
        start = time.time()
        y_pred = clf.predict(X_test)
        test_time = time.time() - start

        # get confusion matrix
        cm = confusion_matrix(y_test, y_pred)

        # normalize confusion matrix
        cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        # write to protected shared resource, i.e. results array
        return {
            'accuracy': accuracy_score(y_test, y_pred),
            'confusion_matrix': cm_normalized,
            'train_time': train_time,
            'test_time': test_time,
            'train_samples': len(y_train),
            'test_samples': len(y_test),
            'clf':clf,
        }

    # parallelize
    executor = ThreadPoolExecutor(multiprocessing.cpu_count())
    tuple_mapper = lambda i: (i, train_and_test(i))

    # execute parallelization and save as dictionary
    results = dict(executor.map(tuple_mapper, range(len(kf))))

    return results


def AdaBoost(d=2, n=50):
    return AdaBoostClassifier(DecisionTreeClassifier(max_depth=d), n_estimators=n)

def test_performance(clf,finaldata):
    y_pred = clf.predict(finaldata['samples'])
    a= accuracy_score(finaldata['labels'], y_pred)
    # get confusion matrix
    cm = confusion_matrix(finaldata['labels'], y_pred)
    # normalize confusion matrix
    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    return a, cm_normalized