from features import mfcc
from features import logfbank
import scipy.io.wavfile as wav
import audiotools
import math
import random
import numpy as np
import pydub
import librosa as lr

(rate, sig) = wav.read('FADG0/SA1_631.wav')
print 'Librosa: ', lr.core.get_duration(sig)

# Paths and Ressources
noisepath = "train/"
impulsepath = "impulse_responses/16kHz/wavs/"
noises = ["pocketMovement2","public_transportation2","super_market_mall2","foot_steps2"]
impulses = ["htc_desk.wav","htc_leather_bag.wav","htc_pocket_down.wav","htc_pocket_up.wav","lg_desk.wav","lg_leather_bag.wav","lg_pocket_down.wav","lg_pocket_up.wav","s1_desk.wav","s1_leather_bag.wav","s1_pocket_down.wav","s1_pocket_up.wav","s3_desk.wav","s3_leather_bag.wav","s3_pocket_down.wav","s3_pocket_up.wav"]
data_path = '/mnt/tatooine/data/'


# Reference gain calculation for normalization
pink = audiotools.open("ref_pink.wav")
ref_Vrms = audiotools.calculate_replay_gain([pink])
ref_val = 0
for i in ref_Vrms:
    print i
    ref_val = i[1]

# Gain calculation of example vad file
vad1 = audiotools.open('FADG0/SA1_631.wav')
file1_Vrms = audiotools.calculate_replay_gain([vad1])
file1_val = 0
for i in file1_Vrms:
    print i
    file1_val = i[1]

# Normalization of example vad file
(rate, sig) = wav.read('FADG0/SA1_631.wav')
sig_normalized = np.asarray(sig*math.pow(10, (-(ref_val-file1_val)/20)), dtype=np.int16)
wav.write('normal.wav', rate, sig_normalized)
# Test if normalization worked
normal = audiotools.open('normal.wav')
normal_Vrms = audiotools.calculate_replay_gain([normal])
for i in normal_Vrms:
    print i


# Mixing with noise (including SNR and fading)

# pick random SNR = signal to noise ratio
ratio = random.randrange(-2,21)

# signals from normalized wav
signal = pydub.AudioSegment.from_wav("normal.wav")

# choose a random noice file
whichnoise = random.choice(noises)

# get signals from the random noice file
noise = pydub.AudioSegment.from_wav(noisepath+whichnoise+".wav")

# get the lenth of the normalized and noise .wav in seconds
lengthsignal = signal.duration_seconds
lengthnoise = noise.duration_seconds

# generate noise
# get a random noise part from within the noise file with the length of the normalized signals
start_noise = random.randrange(0, math.ceil(lengthnoise-lengthsignal))
noise_piece = noise[start_noise*1000:(start_noise+lengthsignal)*1000]

# what do fade in and fade out do?
noise_fade = noise_piece.fade_in(63).fade_out(63)

# apply SNR from above to normalized .wav, boost it/ make it louder
signal_boost = signal.apply_gain(ratio)  # same as signal-ratio?

# overlay boosted normalized .wav with noise
mixedSound = signal_boost.overlay(noise_fade)

# save the result
mixedSound.export("mixed.wav", format="wav")


# Convolution with impulse response

# get a random impulse sound file
whichimpulse = random.choice(impulses)

# get sampling rate and signals from impulse sound file
(rate_imp, sig_imp) = wav.read(impulsepath+whichimpulse)

# change data type to float64, more precise
#sig_imp = np.asarray(sig_imp, dtype=np.float64)
sig_imp = sig_imp.astype(np.float64)/65536.0

# peak-normalization, sig_imp/maximum
# maximum = max(sig_imp)
#sig_imp_pn=np.asarray([x / maximum for x in sig_imp], dtype=np.float64)
sig_imp_pn = sig_imp / max(sig_imp)

# get noised sound file from before
(rate_mixsound, sig_mixsound) = wav.read("mixed.wav")
n = len(sig_mixsound)

# take more precise data type for noised sound file
#sig_mixsound=np.asarray(sig_mixsound, dtype=np.float64)
sig_mixsound = sig_mixsound.astype(np.float64)/65536.0

# convolve noised sound file with impulse reaction
convolved_sample = np.convolve(sig_imp_pn, sig_mixsound)

# save convolved sample, change data type to int16
wav.write("final.wav", rate_mixsound, convolved_sample[:n])


# Calculation of MFCC features with standard parameters ,winlen=0.025,winstep=0.01,numcep=13,nfilt=26,nfft=512,lowfreq=0,highfreq=None,preemph=0.97,ceplifter=22,appendEnergy=True):
mfcc_feat = mfcc(convolved_sample,rate_mixsound)
print len(mfcc_feat[1]), len(mfcc_feat)
# fbank_feat = logfbank(sig,rate)

# Reading VAD annotation
annotation = []
with open('FADG0/SA1_631.ano', 'r') as f:
    annotation = f.readlines()
annotation = map(int, annotation)
