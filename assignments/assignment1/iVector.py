import numpy as np
import bob
import time
import math
import itertools

# Takes a feature vector of variable length and splits it into utterances which are then scored
# against a personal model. The scores are compared with thresholds to make a 0/1 prediction.
# Predictions and ground truth (averaged over frames in utterance) are returned.
def make_predictions_tv(ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine, test_samples, \
    test_vad_ano, test_labels, frames_per_utterance, norm_type, imp_sqrt_diag_covar_mat, impostor_ivectors, \
    mean_impostor_ivector, impostor_mean, impostor_stdv, positives_scores, negatives_scores, \
    speaker_wccn_lda_ivector, debug_level):

    # split test data into short utterances and score those
    num_utt = int(round(len(test_samples)/frames_per_utterance))
    for utt_count in range(0, num_utt):
        t0 = time.time();
        data = test_samples[utt_count*frames_per_utterance \
            : (utt_count+1)*frames_per_utterance]
        data_ano = test_vad_ano[utt_count*frames_per_utterance \
            : (utt_count+1)*frames_per_utterance]
        if float(sum(data_ano))/len(data_ano) < 0.5:
            continue
        tmp_std = np.std(data[data_ano==1], 0)
        data -= np.mean(data[data_ano==1], 0)
        data /= tmp_std
        wccn_lda_ivector = compute_compensated_ivector(data[data_ano==1], ubm, ivec_machine, \
            whitening_machine, lda_machine, wccn_machine)

        #Compute score of projected vector against personal model
        score_norm = score_vector(wccn_lda_ivector, speaker_wccn_lda_ivector, norm_type, \
            impostor_mean, impostor_stdv, impostor_ivectors, mean_impostor_ivector, \
            imp_sqrt_diag_covar_mat, debug_level)

        t1 = time.time()
        if debug_level > 1 and utt_count == 0:
            print 'Scoring first test utterance took %fs' % (t1-t0)

        labels = test_labels[utt_count*frames_per_utterance \
            : (utt_count+1)*frames_per_utterance]
        #average over all frames in this utterance
        label_should = round(np.mean(labels[data_ano==1]))

        #for further performance evaluation
        if label_should==1:
            positives_scores.append(score_norm)
        else:
            negatives_scores.append(score_norm)


#Score a given test vector against the personal model and normalize according to norm_type
def score_vector(test_ivector, speaker_ivector, norm_type, z_impostor_mean, z_impostor_stdv, \
		impostor_ivectors, mean_impostor_ivector, imp_sqrt_diag_covar_mat, debug_level):

	if norm_type == 'znorm':
		score = cosine_distance(speaker_ivector, test_ivector)
		score_norm = (score-z_impostor_mean)/z_impostor_stdv
	elif norm_type == 'normcos':
		#see: "Cosine Similarity Scoring without Score Normalization Techniques",
		#      Dehak et al., 2010

		norm_speaker = np.linalg.norm(np.dot(imp_sqrt_diag_covar_mat, np.atleast_2d(speaker_ivector).T))
		norm_test = np.linalg.norm(np.dot(imp_sqrt_diag_covar_mat, np.atleast_2d(test_ivector).T))

		dist1 = np.subtract(speaker_ivector, mean_impostor_ivector)
		dist2 = np.subtract(test_ivector, mean_impostor_ivector)

		score_norm = np.dot(dist1, dist2) / np.dot(norm_speaker, norm_test)
		#print 'score_norm is %s' % (score_norm)
	elif norm_type == 'snorm':
		#see: "Unsupervised Speaker Adaptation based on the Cosine Similarity for
		#      Text-Independent Speaker Verification", Shum et al., 2010

		score = cosine_distance(speaker_ivector, test_ivector)

		(t_impostor_mean, t_impostor_stdv) = \
			get_impostor_distribution(impostor_ivectors, test_ivector, debug_level)

		score_norm = \
			(score-z_impostor_mean)/z_impostor_stdv + \
			(score-t_impostor_mean)/t_impostor_stdv

	return score_norm

# Takes a feature vector of variable length and transforms it into a channel-compensated ivector.
def compute_compensated_ivector(samples, ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine):
	#Project test vector (~MAP-adapt UBM)
	gmmstats = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
	ubm.acc_statistics(samples, gmmstats)
	projected_ivector = ivec_machine.forward(gmmstats)
	whitened_ivector = whitening_machine.forward(projected_ivector)
	normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
	lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
	lda_machine(normalized_ivector, lda_ivector)
	wccn_lda_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
	wccn_machine(lda_ivector, wccn_lda_ivector)

	return wccn_lda_ivector

# Get a distribution of how impostors score against a given model. This serves for z-normalization.
def get_impostor_distribution(impostor_ivectors, speaker_wccn_lda_ivector, debug_level):
	scores_impostors = []

	t0 = time.time()
	for wccn_lda_ivector in impostor_ivectors:
		score = cosine_distance(wccn_lda_ivector, speaker_wccn_lda_ivector)
		scores_impostors.append(score)

	impostor_mean = np.mean(scores_impostors)
	impostor_stdv = np.std(scores_impostors)

	t1 = time.time()
	if debug_level > 1:
		print 'Computing the impostor distribution took %fs' % (t1-t0)

	return (impostor_mean, impostor_stdv)

def get_impostor_ivectors(ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine, \
	impostor_samples, impostor_vad_anno, frames_per_utterance):

	ivecs = []

	for (speaker_samples, speaker_ano) in zip(impostor_samples, impostor_vad_anno):
		speaker_ivecs = []
		num_utt = int(round(len(speaker_samples)/frames_per_utterance))
		for utt_count in range(0, num_utt):
			data = speaker_samples[utt_count*frames_per_utterance \
				: (utt_count+1)*frames_per_utterance]
			data_ano = speaker_ano[utt_count*frames_per_utterance \
				: (utt_count+1)*frames_per_utterance]
			if float(sum(data_ano))/len(data_ano) >= 0.5:
				tmp_std = np.std(data[data_ano==1], 0)
				data -= np.mean(data[data_ano==1], 0)
				data /= tmp_std
				wccn_lda_ivector = compute_compensated_ivector(data[data_ano==1], ubm, ivec_machine, \
					whitening_machine, lda_machine, wccn_machine)
				speaker_ivecs.append(wccn_lda_ivector)
		avg_wccn_lda_ivector = np.mean(speaker_ivecs)
		if avg_wccn_lda_ivector.shape[0] != speaker_ivecs[0].shape[0]:
			print 'ALARM!!!!!' #just to be sure
			print 'shape1: %s, shape2: %s' % (avg_wccn_lda_ivector.shape, speaker_ivecs[0].shape)
		ivecs.append(avg_wccn_lda_ivector)
	return ivecs


def cosine_distance(a, b):
	if len(a) != len(b):
		raise ValueError, "a and b must be same length"
	numerator = sum(tup[0] * tup[1] for tup in itertools.izip(a,b))
	denoma = sum(avalue ** 2 for avalue in a)
	denomb = sum(bvalue ** 2 for bvalue in b)
	result = numerator / (math.sqrt(denoma)*math.sqrt(denomb))
	return result


def init_ivector_machine(ivec_machine_load_path, ubm, ivector_dim, proj_train_features_flat, ir_name, \
	results_path, debug_level):
	if ivec_machine_load_path:
		ivec_machine = bob.machine.IVectorMachine(bob.io.HDF5File(ivec_machine_load_path, 'r'))
		ivec_machine.ubm = ubm
		print 'I-Vector machine loaded.'
	else:
		print 'Starting I-Vector machine training...'
		t0 = time.time()
		ivec_machine = bob.machine.IVectorMachine(ubm, ivector_dim)
		ivec_machine.variance_threshold = 1e-5
		ivec_trainer = bob.trainer.IVectorTrainer(update_sigma=True, max_iterations=10)
		ivec_trainer.initialize(ivec_machine, proj_train_features_flat)
		ivec_trainer.train(ivec_machine, proj_train_features_flat)
		print 'I-Vector machine training done.'
		t1 = time.time()
		if debug_level > 0:
			print 'Training took %fs' % (t1-t0)

		ivec_file = 'ivec_machine_' + str(ivector_dim) + '_' + ir_name + '.hdf5'
		ivec_machine.save(bob.io.HDF5File(results_path + ivec_file, 'w' ))
		print 'I-Vector machine saved to disk.'
	return ivec_machine


def init_whitening_machine(white_machine_load_path, ivec_machine, proj_train_features_flat, ir_name, \
	results_path, debug_level):
	if white_machine_load_path:
		whitening_machine = bob.machine.LinearMachine(bob.io.HDF5File(white_machine_load_path, 'r'))
		print 'Whitening machine loaded.'
	else:
		print 'Starting whitening enroller training...'
		t0 = time.time()
		ivectors_matrix  = []

		for gmmstats in proj_train_features_flat:
			projected_ivector = ivec_machine.forward(gmmstats)
			ivectors_matrix.append(projected_ivector)

		ivectors_matrix = np.vstack(ivectors_matrix)
		whitening_machine = bob.machine.LinearMachine(ivectors_matrix.shape[1],ivectors_matrix.shape[1])
		t = bob.trainer.WhiteningTrainer()
		t.train(whitening_machine, ivectors_matrix)
		print 'Whitening enroller training done.'
		t1 = time.time()
		if debug_level > 0:
			print 'Training took %fs' % (t1-t0)

		white_file = 'white_machine_' + ir_name + '.hdf5'
		whitening_machine.save(bob.io.HDF5File(results_path + white_file, 'w' ))
		print 'Whitening machine saved to disk.'
	return whitening_machine


def init_lda_machine(lda_machine_load_path, proj_train_features, lda_subspace_dim, ir_name, results_path, \
	debug_level):
	if lda_machine_load_path:
		lda_machine = bob.machine.LinearMachine(bob.io.HDF5File(lda_machine_load_path, 'r'))
		print 'LDA machine loaded.'
	else:
		print 'Starting LDA projector training...'
		t0 = time.time()
		t = bob.trainer.FisherLDATrainer(strip_to_rank=False)
		lda_machine, __eig_vals = t.train(proj_train_features)
		# resize the machine if desired
		lda_machine.resize(lda_machine.shape[0], lda_subspace_dim)
		print 'LDA projector training done.'
		t1 = time.time()
		if debug_level > 0:
			print 'Training took %fs' % (t1-t0)

		lda_file = 'lda_machine_' + str(lda_subspace_dim) + '_' + ir_name + '.hdf5'
		lda_machine.save(bob.io.HDF5File(results_path + lda_file, 'w' ))
		print 'LDA machine saved to disk.'
	return lda_machine


def init_wccn_machine(wccn_machine_load_path, proj_train_features, ir_name, results_path, debug_level):
	if wccn_machine_load_path:
		wccn_machine = bob.machine.LinearMachine(bob.io.HDF5File(wccn_machine_load_path, 'r'))
		print 'WCCN machine loaded.'
	else:
		print 'Starting WCCN projector training...'
		t0 = time.time()
		t = bob.trainer.WCCNTrainer()
		wccn_machine = t.train(proj_train_features)
		print 'WCCN projector training done.'
		t1 = time.time()
		if debug_level > 0:
			print 'Training took %fs' % (t1-t0)

		wccn_file = 'wccn_machine_' + ir_name + '.hdf5'
		wccn_machine.save(bob.io.HDF5File(results_path + wccn_file, 'w' ))
		print 'WCCN machine saved to disk.'
	return wccn_machine
