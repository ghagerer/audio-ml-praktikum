import os
from enum import Enum

class Config:
    # PATH CONFIGS SSH or LOCAL
    type='local'
    set="Berlin"
    code_dir = os.path.abspath(os.getcwd()) + '/'
    data_dir = code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Berlin-EMO')
    speech_dir = data_dir + 'wav/'

    model_dir = code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Models')
    support_dir = code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Support')
    noise_dir = support_dir + 'train/'
    noise_int_dir = support_dir + 'noise_int/'
    ir_dir = support_dir + 'impulse_responses/16kHz/wavs/'
    ref_noise = support_dir + 'ref_pink.wav'
    label_dir=speech_dir.replace('wav','labels/CEICES')+'/'+'chunk_labels_4cl_aibo_chunk_set.txt'

    working_dir = data_dir
    normalize_dir = working_dir + 'normalized/'
    noised_dir = working_dir + 'cleanData/'
    convolute_dir = working_dir + 'channeledData/'
    channeledData= 'channeledData'
    cleanData= 'cleanData'
    ubmData= 'ubmData'


    # EMOTION CONFIGS

    class emotionCodes(Enum):
        Happy = 1
        Bored = 2
        Neutral = 3
        Sad = 4
        Disgusted = 5
        Angry = 6
        Anxious = 7


    emotionDict = {
            # active: fast speaking rate,
            # passive: slow speaking rate,
            # normal: normal speaking rate
            'W': emotionCodes.Angry, #index-0 # active-0
            'A': emotionCodes.Anxious, #index-1 # active-0
            'L': emotionCodes.Bored , #index-2 # passive-2
            'E': emotionCodes.Disgusted, #index-3 # active-0
            'F': emotionCodes.Happy, #index-4 # normal-1
            'N': emotionCodes.Neutral, #index-5 # normal-1
            'T': emotionCodes.Sad #index-6 # passive-2
        }

    # COMMON CONFIGS
    num_mixtures_ubm=256 # 512
    frames_per_utterance = 256 #256 roughly equals 2.5s
    ivector_dim = 100 # nicht unter 50
    lda_subspace_dim = 100 # 200 /plda 100/200
    k_fold=4
    n_jobs_kmeans = 10
    gmm_cvtype = 'diag'
    num_features=36
    n_iter_em = 20
    winlen = 0.025
    winstep = 0.01


def setConfiguration(type,set):
    setConfigEmotion(set);
    if type=='local':
        setLocalConfig(set);
        Config.type='local'
    else:
        if type=='ssh':
            setSSHConfig(set);
            Config.type='ssh'
        else:
            raise Exception


def setLocalConfig(set):
    Config.code_dir = os.path.abspath(os.getcwd()) + '/'
    if set=='Berlin':
        Config.data_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Berlin-EMO')
    else:
        if set == 'Fau':
            Config.data_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Fau')
            Config.speech_dir = Config.data_dir + 'wav/'
            Config.label_dir= Config.speech_dir+'/'+'chunk_labels_4cl_aibo_chunk_set.txt'
        else:
            if set=='Semaine':
                Config.data_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Semaine')
            else:
                if set == 'UBM':
                    Config.data_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/UBM')
                else:
                    if set == 'Speaker':
                        Config.data_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Speaker')
                    elif set=='voxforge':
                        Config.data_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/voxubm')
                    else:
                        raise Exception
    Config.speech_dir = Config.data_dir + 'wav/'

    Config.model_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Models')
    Config.support_dir = Config.code_dir.replace('master_thesis_hanna_schaefer/SourceCode','Datasets/Support')
    Config.noise_dir = Config.support_dir + 'train/'
    Config.noise_int_dir = Config.support_dir + 'noise_int/'
    Config.ir_dir = Config.support_dir + 'impulse_responses/16kHz/wavs/'
    Config.ref_noise = Config.support_dir + 'ref_pink.wav'

    Config.working_dir = Config.data_dir
    Config.normalize_dir = Config.working_dir + 'normalized/'
    Config.noised_dir =Config.working_dir + 'cleanData/'
    Config.convolute_dir = Config.working_dir + 'channeledData/'
    Config.channeledData= 'channeledData'
    Config.cleanData= 'cleanData'
    Config.n_jobs_kmeans=1
    Config.ubmData= 'ubmData'



def setSSHConfig(set):
    Config.code_dir = os.path.abspath(os.getcwd()) + '/'
    Config.data_dir = '/mnt/tatooine/data/'
    if set=='Berlin':
        Config.speech_dir = Config.data_dir+'emotion/Berlin-EMO/wav/'
    elif set == 'Fau':
        Config.speech_dir = Config.data_dir+'emotion/FAU-Aibo-Emotion-Corpus/wav/'
        Config.label_dir= Config.speech_dir.replace('wav','labels/CEICES')+'/'+'chunk_labels_4cl_aibo_chunk_set.txt'
    elif set=='Semaine':
        Config.speech_dir = Config.data_dir+'emotion/Semaine/Sessions/'
    elif set=='UBM':
        Config.speech_dir = '/mnt/tatooine/data/vad_speaker_recog/TIMIT_Buckeye/ubm/'
    elif set=='Speaker':
        Config.speech_dir = '/mnt/tatooine/data/vad_speaker_recog/TIMIT_Buckeye/speaker_model/'
    elif set=='voxforge':
        Config.speech_dir = Config.data_dir+'vad_speaker_recog/voxforge/ubm/'
    else:
        raise Exception

    Config.noise_dir = Config.data_dir+'noise/noise_equal_concat/train/'
    Config.ir_dir = '/mnt/naboo/impulse_responses/'
    Config.ref_noise = Config.data_dir+'ref_pink.wav'

    dataset_dir= '/mnt/alderaan/schaefer/Datasets/'
    Config.support_dir = dataset_dir+'Support/'
    Config.noise_int_dir = Config.support_dir+ 'noise_int/'

    Config.model_dir = dataset_dir +'Models/'
    Config.working_dir = dataset_dir+set+'/'
    Config.normalize_dir = Config.working_dir + 'normalized/'
    Config.noised_dir = Config.working_dir + 'cleanData/'
    Config.convolute_dir = Config.working_dir + 'channeledData/'
    Config.channeledData= 'channeledData'
    Config.cleanData= 'cleanData'
    Config.ubmData= 'ubmData'

def setConfigEmotion(set):
    Config.set=set
    if set=='Berlin':
        class emotionCodesBerlin(Enum):
            Happy = 1
            Bored = 2
            Neutral = 3
            Sad = 4
            Disgusted = 5
            Angry = 6
            Anxious = 7

        emotionDictBerlin = {
                # active: fast speaking rate,
                # passive: slow speaking rate,
                # normal: normal speaking rate
                'W': emotionCodesBerlin.Angry, #index-0 # active-0
                'A': emotionCodesBerlin.Anxious, #index-1 # active-0
                'L': emotionCodesBerlin.Bored , #index-2 # passive-2
                'E': emotionCodesBerlin.Disgusted, #index-3 # active-0
                'F': emotionCodesBerlin.Happy, #index-4 # normal-1
                'N': emotionCodesBerlin.Neutral, #index-5 # normal-1
                'T': emotionCodesBerlin.Sad #index-6 # passive-2
            }

        Config.emotionCodes=emotionCodesBerlin
        Config.emotionDict=emotionDictBerlin
        Config.winlen = 0.025
        Config.winstep = 0.01
    else:
        if set == 'Fau':
            class emotionCodesFau(Enum):
                Motherese = 1
                Neutral = 2
                Empathic = 3
                Angry = 4

            label_group_fourclass = {
                'A': emotionCodesFau.Angry,  # active
                'E': emotionCodesFau.Empathic,  # active
                'N': emotionCodesFau.Neutral,  # normal
                'M': emotionCodesFau.Motherese  # normal
            }
            Config.emotionCodes=emotionCodesFau
            Config.emotionDict=label_group_fourclass
            Config.winlen = 0.025
            Config.winstep = 0.01
        else:
            if set=='Semaine':

                class emotionCodesSemaine(Enum):
                    Happy = 1
                    Relaxed = 2
                    Neutral = 3
                    Sad = 4
                    Angry = 5

                Config.emotionCodes=emotionCodesSemaine
                Config.winlen = 0.02
                Config.winstep = 0.02
            else:
                if set == "UBM":
                    Config.emotionCodes=None
                    Config.emotionDict=None
                    Config.winlen = 0.025
                    Config.winstep = 0.01
                else:
                    if set=='Speaker':
                        class speakerCodes(Enum):
                            FSJG0_f= 1
                            MAHH0= 2
                            MCCS0= 3
                            MPMB0= 4
                            MTQC0= 5
                            s01_f= 6
                            s06= 7
                            s20_f= 8
                            s32= 9
                            s37_f= 10

                        speakerDict = {
                                'FSJG0_f': speakerCodes.FSJG0_f,
                                'MAHH0': speakerCodes.MAHH0,
                                'MCCS0': speakerCodes.MCCS0,
                                'MPMB0': speakerCodes.MPMB0,
                                'MTQC0': speakerCodes.MTQC0,
                                's01_f': speakerCodes.s01_f,
                                's06': speakerCodes.s06,
                                's20_f': speakerCodes.s20_f,
                                's32': speakerCodes.s32,
                                's37_f': speakerCodes.s37_f,
                            }

                        Config.emotionCodes=speakerCodes
                        Config.emotionDict=speakerDict
                        Config.winlen = 0.025
                        Config.winstep = 0.01
                    elif set=='voxforge':
                        Config.emotionCodes=None
                        Config.emotionDict=None
                        Config.winlen = 0.025
                        Config.winstep = 0.01
                    else:
                        raise Exception
