import audiotools
import numpy as np
import scipy.io.wavfile as wav
from scipy import signal
import math
import os
import random
import pydub
from features import mfcc
from scikits import audiolab
from Configuration import Config



def makedirs(file_path):
    """Takes a path of a file and creates directory structure if not existent"""
    if not os.path.exists(os.path.dirname(file_path)):
        os.makedirs(os.path.dirname(file_path))

def normalize_gain(speech_file, reference_file, result_file):
    """Normalizes gain of speech_file wrt reference_file. Saves to result_file."""

    # Reference gain calculation for normalization
    pink = audiotools.open(reference_file)
    ref_vrms = list(audiotools.calculate_replay_gain([pink]))
    ref_val = ref_vrms[-1][1]

    # Gain calculation of example vad file
    vad1 = audiotools.open(speech_file)
    file1_vrms = list(audiotools.calculate_replay_gain([vad1]))
    file1_val = file1_vrms[-1][1]

    # Normalization of example vad file
    f = audiolab.sndfile(speech_file)
    sig = f.read_frames(f.get_nframes(), dtype=np.float64)
    f.close()
    (rate, u) = wav.read(speech_file)
    # sig_normalized = np.asarray(sig*math.pow(10, (-(ref_val-file1_val)/20)), dtype=np.float64)
    sig_normalized=sig*math.pow(10, (-(ref_val-file1_val)/20))

    # write wav file to disk
    makedirs(result_file)
    f = audiolab.sndfile(result_file, 'write', audiolab.formatinfo('wav', 'pcm16'), 1, rate)
    f.write_frames(sig_normalized)
    f.close()
    # wav.write(result_file, rate, sig_normalized)


def add_noise(speech_file, noise_file, result_file):
    """Adds noise from noise_file to speech_file. Saves to result_file."""

    # pick random SNR = signal to noise ratio
    ratio = random.randrange(-2, 21)

    # signals from normalized wav
    signal = pydub.AudioSegment.from_wav(speech_file)

    # get signals from the random noice file
    noise = pydub.AudioSegment.from_wav(noise_file)

    # get the lenth of the normalized and noise .wav in seconds
    lengthsignal = signal.duration_seconds
    lengthnoise = noise.duration_seconds

    while lengthnoise<lengthsignal:
        noise=noise+noise
        lengthsignal = signal.duration_seconds
        lengthnoise = noise.duration_seconds

    # generate noise
    # get a random noise part from within the noise file with the length of the normalized signals
    start_noise = random.randrange(0, math.ceil(lengthnoise-lengthsignal))
    noise_piece = noise[start_noise*1000:(start_noise+lengthsignal)*1000]

    # what do fade in and fade out do?
    noise_fade = noise_piece.fade_in(63).fade_out(63)

    # apply SNR from above to normalized .wav, boost it/ make it louder
    signal_boost = signal.apply_gain(ratio)

    # overlay boosted normalized .wav with noise
    mixed_sound = signal_boost.overlay(noise_fade)

    # save the result
    makedirs(result_file)
    handle = mixed_sound.export(result_file, format="wav")
    handle.close()

def convolute_ir(speech_file, ir_file, result_file):
    """ Convolutes speech_file with impulse response from ir_file.
    Saves to result_file
    """

    # get sampling rate and signals from impulse sound file
    f = audiolab.sndfile(ir_file)
    sig_imp = f.read_frames(f.get_nframes(), dtype=np.float64)
    f.close()
    # sig_imp = np.asarray(sig_imp/65536.0, dtype=np.float64)
    # get noised sound file from before
    f = audiolab.sndfile(speech_file)
    sig_mixsound = f.read_frames(f.get_nframes(), dtype=np.int16)
    f.close()
    sig_mixsound = np.asarray(sig_mixsound/65536.0, dtype=np.float64)
    n = len(sig_mixsound)
    (rate_mixsound,u)= wav.read(speech_file)

    # peak-normalization, sig_imp/maximum
    sig_imp_pn = sig_imp / max(sig_imp)

    # convolve noised sound file with impulse reaction
    convolved_sample = signal.fftconvolve(sig_imp_pn, sig_mixsound)
    # convolved_sample = np.convolve(sig_imp_pn, sig_mixsound)
    convolved_sample =convolved_sample / max(convolved_sample)

    convolved_sample = np.asarray(convolved_sample[:n]*65536.0, dtype=np.int16)
    # save convolved sample, change data type to int16
    makedirs(result_file)
    f = audiolab.sndfile(result_file, 'write', audiolab.formatinfo('wav', 'pcm16'), 1, rate_mixsound)
    f.write_frames(convolved_sample[:n])
    f.close()
    # wav.write(result_file, rate_mixsound, convolved_sample[:n])

def accumulate_VAD_ano(ano_file, winlen, winstep):
    """"""
    annotation = []
    with open(ano_file, 'r') as f:
        annotation = f.readlines()
    annotation = map(int, annotation)
    annotation_mean=[]
    annotation_mean_sub=[]
    for el in annotation:
        annotation_mean_sub.append(el)
        if len(annotation_mean_sub)==winlen*16000:
            annotation_mean.append(math.ceil(np.average(annotation_mean_sub)))
            annotation_mean_sub=annotation_mean_sub[int(winstep*16000):]
    annotation_mean.append(math.ceil(np.average(annotation_mean_sub)))
    annotation_mean = map(str, annotation_mean)
    return annotation_mean

def process_speech(speech_file, winlen, winstep):
    f = audiolab.sndfile(speech_file)
    sig = f.read_frames(f.get_nframes(), dtype=np.int16)
    sig = np.asarray(sig/65536.0, dtype=np.float64)
    f.close()
    (rate,unused)=wav.read(speech_file)
    # Cepstral parameters
    mfcc_n_ceps = Config.num_features+1
    mfcc_pre_emphasis_coef = 0.95

    mfcc_features = mfcc(sig, rate, winlen=winlen, winstep=winstep, \
                     numcep=mfcc_n_ceps, preemph=mfcc_pre_emphasis_coef, nfilt=2 * mfcc_n_ceps)
    return mfcc_features

def filtr(files, filetype):
    """Filters a file list by the given filename ending to be accepted"""
    return filter(lambda d: 1 if d.endswith(filetype) else 0, files)

def normalize_vs_noise(speech_file, noise_file, result_file):
    """Normalizes gain of speech_file wrt noise_file. Saves to result_file."""

    # Reference gain calculation for normalization
    pink = audiotools.open(noise_file)
    ref_vrms = list(audiotools.calculate_replay_gain([pink]))
    ref_val = ref_vrms[-1][1]

    # Gain calculation of example vad file
    vad1 = audiotools.open(speech_file)
    file1_vrms = list(audiotools.calculate_replay_gain([vad1]))
    file1_val = file1_vrms[-1][1]

    # Normalization of example vad file
    f = audiolab.sndfile(speech_file)
    sig = f.read_frames(f.get_nframes(), dtype=np.float64)
    f.close()
    sig_normalized = sig*math.pow(10, (-(ref_val-file1_val)/20))

    # write wav file to disk
    makedirs(result_file)
    (rate,u)=wav.read(speech_file)
    f = audiolab.sndfile(result_file, 'write', audiolab.formatinfo('wav', 'pcm16'), 1, rate)
    f.write_frames(sig_normalized)
    f.close()

def float2int(float_wav_path, int_wav_path):
    """Transforms a 64bit float wav file to a 16bit int file"""
    (rate, sig) = wav.read(float_wav_path)
    sig = np.asarray(sig*65536.0, dtype=np.int16)
    wav.write(int_wav_path, rate, sig)


def iter_load_txt_col(filename, col ,delimiter=' '):
    data = []
    with open(filename, 'r') as infile:
        for line in infile:
            line = line.rstrip().split(delimiter)
            if len(line)>=col+1:
                data.append(float(line[col]))
    return data

def iter_load_txt(filename, delimiter=' '):
    data = []
    with open(filename, 'r') as infile:
        for line in infile:
            line = line.rstrip().split(delimiter)
            data.append(line)
    return data

def cmn(data_mfccs):
    a = np.asmatrix(data_mfccs)
    mean_mfcc = a.mean(axis=0)  # mfccs means
    mean_matrix = np.repeat(mean_mfcc[:, np.newaxis], len(data_mfccs), 0)
    data_mfccs -= mean_matrix  # substract the means
    return data_mfccs
