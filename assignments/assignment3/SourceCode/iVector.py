import numpy as np
import bob
import time
import math
import itertools
from Configuration import Config
from sklearn.cluster import KMeans
import sys

# def init_with_kmeans(num_mixtures, num_dimension, n_jobs_kmeans, samples_fold):
#     initKMeans = KMeans(n_clusters=num_mixtures, init='k-means++', n_init=1, max_iter=5, \
#         verbose=0, random_state=1, n_jobs=n_jobs_kmeans)
#     # kmeans_trainer.initialization_method = kmeans_trainer.initialization_method_type.KMEANS_PLUS_PLUS
#     initKMeans.fit(samples_fold)
#     init_mean = initKMeans.cluster_centers_
#
#     #calc covariance matrix for each cluster/mixture
#     init_cov = np.zeros((num_mixtures,num_dimension,num_dimension))
#     for i in range(num_mixtures):
#         cluster_index = i
#         samples_of_cluster = samples_fold[initKMeans.labels_ == cluster_index]
#         if len(samples_of_cluster) > 1:
#             cov_of_cluster = np.cov(samples_of_cluster.T)
#             diag_cov = cov_of_cluster * np.identity(num_dimension)
#         else:
#             diag_cov = np.identity(num_dimension)
#         init_cov[i,:,:] = diag_cov
#
#     init_mean = np.array(init_mean,dtype=np.float32)
#     init_cov =  np.array(init_cov,dtype=np.float32)
#     return init_mean, init_cov, initKMeans.cluster_centers_

def init_with_kmeans(num_mixtures, num_dimension, n_jobs_kmeans, samples_fold):
    kmeans = bob.machine.KMeansMachine(Config.num_mixtures_ubm, Config.num_features)
    kmeansTrainer = bob.trainer.KMeansTrainer()
    kmeansTrainer.max_iterations = Config.n_iter_em
    kmeansTrainer.convergence_threshold = 1e-5
    kmeansTrainer.train(kmeans, samples_fold)
    init_mean = kmeans.means
    init_cov,init_weights= kmeans.get_variances_and_weights_for_each_cluster(samples_fold)
    print init_mean, init_cov, init_weights
    return init_mean, init_cov, init_weights


def windowing(training_samples,training_labels):
    num_utt = int(round(len(training_samples)/Config.frames_per_utterance))
    windows=[]
    window_labels=[]
    for utt_count in range(0, num_utt):
        #get values from array, normalize, then update array
        data = training_samples[utt_count*Config.frames_per_utterance : (utt_count+1)*Config.frames_per_utterance]
        data_ano = training_labels[utt_count*Config.frames_per_utterance : (utt_count+1)*Config.frames_per_utterance]
        tmp_std = np.std(data, 0)
        data -= np.mean(data, 0)
        data /= tmp_std
        windows.append(np.float64(data))
        window_labels.append(data_ano)
    return windows, window_labels

def cmn(ubm_training_samples,ubm_training_labels):
    num_utt = int(round(len(ubm_training_samples)/Config.frames_per_utterance))
    for utt_count in range(0, num_utt):
        #get values from array, normalize, then update array
        data = ubm_training_samples[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        tmp_std = np.std(data, 0)
        data -= np.mean(data, 0)
        data /= tmp_std
        if not  np.any(np.isnan(data)):
            ubm_training_samples[utt_count*Config.frames_per_utterance \
                : (utt_count+1)*Config.frames_per_utterance] = data
    #ignore remaining frames that don't fill the last window
    ubm_training_samples = ubm_training_samples[:num_utt*Config.frames_per_utterance]
    ubm_training_samples = np.float32(ubm_training_samples)
    ubm_training_labels = ubm_training_labels[:num_utt*Config.frames_per_utterance]
    return ubm_training_samples,ubm_training_labels

def train_ubm_bob(training_samples_ubm, training_labels_ubm, results_path):
    # training_samples_ubm, training_labels_ubm = cmn(training_samples_ubm,training_labels_ubm)
    init_mean_ubm, init_cov_ubm, init_weight_ubm = init_with_kmeans(Config.num_mixtures_ubm, Config.num_features, Config.n_jobs_kmeans, training_samples_ubm)
    try: # if gmm specializer works
        from gmm_specializer.gmm import GMM
        # init_weight_ubm = np.array(np.ones(Config.num_mixtures_ubm)* (1/np.float32(Config.num_mixtures_ubm)),dtype=np.float32)
        gmm_fast_ubm = GMM(Config.num_mixtures_ubm, Config.num_features, cvtype=Config.gmm_cvtype, means=init_mean_ubm, covars=init_cov_ubm, weights=init_weight_ubm)
        gmm_fast_ubm.train(training_samples_ubm, max_em_iters=Config.n_iter_em)
        ubm = bob.machine.GMMMachine(Config.num_mixtures_ubm, Config.num_features)
        ubm.means = np.array(gmm_fast_ubm.get_all_component_means(), 'float64')
        ubm.weights = np.array(gmm_fast_ubm.get_all_component_weights(), 'float64')
        ubm.variances = np.array(gmm_fast_ubm.get_all_component_full_covariance(), 'float64').diagonal(axis1=1,axis2=2)
    except: # without gmm specializer
        ubm = bob.machine.GMMMachine(Config.num_mixtures_ubm,Config.num_features) # Create a machine with 2 Gaussian and feature dimensionality 3
        ubm.means = init_mean_ubm
        ubm.variances = init_cov_ubm
        ubm.weights = init_weight_ubm
        trainer = bob.trainer.ML_GMMTrainer(True, True, True) # update means/variances/weights at each iteration
        trainer.convergence_threshold = 1e-5
        trainer.max_iterations = Config.n_iter_em
        trainer.train(ubm, training_samples_ubm)
    ubm_file = 'ubm_' + str(Config.num_mixtures_ubm) + '.hdf5'
    ubm.save(bob.io.HDF5File(results_path + ubm_file, 'w' ))
    print 'UBM saved to disk.', ubm
    return ubm

def train_emotion_gmms(ubm, cleansamples,cleanannos,set):
    emotion_models =[]
    for name, emotion in Config.emotionCodes.__members__.items():
        emo=emotion
        if not isinstance( emo, ( int, long ) ):
            emo=emotion.value
        emotiondata= cleansamples[cleanannos == float(emo)]
        if len(emotiondata)>0:
            relevance_factor = 4.
            trainer = bob.trainer.MAP_GMMTrainer(relevance_factor, True, False, False) # mean adaptation only
            trainer.convergence_threshold = 1e-5
            trainer.max_iterations = Config.n_iter_em
            trainer.set_prior_gmm(ubm)
            gmmAdapted = bob.machine.GMMMachine(Config.num_mixtures_ubm,Config.num_features) # Create a new machine for the MAP estimate
            trainer.train(gmmAdapted, emotiondata)
            gmmAdapted.save(bob.io.HDF5File(Config.model_dir + str(emo)+set+'gmm.hdf5', 'w' ))
            emotion_models.append(gmmAdapted)
    return emotion_models

def train_emotion_models(ubm, cleansamples,cleanannos,set):
    numframes=Config.frames_per_utterance
    emotion_models =[]
    all_models=[]
    for name, emotion in Config.emotionCodes.__members__.items():
        emo=emotion
        if not isinstance( emo, ( int, long ) ):
            emo=emotion.value
        emotiondata= cleansamples[cleanannos == float(emo)]
        emotionannos = cleanannos[cleanannos == float(emo)]
        if len(emotiondata)>0:
            num_utt = int(round(len(emotiondata)/numframes))
            for utt_count in range(0, num_utt):
                data = emotiondata[utt_count*numframes \
                    : (utt_count+1)*numframes]
                data_ano = emotionannos[utt_count*numframes \
                    : (utt_count+1)*numframes]
                #CMVN:
                tmp_std = np.std(data, 0)
                data -= np.mean(data, 0)
                data /= tmp_std
                proj_data = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
                ubm.acc_statistics(data, proj_data)
                emotion_models.append(proj_data)
                proj_data.save(bob.io.HDF5File(Config.model_dir + str(emo)+'_'+str(utt_count)+set+'model.hdf5', 'w' ))
        all_models.append(emotion_models)
    return all_models


def read_emotion_models(path,set):
    emotion_models =[]
    for name, emotion in Config.emotionCodes.__members__.items():
        emo=emotion
        if not isinstance( emo, ( int, long ) ):
            emo=emotion.value
        emotion_models.append([bob.machine.GMMStats(bob.io.HDF5File(path+str(emo)+set+'model.hdf5', 'r'))])
    return emotion_models

def read_emotion_gmms(path,set):
    emotion_models =[]
    for name, emotion in Config.emotionCodes.__members__.items():
        emo=emotion
        if not isinstance( emo, ( int, long ) ):
            emo=emotion.value
        emotion_models.append(bob.machine.GMMMachine(bob.io.HDF5File(path+str(emo)+set+'gmm.hdf5', 'r')))
    return emotion_models


def scoreGMM(ubm, speakergmms,testsamples,testlabels):
    max = -sys.maxint - 1
    for model in speakergmms:
        mean_log_lklds_fast_ubm=0
        mean_log_lklds_fast_speaker=0
        for i in range(len(testsamples)):
            mean_log_lklds_fast_ubm = mean_log_lklds_fast_ubm+ ubm.log_likelihood(testsamples[i])/len(testsamples)
            mean_log_lklds_fast_speaker = mean_log_lklds_fast_speaker+ model.log_likelihood(testsamples[i])/len(testsamples)
        score = mean_log_lklds_fast_speaker - mean_log_lklds_fast_ubm
        print speakergmms.index(model)+1, ":", score
        if score>max:
            max= score
            speaker = model
    if speakergmms.index(speaker)+1 == round(np.mean(testlabels)):
        return True
    else: return False

def train_test_gmm(ubm, testsamples):
    proj_train_features_test = bob.machine.GMMStats(Config.num_mixtures_ubm, Config.num_features)
    ubm.acc_statistics(np.float64(testsamples), proj_train_features_test)
    return proj_train_features_test

def train_test_ivec (ubm, testsamples):
    all_gs=[]
    numframes=Config.frames_per_utterance
    num_utt = int(round(len(testsamples)/numframes))
    for utt_count in range(0, num_utt):
        data = testsamples[utt_count*numframes \
            : (utt_count+1)*numframes]
        data_ano = testsamples[utt_count*numframes \
            : (utt_count+1)*numframes]
        #CMVN:
        tmp_std = np.std(data, 0)
        data -= np.mean(data, 0)
        data /= tmp_std
        proj_data = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
        ubm.acc_statistics(data, proj_data)
        all_gs.append(proj_data)
    ivec = transform_gmm_to_whitened_iVector(ubm,[all_gs])
    lda = transform_iVector_to_lda(np.asmatrix(ivec[0]))
    wccn = transform_lda_to_wccn(lda)
    return wccn


def transform_gmm_to_whitened_iVector(ubm, models):
    # Create space projection
    models_flat = [item for sublist in models for item in sublist]
    ivec_machine = bob.machine.IVectorMachine(ubm, Config.ivector_dim)
    ivec_machine.variance_threshold = 1e-5
    ivec_trainer = bob.trainer.IVectorTrainer(update_sigma=True, max_iterations=Config.n_iter_em)
    ivec_trainer.initialize(ivec_machine, models_flat)
    ivec_trainer.train(ivec_machine, models_flat)

    # Project models to space
    ivectors_matrix = []
    for model in models_flat:
        projected_ivector = ivec_machine.forward(model)
        ivectors_matrix.append(projected_ivector)
    ivectors_matrix = np.vstack(ivectors_matrix)

    print np.shape(ivectors_matrix)

    # Whitening
    linear_whitening_machine = bob.machine.LinearMachine(ivectors_matrix.shape[1], ivectors_matrix.shape[1])
    t = bob.trainer.WhiteningTrainer()
    t.train(linear_whitening_machine, ivectors_matrix)
    whitened_ivectors = []
    for proj_train_features_speaker in models:
        speaker_ivectors = []
        for gmmstats in proj_train_features_speaker:
            projected_ivector = ivec_machine.forward(gmmstats)
            whitened_ivector = linear_whitening_machine.forward(projected_ivector)
            normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
            speaker_ivectors.append(normalized_ivector)
        whitened_ivectors.append(np.vstack(speaker_ivectors))
    return whitened_ivectors

def transform_iVector_to_lda(ivectors):
    t = bob.trainer.FisherLDATrainer(strip_to_rank=False)
    print ivectors
    # print (np.asmatrix(ivectors)) #, np.linalg.cholesky(np.asmatrix(ivectors))
    lda_machine, __eig_vals = t.train(ivectors)
    # resize the machine if desired
    lda_machine.resize(lda_machine.shape[0], Config.lda_subspace_dim)
    lda_projected_ivectors = []
    for whitened_ivectors_speaker in ivectors:
        speaker_lda_ivectors = []
        for ivector in whitened_ivectors_speaker:
            lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
            lda_machine(ivector, lda_ivector)
            speaker_lda_ivectors.append(lda_ivector)
        lda_projected_ivectors.append(np.vstack(speaker_lda_ivectors))
    return lda_projected_ivectors


def transform_lda_to_wccn(ldas):
    t = bob.trainer.WCCNTrainer()
    wccn_machine = t.train(ldas)
    wccn_lda_projected_ivectors = []
    for lda_ivectors_speaker in ldas:
        speaker_wccn_lda_ivectors = []
        for lda_ivector in lda_ivectors_speaker:
            wccn_lda_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
            wccn_machine(lda_ivector, wccn_lda_ivector)
            speaker_wccn_lda_ivectors.append(wccn_lda_ivector)
        wccn_lda_projected_ivectors.append(np.vstack(speaker_wccn_lda_ivectors))
    return wccn_lda_projected_ivectors


def get_emotion_with_cosine_score(emotion_models, test_model):
    emotion= Config.emotionCodes.Neutral
    max = -sys.maxint - 1
    for model in emotion_models:
        cosi = abs(cosine_distance(model, test_model))
        if score>max:
            max=score
            emotion = emotion_models.index(model)
    return emotion


def cosine_distance(a, b):
    if len(a) != len(b):
        raise ValueError, "a and b must be same length"
    numerator = sum(tup[0] * tup[1] for tup in itertools.izip(a,b))
    denoma = sum(avalue ** 2 for avalue in a)
    denomb = sum(bvalue ** 2 for bvalue in b)
    result = numerator / (math.sqrt(denoma)*math.sqrt(denomb))
    return result

def testGMM(ubm, emogmms,cleansamples,cleanannos):
    windows, labels =windowing(cleansamples,cleanannos)
    res=[]
    for i in range(len(labels)):
        sc= scoreGMM( ubm, emogmms,windows[i],labels[i])
        print sc
        if sc:
            res.append(sc)
    return float(len(res))/len(windows)


#Score a given test vector against the personal model and normalize according to norm_type
def score_vector(test_ivector, speaker_ivector, norm_type, z_impostor_mean, z_impostor_stdv, \
        impostor_ivectors, mean_impostor_ivector, imp_sqrt_diag_covar_mat, debug_level):

    if norm_type == 'znorm':
        score = cosine_distance(speaker_ivector, test_ivector)
        score_norm = (score-z_impostor_mean)/z_impostor_stdv
    elif norm_type == 'normcos':
        #see: "Cosine Similarity Scoring without Score Normalization Techniques",
        #      Dehak et al., 2010

        norm_speaker = np.linalg.norm(np.dot(imp_sqrt_diag_covar_mat, np.atleast_2d(speaker_ivector).T))
        norm_test = np.linalg.norm(np.dot(imp_sqrt_diag_covar_mat, np.atleast_2d(test_ivector).T))

        dist1 = np.subtract(speaker_ivector, mean_impostor_ivector)
        dist2 = np.subtract(test_ivector, mean_impostor_ivector)

        score_norm = np.dot(dist1, dist2) / np.dot(norm_speaker, norm_test)
        #print 'score_norm is %s' % (score_norm)
    elif norm_type == 'snorm':
        #see: "Unsupervised Speaker Adaptation based on the Cosine Similarity for
        #      Text-Independent Speaker Verification", Shum et al., 2010

        score = cosine_distance(speaker_ivector, test_ivector)

        (t_impostor_mean, t_impostor_stdv) = \
            get_impostor_distribution(impostor_ivectors, test_ivector, debug_level)

        score_norm = \
            (score-z_impostor_mean)/z_impostor_stdv + \
            (score-t_impostor_mean)/t_impostor_stdv

    return score_norm

# def createUBM(ubm_training_samples):
#     num_utt = int(round(len(ubm_training_samples)/Config.frames_per_utterance))
#         for utt_count in range(0, num_utt):
#             #get values from array, normalize, then update array
#             data = ubm_training_samples[utt_count*Config.frames_per_utterance \
#                 : (utt_count+1)*Config.frames_per_utterance]
#             tmp_std = np.std(data[data_ano==1], 0)
#             data -= np.mean(data[data_ano==1], 0)
#             data /= tmp_std
#             ubm_training_samples[utt_count*Config.frames_per_utterance \
#                 : (utt_count+1)*Config.frames_per_utterance] = data
#         #ignore remaining frames that don't fill the last window
#         ubm_training_samples = ubm_training_samples[:num_utt*Config.frames_per_utterance]
#         ubm_training_labels = ubm_training_labels[:num_utt*Config.frames_per_utterance]
#
#         training_samples_ubm = np.float32(ubm_training_samples[ubm_training_labels==1])
#         from helpers import train_ubm_bob
#         ubm = train_ubm_bob(num_mixtures_ubm, num_dimension, gmm_cvtype, n_jobs_kmeans, \
#             training_samples_ubm, num_iter_em, debug_level, ir_name, results_path)
#
#         # print 'Starting UBM training...'
#         # num_mixtures_ubm = 512
#         # ubm = bob.machine.GMMMachine(num_mixtures_ubm, num_dimension)
#         # mu = iter_loadtxt('/home/schulze/ubm_512_4m_mu.csv')
#         # sigma = iter_loadtxt('/home/schulze/ubm_512_4m_Sigma.csv')
#         # pi = iter_loadtxt('/home/schulze/ubm_512_4m_pi.csv')
#         # ubm.means = np.array(mu, dtype=np.float64)
#         # ubm.variances = np.array(sigma, dtype=np.float64)
#         # ubm.weights = np.array(pi, dtype=np.float64).flatten()
#         # print 'UBM training done.'
#


# def gmm_tv(settings):
#
#     (base_path_features, results_path, ubm_load_path, ivec_machine_load_path, \
#         white_machine_load_path, lda_machine_load_path, wccn_machine_load_path, \
#         split_impostors_into_utterances, norm_type, frame_inc, sampling_rate, k_fold, \
#         n_jobs_kmeans, num_mixtures_ubm, num_features, gmm_cvtype, num_iter_em, frames_per_utterance, \
#         ivector_dim, lda_subspace_dim, samples_feature_type, \
#         parameter_file_config_samples, vad_label_extension, anno_feature_type, parameter_file_config_anno, \
#         ubm_path, ubm_by_speaker_path, impostors_path, basic_speaker_path, basic_speaker_test_path, \
#         ir_name, skip_new_datasets, skip_old_datasets, debug_level, mean_vec, std_vec) = settings
#
#     ########################################
#     #read data
#
#     import sys
#     sys.path.append('/home/schulze/speaker_recog/classification/')
#
#     from read_data_uttnorm_real import read_ubm_and_speakers
#     settings_data = (base_path_features, ubm_path, basic_speaker_path, basic_speaker_test_path, \
#         ubm_load_path, frame_inc, sampling_rate, k_fold, num_features, samples_feature_type, \
#         parameter_file_config_samples, vad_label_extension, anno_feature_type, parameter_file_config_anno, \
#         skip_new_datasets, skip_old_datasets, mean_vec, std_vec, frames_per_utterance)
#     (ubm_training_samples, ubm_training_labels, all_speaker_samples, all_speaker_vad_anno, all_speaker_test_samples, \
#             all_speaker_training_partition, all_speaker_test_partition) = read_ubm_and_speakers(settings_data)
#
#     from read_data_uttnorm_real import read_ubm_by_speakers
#     settings_data = (base_path_features, ubm_by_speaker_path, num_features, samples_feature_type, \
#         parameter_file_config_samples, vad_label_extension, anno_feature_type, parameter_file_config_anno, \
#         mean_vec, std_vec, frames_per_utterance)
#     all_ubm_speaker_samples, all_ubm_speaker_vad_anno = read_ubm_by_speakers(settings_data)
#
#     if impostors_path != []:
#         from read_data_uttnorm_real import read_impostors
#         settings_data = (base_path_features, impostors_path, num_features, samples_feature_type, \
#             parameter_file_config_samples, vad_label_extension, anno_feature_type, parameter_file_config_anno, \
#             mean_vec, std_vec, frames_per_utterance)
#         (all_impostor_samples, all_impostor_vad_anno) = read_impostors(settings_data)
#
#
#     ########################################
#
#
#     no_of_speakers = len(all_speaker_samples)
#     num_dimension = num_features
#
#     #train the UBM
#     if ubm_load_path:
#         ubm = bob.machine.GMMMachine(bob.io.HDF5File(ubm_load_path, 'r'))
#         print 'UBM loaded.'
#     else:
#         num_utt = int(round(len(ubm_training_samples)/frames_per_utterance))
#         for utt_count in range(0, num_utt):
#             #get values from array, normalize, then update array
#             data = ubm_training_samples[utt_count*frames_per_utterance \
#                 : (utt_count+1)*frames_per_utterance]
#             data_ano = ubm_training_labels[utt_count*frames_per_utterance \
#                     : (utt_count+1)*frames_per_utterance]
#             tmp_std = np.std(data[data_ano==1], 0)
#             data -= np.mean(data[data_ano==1], 0)
#             data /= tmp_std
#             ubm_training_samples[utt_count*frames_per_utterance \
#                 : (utt_count+1)*frames_per_utterance] = data
#         #ignore remaining frames that don't fill the last window
#         ubm_training_samples = ubm_training_samples[:num_utt*frames_per_utterance]
#         ubm_training_labels = ubm_training_labels[:num_utt*frames_per_utterance]
#
#         training_samples_ubm = np.float32(ubm_training_samples[ubm_training_labels==1])
#         from helpers import train_ubm_bob
#         ubm = train_ubm_bob(num_mixtures_ubm, num_dimension, gmm_cvtype, n_jobs_kmeans, \
#             training_samples_ubm, num_iter_em, debug_level, ir_name, results_path)
#
#         # print 'Starting UBM training...'
#         # num_mixtures_ubm = 512
#         # ubm = bob.machine.GMMMachine(num_mixtures_ubm, num_dimension)
#         # mu = iter_loadtxt('/home/schulze/ubm_512_4m_mu.csv')
#         # sigma = iter_loadtxt('/home/schulze/ubm_512_4m_Sigma.csv')
#         # pi = iter_loadtxt('/home/schulze/ubm_512_4m_pi.csv')
#         # ubm.means = np.array(mu, dtype=np.float64)
#         # ubm.variances = np.array(sigma, dtype=np.float64)
#         # ubm.weights = np.array(pi, dtype=np.float64).flatten()
#         # print 'UBM training done.'
#
#
#     from helpers_real import project_features_ubm
#     proj_train_features = project_features_ubm(all_ubm_speaker_samples, all_ubm_speaker_vad_anno, \
#         split_impostors_into_utterances, frames_per_utterance, ubm)
#
#     proj_train_features_flat = [item for sublist in proj_train_features for item in sublist]
#
#     ivec_machine = init_ivector_machine(ivec_machine_load_path, ubm, ivector_dim, proj_train_features_flat, \
#         ir_name, results_path, debug_level)
#
#     whitening_machine = init_whitening_machine(white_machine_load_path, ivec_machine, \
#         proj_train_features_flat, ir_name, results_path, debug_level)
#
#
#     print 'Starting I-Vector whitening and normalization...'
#     whitened_ivectors = []
#     for proj_train_features_speaker in proj_train_features:
#         speaker_ivectors = []
#         for gmmstats in proj_train_features_speaker:
#             projected_ivector = ivec_machine.forward(gmmstats)
#             whitened_ivector = whitening_machine.forward(projected_ivector)
#             normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
#             speaker_ivectors.append(normalized_ivector)
#         whitened_ivectors.append(np.vstack(speaker_ivectors))
#     print 'I-Vector whitening and normalization done.'
#
#     proj_train_features = whitened_ivectors
#
#     lda_machine = init_lda_machine(lda_machine_load_path, proj_train_features, lda_subspace_dim, \
#         ir_name, results_path, debug_level)
#
#     print 'Starting LDA projection...'
#     lda_projected_ivectors = []
#     for whitened_ivectors_speaker in proj_train_features:
#         speaker_lda_ivectors = []
#         for ivector in whitened_ivectors_speaker:
#             lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
#             lda_machine(ivector, lda_ivector)
#             speaker_lda_ivectors.append(lda_ivector)
#         lda_projected_ivectors.append(np.vstack(speaker_lda_ivectors))
#     print 'LDA projection done.'
#
#     proj_train_features = lda_projected_ivectors
#
#     wccn_machine = init_wccn_machine(wccn_machine_load_path, proj_train_features, ir_name, \
#         results_path, debug_level)
#
#     if impostors_path != []:
#         impostor_ivectors = get_impostor_ivectors(ubm, ivec_machine, whitening_machine, lda_machine, \
#             wccn_machine, all_impostor_samples, all_impostor_vad_anno, frames_per_utterance)
#     else:
#         impostor_ivectors = get_impostor_ivectors(ubm, ivec_machine, whitening_machine, lda_machine, \
#             wccn_machine, all_impostor_samples, all_impostor_vad_anno, frames_per_utterance)
#
#     imp_sqrt_diag_covar_mat = np.sqrt(np.diag(np.cov(np.matrix(impostor_ivectors).T).diagonal()))
#     #should be ivecdim x ivecdim
#     print 'Shape of impostor_sqrt_diag_covar_mat is (%d, %d)' % (imp_sqrt_diag_covar_mat.shape)
#     mean_impostor_ivector = np.mean(impostor_ivectors, 0)
#
#     positives_scores = []
#     negatives_scores = []
#
#     from helpers_real import prepare_eval_data_cv
#
#     if debug_level > 0:
#         mem_free, mem_total = cuda.mem_get_info()
#         print 'Allocated GPU memory before evaluation: %sMB' % ((mem_total-mem_free)/1048576)
#
#     #evaluate over all speakers
#     for speaker_index in range(no_of_speakers):
#
#         print 'Starting CV for speaker %d of %d...' % (speaker_index+1, no_of_speakers)
#         ts_0 = time.time()
#
#         positives_scores_speaker = []
#         negatives_scores_speaker = []
#
#         #evalute over all folds in CV
#         for cross_index in range(k_fold):
#
#             #build training data speaker
#             speaker_training_partition = all_speaker_training_partition[speaker_index]
#             current_fold_training_partition_index = speaker_training_partition[cross_index]
#             samples_speaker = all_speaker_samples[speaker_index]
#             ano_speaker = all_speaker_vad_ano[speaker_index]
#             training_samples_speaker = \
#                 np.float64(samples_speaker[current_fold_training_partition_index])
#             training_ano_speaker = ano_speaker[current_fold_training_partition_index]
#
#             #which set of voiced samples should we evaluate?
#             if basic_speaker_test_path:
#                 all_speaker_samples_eval = all_speaker_test_samples
#             else:
#                 all_speaker_samples_eval = all_speaker_samples
#
#             (test_samples, test_vad_ano, test_labels) = prepare_eval_data_cv(speaker_index, cross_index, \
#                 all_speaker_samples_eval, all_speaker_vad_ano, all_speaker_test_partition, no_of_speakers)
#
#             #######################
#             #Train personal model (enrollment)
#             #######################
#             speaker_ivecs = []
#             num_utt = int(round(len(training_samples_speaker)/frames_per_utterance))
#             for utt_count in range(0, num_utt):
#                 data = training_samples_speaker[utt_count*frames_per_utterance \
#                     : (utt_count+1)*frames_per_utterance]
#                 data_ano = training_ano_speaker[utt_count*frames_per_utterance \
#                     : (utt_count+1)*frames_per_utterance]
#                 if float(sum(data_ano))/len(data_ano) >= 0.5:
#                     tmp_std = np.std(data[data_ano==1], 0)
#                     data -= np.mean(data[data_ano==1], 0)
#                     data /= tmp_std
#                     wccn_lda_ivector = compute_compensated_ivector(data[data_ano==1], ubm, ivec_machine, \
#                         whitening_machine, lda_machine, wccn_machine)
#                     speaker_ivecs.append(wccn_lda_ivector)
#             speaker_wccn_lda_ivector = np.mean(speaker_ivecs)
#
#             if len(speaker_wccn_lda_ivector) != len(speaker_ivecs[0]):
#                 print 'ALARM!!!!!' #just to be sure
#
#             #######################
#             # Determine impostor scores
#             #######################
#
#             if norm_type != 'normcos':
#                 (impostor_mean, impostor_stdv) = get_impostor_distribution(impostor_ivectors, \
#                     speaker_wccn_lda_ivector, debug_level)
#             else:
#                 (impostor_mean, impostor_stdv) = (0,0)
#
#             #######################
#             # Evaluation
#             #######################
#
#             test_samples = np.float64(test_samples)
#             test_labels = np.int8(test_labels)
#
#             #get predictions and store scores for positive/negative samples in-place
#             make_predictions_tv(ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine, \
#                 test_samples, test_vad_ano, test_labels, frames_per_utterance, norm_type, imp_sqrt_diag_covar_mat, \
#                 impostor_ivectors, mean_impostor_ivector, impostor_mean, impostor_stdv, \
#                 positives_scores_speaker, negatives_scores_speaker, speaker_wccn_lda_ivector, debug_level)
#
#         ts_1 = time.time()
#         print 'Speaker %d done. Time passed: %fs' % (speaker_index+1, ts_1-ts_0)
#         print 'EER is %s' % (bob.measure.eer_rocch(negatives_scores_speaker, \
#             positives_scores_speaker))
#         print 'EER threshold is %s' % (bob.measure.eer_threshold(negatives_scores_speaker, \
#             positives_scores_speaker))
#         negatives_scores.append(negatives_scores_speaker)
#         positives_scores.append(positives_scores_speaker)
#
#     print 'Entire run completed.'
#     print '*********************'
#     return (positives_scores, negatives_scores)
#
#
# # Takes a feature vector of variable length and splits it into utterances which are then scored
# # against a personal model. The scores are compared with thresholds to make a 0/1 prediction.
# # Predictions and ground truth (averaged over frames in utterance) are returned.
# def make_predictions_tv(ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine, test_samples, \
#     test_vad_ano, test_labels, frames_per_utterance, norm_type, imp_sqrt_diag_covar_mat, impostor_ivectors, \
#     mean_impostor_ivector, impostor_mean, impostor_stdv, positives_scores, negatives_scores, \
#     speaker_wccn_lda_ivector, debug_level):
#
#     # split test data into short utterances and score those
#     num_utt = int(round(len(test_samples)/frames_per_utterance))
#     for utt_count in range(0, num_utt):
#         t0 = time.time();
#         data = test_samples[utt_count*frames_per_utterance \
#             : (utt_count+1)*frames_per_utterance]
#         data_ano = test_vad_ano[utt_count*frames_per_utterance \
#             : (utt_count+1)*frames_per_utterance]
#         if float(sum(data_ano))/len(data_ano) < 0.5:
#             continue
#         tmp_std = np.std(data[data_ano==1], 0)
#         data -= np.mean(data[data_ano==1], 0)
#         data /= tmp_std
#         wccn_lda_ivector = compute_compensated_ivector(data[data_ano==1], ubm, ivec_machine, \
#             whitening_machine, lda_machine, wccn_machine)
#
#         #Compute score of projected vector against personal model
#         score_norm = score_vector(wccn_lda_ivector, speaker_wccn_lda_ivector, norm_type, \
#             impostor_mean, impostor_stdv, impostor_ivectors, mean_impostor_ivector, \
#             imp_sqrt_diag_covar_mat, debug_level)
#
#         t1 = time.time()
#         if debug_level > 1 and utt_count == 0:
#             print 'Scoring first test utterance took %fs' % (t1-t0)
#
#         labels = test_labels[utt_count*frames_per_utterance \
#             : (utt_count+1)*frames_per_utterance]
#         #average over all frames in this utterance
#         label_should = round(np.mean(labels[data_ano==1]))
#
#         #for further performance evaluation
#         if label_should==1:
#             positives_scores.append(score_norm)
#         else:
#             negatives_scores.append(score_norm)
#
#
# #Score a given test vector against the personal model and normalize according to norm_type
# def score_vector(test_ivector, speaker_ivector, norm_type, z_impostor_mean, z_impostor_stdv, \
#         impostor_ivectors, mean_impostor_ivector, imp_sqrt_diag_covar_mat, debug_level):
#
#     if norm_type == 'znorm':
#         score = cosine_distance(speaker_ivector, test_ivector)
#         score_norm = (score-z_impostor_mean)/z_impostor_stdv
#     elif norm_type == 'normcos':
#         #see: "Cosine Similarity Scoring without Score Normalization Techniques",
#         #      Dehak et al., 2010
#
#         norm_speaker = np.linalg.norm(np.dot(imp_sqrt_diag_covar_mat, np.atleast_2d(speaker_ivector).T))
#         norm_test = np.linalg.norm(np.dot(imp_sqrt_diag_covar_mat, np.atleast_2d(test_ivector).T))
#
#         dist1 = np.subtract(speaker_ivector, mean_impostor_ivector)
#         dist2 = np.subtract(test_ivector, mean_impostor_ivector)
#
#         score_norm = np.dot(dist1, dist2) / np.dot(norm_speaker, norm_test)
#         #print 'score_norm is %s' % (score_norm)
#     elif norm_type == 'snorm':
#         #see: "Unsupervised Speaker Adaptation based on the Cosine Similarity for
#         #      Text-Independent Speaker Verification", Shum et al., 2010
#
#         score = cosine_distance(speaker_ivector, test_ivector)
#
#         (t_impostor_mean, t_impostor_stdv) = \
#             get_impostor_distribution(impostor_ivectors, test_ivector, debug_level)
#
#         score_norm = \
#             (score-z_impostor_mean)/z_impostor_stdv + \
#             (score-t_impostor_mean)/t_impostor_stdv
#
#     return score_norm

# Takes a feature vector of variable length and transforms it into a channel-compensated ivector.
def compute_compensated_ivector(samples, ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine):
    #Project test vector (~MAP-adapt UBM)
    gmmstats = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
    ubm.acc_statistics(samples, gmmstats)
    projected_ivector = ivec_machine.forward(gmmstats)
    whitened_ivector = whitening_machine.forward(projected_ivector)
    normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
    lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
    lda_machine(normalized_ivector, lda_ivector)
    wccn_lda_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
    wccn_machine(lda_ivector, wccn_lda_ivector)

    return wccn_lda_ivector

# Get a distribution of how impostors score against a given model. This serves for z-normalization.
def get_impostor_distribution(impostor_ivectors, speaker_wccn_lda_ivector, debug_level):
    scores_impostors = []

    t0 = time.time()
    for wccn_lda_ivector in impostor_ivectors:
        score = cosine_distance(wccn_lda_ivector, speaker_wccn_lda_ivector)
        scores_impostors.append(score)

    impostor_mean = np.mean(scores_impostors)
    impostor_stdv = np.std(scores_impostors)

    t1 = time.time()
    if debug_level > 1:
        print 'Computing the impostor distribution took %fs' % (t1-t0)

    return (impostor_mean, impostor_stdv)

def get_impostor_ivectors(ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine, \
    impostor_samples, impostor_vad_anno, frames_per_utterance):

    ivecs = []

    for (speaker_samples, speaker_ano) in zip(impostor_samples, impostor_vad_anno):
        speaker_ivecs = []
        num_utt = int(round(len(speaker_samples)/frames_per_utterance))
        for utt_count in range(0, num_utt):
            data = speaker_samples[utt_count*frames_per_utterance \
                : (utt_count+1)*frames_per_utterance]
            data_ano = speaker_ano[utt_count*frames_per_utterance \
                : (utt_count+1)*frames_per_utterance]
            if float(sum(data_ano))/len(data_ano) >= 0.5:
                tmp_std = np.std(data[data_ano==1], 0)
                data -= np.mean(data[data_ano==1], 0)
                data /= tmp_std
                wccn_lda_ivector = compute_compensated_ivector(data[data_ano==1], ubm, ivec_machine, \
                    whitening_machine, lda_machine, wccn_machine)
                speaker_ivecs.append(wccn_lda_ivector)
        avg_wccn_lda_ivector = np.mean(speaker_ivecs)
        if avg_wccn_lda_ivector.shape[0] != speaker_ivecs[0].shape[0]:
            print 'ALARM!!!!!' #just to be sure
            print 'shape1: %s, shape2: %s' % (avg_wccn_lda_ivector.shape, speaker_ivecs[0].shape)
        ivecs.append(avg_wccn_lda_ivector)
    return ivecs


def cosine_distance(a, b):
    # if len(a) != len(b):
    #     raise ValueError, "a and b must be same length"
    numerator = sum(tup[0] * tup[1] for tup in itertools.izip(a,b))
    denoma = sum(avalue ** 2 for avalue in a)
    denomb = sum(bvalue ** 2 for bvalue in b)
    result = numerator / (math.sqrt(denoma)*math.sqrt(denomb))
    return result


def init_ivector_machine(ivec_machine_load_path, ubm, ivector_dim, proj_train_features_flat, ir_name, \
    results_path, debug_level):
    if ivec_machine_load_path:
        ivec_machine = bob.machine.IVectorMachine(bob.io.HDF5File(ivec_machine_load_path, 'r'))
        ivec_machine.ubm = ubm
        print 'I-Vector machine loaded.'
    else:
        print 'Starting I-Vector machine training...'
        t0 = time.time()
        ivec_machine = bob.machine.IVectorMachine(ubm, ivector_dim)
        ivec_machine.variance_threshold = 1e-5
        ivec_trainer = bob.trainer.IVectorTrainer(update_sigma=True, max_iterations=10)
        ivec_trainer.initialize(ivec_machine, proj_train_features_flat)
        ivec_trainer.train(ivec_machine, proj_train_features_flat)
        print 'I-Vector machine training done.'
        t1 = time.time()
        if debug_level > 0:
            print 'Training took %fs' % (t1-t0)

        ivec_file = 'ivec_machine_' + str(ivector_dim) + '_' + ir_name + '.hdf5'
        ivec_machine.save(bob.io.HDF5File(results_path + ivec_file, 'w' ))
        print 'I-Vector machine saved to disk.'
    return ivec_machine


def init_whitening_machine(white_machine_load_path, ivec_machine, proj_train_features_flat, ir_name, \
    results_path, debug_level):
    if white_machine_load_path:
        whitening_machine = bob.machine.LinearMachine(bob.io.HDF5File(white_machine_load_path, 'r'))
        print 'Whitening machine loaded.'
    else:
        print 'Starting whitening enroller training...'
        t0 = time.time()
        ivectors_matrix  = []

        for gmmstats in proj_train_features_flat:
            projected_ivector = ivec_machine.forward(gmmstats)
            ivectors_matrix.append(projected_ivector)

        ivectors_matrix = np.vstack(ivectors_matrix)
        whitening_machine = bob.machine.LinearMachine(ivectors_matrix.shape[1],ivectors_matrix.shape[1])
        t = bob.trainer.WhiteningTrainer()
        t.train(whitening_machine, ivectors_matrix)
        print 'Whitening enroller training done.'
        t1 = time.time()
        if debug_level > 0:
            print 'Training took %fs' % (t1-t0)

        white_file = 'white_machine_' + ir_name + '.hdf5'
        whitening_machine.save(bob.io.HDF5File(results_path + white_file, 'w' ))
        print 'Whitening machine saved to disk.'
    return whitening_machine


def init_lda_machine(lda_machine_load_path, proj_train_features, lda_subspace_dim, ir_name, results_path, \
    debug_level):
    if lda_machine_load_path:
        lda_machine = bob.machine.LinearMachine(bob.io.HDF5File(lda_machine_load_path, 'r'))
        print 'LDA machine loaded.'
    else:
        print 'Starting LDA projector training...'
        t0 = time.time()
        t = bob.trainer.FisherLDATrainer(strip_to_rank=False)
        lda_machine, __eig_vals = t.train(proj_train_features)
        # resize the machine if desired
        lda_machine.resize(lda_machine.shape[0], lda_subspace_dim)
        print 'LDA projector training done.'
        t1 = time.time()
        if debug_level > 0:
            print 'Training took %fs' % (t1-t0)

        lda_file = 'lda_machine_' + str(lda_subspace_dim) + '_' + ir_name + '.hdf5'
        lda_machine.save(bob.io.HDF5File(results_path + lda_file, 'w' ))
        print 'LDA machine saved to disk.'
    return lda_machine


def init_wccn_machine(wccn_machine_load_path, proj_train_features, ir_name, results_path, debug_level):
    if wccn_machine_load_path:
        wccn_machine = bob.machine.LinearMachine(bob.io.HDF5File(wccn_machine_load_path, 'r'))
        print 'WCCN machine loaded.'
    else:
        print 'Starting WCCN projector training...'
        t0 = time.time()
        t = bob.trainer.WCCNTrainer()
        wccn_machine = t.train(proj_train_features)
        print 'WCCN projector training done.'
        t1 = time.time()
        if debug_level > 0:
            print 'Training took %fs' % (t1-t0)

        wccn_file = 'wccn_machine_' + ir_name + '.hdf5'
        wccn_machine.save(bob.io.HDF5File(results_path + wccn_file, 'w' ))
        print 'WCCN machine saved to disk.'
    return wccn_machine
