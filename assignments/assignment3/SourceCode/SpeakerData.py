import logging
import AudioProcessing
import random
import os
import numpy as np
from enum import Enum
import bob
from Configuration import Config
from concurrent.futures import ThreadPoolExecutor
import multiprocessing
import math


# Use in Main like this:
# Configuration.setConfiguration(type,'Speaker') # local or ssh, Berlin, Fau or Semaine or Speakers
# # SpeakerData.processAudioDataset()
# # SpeakerData.createCleanLearningDataset()
# # SpeakerData.createChanneledLearningDataset()
# clean_speakers,clean_files,clean_samples, clean_data_vad=SpeakerData.readClean()
# channeledannos,channelednames,channeledsamples, data_vad=SpeakerData.readChanneled()


def processAudioDataset():
    # Configure logging
    logging_path = Config.working_dir + 'debug.log'
    logging.basicConfig(filename=logging_path, level=logging.DEBUG, format='%(asctime)s %(message)s')


    logging.info('Starting normalizing speech files...')
    for subdir, dirs, files in os.walk(Config.speech_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        for w in wavs:
            speech_file = subdir+'/'+w
            result_file = speech_file.replace(Config.speech_dir, Config.normalize_dir)
            logging.debug('speech_file='+speech_file)
            logging.debug('result_file='+speech_file)
            AudioProcessing.normalize_vs_noise(speech_file, Config.ref_noise, result_file)
    logging.info('Finished normalizing speech files!')

    logging.info('')
    logging.info('')
    logging.info('Starting transformation of 64bit float noise files to 16bit int...')
    # Transform 64bit float noise files to 16bit int
    for subdir, dirs, files in os.walk(Config.noise_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        for w in wavs:
            float_wav_file = subdir+w
            int_wav_file = float_wav_file.replace(Config.noise_dir, Config.noise_int_dir)
            logging.debug('float_wav_file ='+float_wav_file)
            logging.debug('int_wav_file='+int_wav_file)
            AudioProcessing.float2int(float_wav_file, int_wav_file)
    logging.info('Finished transformation of data type!')

    logging.info('')
    logging.info('')
    logging.info('Starting mixing speech files with random noise...')
    # Mix speech files with random noise
    for subdir, dirs, files in os.walk(Config.normalize_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        for w in wavs:
            speech_file = subdir+'/'+w
            result_file = speech_file.replace(Config.normalize_dir, Config.noised_dir)
            # pick random noise file, must be 16bit int, 64bit int doesn't work!
            noise_candidates = AudioProcessing.filtr(os.listdir(Config.noise_int_dir),'.wav')
            noise_file =Config.noise_int_dir+random.choice(noise_candidates)
            logging.debug('speech_file='+speech_file)
            logging.debug('noise_file ='+noise_file)
            logging.debug('result_file='+result_file)
            AudioProcessing.add_noise(speech_file, noise_file, result_file)
    logging.info('Finished mixing speech files!')

    logging.info('')
    logging.info('')
    logging.info('Starting convoluting noised speech with random IR...')
    # Convolute noised speech files with random impulse reaction
    for subdir, dirs, files in os.walk(Config.noised_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        # parallelize
        with ThreadPoolExecutor(multiprocessing.cpu_count()) as executor:
            for w in wavs:
                executor.submit(execConvolution, subdir,w)
    logging.info('Finished convoluting speech!')

def execConvolution(subdir, w):
    speech_file = subdir+'/'+w
    result_file = speech_file.replace(Config.noised_dir, Config.convolute_dir)
    # pick random impulse reaction file
    ir_file = Config.ir_dir+random.choice(AudioProcessing.filtr(os.listdir(Config.ir_dir),'.wav'))
    logging.debug('speech_file='+speech_file)
    logging.debug('ir_file    ='+ir_file)
    logging.debug('result_file='+result_file)
    AudioProcessing.convolute_ir(speech_file, ir_file, result_file)


def createCleanLearningDataset():
    logging.info('')
    logging.info('')
    logging.info('Starting extraction of speech MFCCS & adaption of annotations...')

    type='clean'
    data_names = []
    data_anos = []
    data_mfccs = []
    data_file =[]
    data_vad =[]
    for subdir, dirs, files in os.walk(Config.noised_dir):
        # iterate over all wavs in the subdirectories
        soundfiles = AudioProcessing.filtr(files, 'wav')
        for file in soundfiles:
            filedata=[]
            # Calculate MFCCS for file
            frame_mfccs = AudioProcessing.process_speech(subdir+'/'+file, Config.winlen, Config.winstep)
            # Append all data to array with filename as identifier
            name = subdir.split('/')[-1]
            emotion = Config.emotionDict.get(name)
            if not isinstance( emotion, ( int, long ) ):
                emotion=emotion.value
            name= str(emotion)
            name="".join(name)
            ano_file=subdir.replace(Config.noised_dir,Config.speech_dir)+'/'+file.replace('.wav','.ano')
            annotation = []
            with open(ano_file, 'r') as f:
                annotation = f.readlines()
            annotation = map(int, annotation)
            annotation_mean=[]
            annotation_mean_sub=[]
            for el in annotation:
                annotation_mean_sub.append(el)
                if len(annotation_mean_sub)==Config.winlen*16000:
                    annotation_mean.append(math.ceil(np.average(annotation_mean_sub)))
                    annotation_mean_sub=annotation_mean_sub[int(Config.winstep*16000):]
            annotation_mean.append(math.ceil(np.average(annotation_mean_sub)))

            for i in range(len(frame_mfccs)):
                data_mfccs.append(frame_mfccs[i,1:])
                data_names.append(int(name))
                data_vad.append(annotation_mean[i])
                data_file.append(file)
                temparray=[int(name)]+np.ndarray.tolist(frame_mfccs[i,1:])+[emotion]
                filedata.append(temparray)


            features_output_file = Config.working_dir+'/'+type + 'Features/' + name +'.csv'
            bob.io.save(np.asmatrix(filedata), features_output_file, create_directories=True)


    logging.info('Finished copying speech annotations!')

    a = np.asmatrix(data_mfccs)
    mean_mfcc = a.mean(axis=0)  # mfccs means
    mean_matrix = np.repeat(mean_mfcc[:, np.newaxis], len(data_mfccs), 0)
    data_mfccs -= mean_matrix  # substract the means
    # save MFCCs
    np.savez(Config.working_dir + Config.cleanData, data_names=data_names, data_files=data_file, data_vad=data_vad, data_mfccs=data_mfccs)

    # data =np.hstack((np.asarray(data_names).reshape(len(data_names),1),data_mfccs,np.asarray(data_anos).reshape(len(data_anos),1)))
    # features_output_file = Config.working_dir+'/'+type + 'Features/' + 'allFiles' +'.csv'
    # bob.io.save(data, features_output_file, create_directories=True)

    logging.info('Saved speech MFCCS!')

def createChanneledLearningDataset():
    logging.info('')
    logging.info('')
    logging.info('Starting extraction of speech MFCCS & adaption of annotations...')

    type='channeled'
    data_names = []
    data_anos = []
    data_mfccs = []
    data_file =[]
    data_vad =[]
    for subdir, dirs, files in os.walk(Config.convolute_dir):
        # iterate over all wavs in the subdirectories
        soundfiles = AudioProcessing.filtr(files, 'wav')
        for file in soundfiles:
            filedata=[]
            # Calculate MFCCS for file
            frame_mfccs = AudioProcessing.process_speech(subdir+'/'+file, Config.winlen, Config.winstep)
            # Append all data to array with filename as identifier
            name = subdir.split('/')[-1]
            emotion = Config.emotionDict.get(name)
            if not isinstance( emotion, ( int, long ) ):
                emotion=emotion.value
            name= str(emotion)
            name="".join(name)
            ano_file=subdir.replace(Config.convolute_dir,Config.speech_dir)+'/'+file.replace('.wav','.ano')
            annotation = []
            with open(ano_file, 'r') as f:
                annotation = f.readlines()
            annotation = map(int, annotation)
            annotation_mean=[]
            annotation_mean_sub=[]
            for el in annotation:
                annotation_mean_sub.append(el)
                if len(annotation_mean_sub)==Config.winlen*16000:
                    annotation_mean.append(math.ceil(np.average(annotation_mean_sub)))
                    annotation_mean_sub=annotation_mean_sub[int(Config.winstep*16000):]
            annotation_mean.append(math.ceil(np.average(annotation_mean_sub)))


            for i in range(len(frame_mfccs)):
                data_mfccs.append(frame_mfccs[i,1:])
                data_names.append(int(name))
                data_vad.append(annotation_mean[i])
                data_file.append(file)
                temparray=[int(name)]+np.ndarray.tolist(frame_mfccs[i,1:])+[emotion]
                filedata.append(temparray)

            # speakers[Sprechername][Dateiname] = {'anos': [0,1,...], 'mfccs': [[...],[...],...]}

            features_output_file = Config.working_dir+'/'+type + 'Features/' + name +'.csv'
            bob.io.save(np.asmatrix(filedata), features_output_file, create_directories=True)


    logging.info('Finished copying speech annotations!')

    a = np.asmatrix(data_mfccs)
    mean_mfcc = a.mean(axis=0)  # mfccs means
    mean_matrix = np.repeat(mean_mfcc[:, np.newaxis], len(data_mfccs), 0)
    data_mfccs -= mean_matrix  # substract the means
    # save MFCCs
    np.savez(Config.working_dir + Config.channeledData, data_names=data_names, data_files=data_file, data_vad=data_vad, data_mfccs=data_mfccs)

    # data =np.hstack((np.asarray(data_names).reshape(len(data_names),1),data_mfccs,np.asarray(data_anos).reshape(len(data_anos),1)))
    # features_output_file = Config.working_dir+'/'+type + 'Features/' + 'allFiles' +'.csv'
    # bob.io.save(data, features_output_file, create_directories=True)

    logging.info('Saved speech MFCCS!')



def readClean():
    data_load = np.load(Config.working_dir + Config.cleanData+'.npz')
    speakers = data_load['data_names']
    files = data_load['data_files']
    data_vad = data_load['data_vad']
    samples = data_load['data_mfccs']
    return speakers,files,samples, data_vad

def readChanneled():
    data_load = np.load(Config.working_dir + Config.channeledData+'.npz')
    speakers = data_load['data_names']
    files = data_load['data_files']
    data_vad = data_load['data_vad']
    samples = data_load['data_mfccs']
    return speakers,files,samples, data_vad

