import SpeakerData
import Configuration
import UBMData
import numpy as np
from Configuration import Config
from sklearn.ensemble import RandomForestClassifier
import cPickle
import BerlinData
import iVector2


type='ssh'
set="Berlin"

Configuration.setConfiguration(type,'UBM') # local or ssh
[ubm_speakers,ubm_files,ubm_samples, ubm_data_vad]=UBMData.readData()

Configuration.setConfiguration(type,set) # local or ssh, Berlin, Fau or Semaine
[channelednames, channeledsamples,channeledannos]=BerlinData.readChanneled()
if set=="Semaine":
    channeledannos= channeledannos[:,2]


samples,speakers,files = iVector2.mix_data_by_file(channeledsamples,channeledannos,channelednames)
trainsamples,trainanonos,testsamples,testanonos,testfiles=iVector2.split_train_test(samples,speakers,files,int(0.75*len(samples)))

try:
    with open(set+'.pkl', 'rb') as f:
        clf = cPickle.load(f)
    predicted_categorys = clf.predict(testsamples[:,:Config.num_features])
    np.savez('predictedSpeakers', data_vad=predicted_categorys)
except:
    clf = RandomForestClassifier(n_estimators=20)
    clf.fit(trainsamples[:,:Config.num_features], trainanonos[:])
    with open(set+'.pkl', 'wb') as f:
        cPickle.dump(clf, f)
    predicted_categorys = clf.predict(testsamples[:,:Config.num_features])
    np.savez('predictedSpeakers', data_vad=predicted_categorys)


true=0
for i in range(len(testanonos)):
    true+=(predicted_categorys[i]==testanonos[i])
print float(true)/len(testanonos)