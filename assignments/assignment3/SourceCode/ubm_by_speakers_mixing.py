import scipy.io.wavfile
import scipy.signal
import os
import glob
import math
import random
import numpy as np
from joblib import Parallel, delayed
import audiotools
import time


impRespPath = '/mnt/naboo/impulse_responses'
noisePath = '/mnt/naboo/noise/noise_equal_concat/train'
pocketMovementWav = '/mnt/naboo/noise/noise_equal_concat/trainPocketMovement.wav'
rootPathCleanCorpus = '/mnt/naboo/vad_speaker_recog/TIMIT_Buckeye/ubm'
replay_gain_ref_path = '/mnt/tatooine/data/ref_pink_16k.wav'

outputPath1 = '/mnt/naboo/schulze/mixed_corpora/e10/mixed_ubm_by_speakers_no_htc_no_pos4'


#pobability of pocket movement
probOfPocketMov = 0.2
#pobabiliy of noise being added up to the clean file
probOfNoise = 0.8

numJobs = 20

wantedFs = 16000
#maximum of the SNR in db (snrMaxDecimal = 10)
snrMax = 20
#minimum of the SNR in db (snrMinDecimal = 0.79433)
snrMin = -2

irPhoneIndex = 3
irTestPosIndex = 3

# LG:  1 -> 0, 0
# S3:  2 -> 1, 1
# S1:  3 -> 2, 2
# HTC: 4 -> 3, 3

###
# End of config
###

noises = []
pocketNoise = []
impResps = []

def cacheNoiseFiles():
	wavFileList = glob.glob(os.path.join(noisePath, '*.wav'))
	for wavFile in wavFileList:
		(fs, samples) = scipy.io.wavfile.read(wavFile)
		noises.append(samples)
		print 'Noise file %s read.' % (wavFile)
	
	(_,pocketNoise) = scipy.io.wavfile.read(pocketMovementWav)
	print 'Noise cached in memory.'


def cacheImpulseResponses():
	phones = ['lg', 's3', 's1', 'htc']
	positions = ['oben', 'unten', 'tisch', 'tasche', 'hose1', 'hose2', 'table', 'jacke']
	
	for phone in phones:
		irSignals = []
		for position in positions:
			fileName = 'ir_' + phone + '_' + position + '.wav'
			(_, samples) = scipy.io.wavfile.read(os.path.join(impRespPath, fileName))
			irSignals.append(samples)
		impResps.append(irSignals)
	print 'IRs cached in memory.'


def getRandomFadedNoise(nSamples):
	while(True):
		index = random.randint(0, len(noises)-1)
		if len(noises[index] >= nSamples):
			#this noise file is long enough, use it
			break
	
	noiseSignal = noises[index]
	
	#get random start point
	rangePotStartPoints = len(noiseSignal) - nSamples
	startInd = math.ceil(random.random() * rangePotStartPoints)
	noiseSegment = noiseSignal[startInd:startInd+nSamples]
	
	#fade to avoid artifacts
	fadeLength = min(len(noiseSegment), 2000)/2
	noiseSegment[:fadeLength] *= np.linspace(0, 1, num=fadeLength)
	noiseSegment[-fadeLength:] *= np.linspace(1, 0, num=fadeLength)
	
	return noiseSegment


def getRandomFadedPocketNoise(nSamples):
	
	#get random start point
	rangePotStartPoints = len(pocketNoise) - nSamples
	if (rangePotStartPoints < 0):
		return np.zeros(nSamples)
	
	startInd = math.ceil(random.random() * rangePotStartPoints)
	noiseSegment = pocketNoise[startInd:startInd+nSamples]

	#fade to avoid artifacts
	fadeLength = min(len(noiseSegment), 2000)/2
	noiseSegment[:fadeLength] *= np.linspace(0, 1, num=fadeLength)
	noiseSegment[-fadeLength:] *= np.linspace(1, 0, num=fadeLength)

	return noiseSegment


def mixFilesInSpeakerPath(folder):  
	speakerPath = os.path.join(rootPathCleanCorpus, folder)
	wavFileList = glob.glob(os.path.join(speakerPath, '*.wav'))
	
	print 'Starting speaker %s...' % (folder)
	
	for (ind,wavFile) in enumerate(wavFileList):
		(fs, samples) = scipy.io.wavfile.read(wavFile)
		samples = samples.astype(np.float64)/65536.0
		#print 'Speech snippet %s read.' % (wavFile)
		
		#get replaygain stats of current file
		file_rplgain = list(audiotools.calculate_replay_gain([ \
			audiotools.open(wavFile) \
			]))[0][1]
		
		#calculate gain to ref file and normalize accordingly
		gain = file_rplgain - ref_rplgain
		normSignal = samples * (10**(gain/20.0))
		
		if (random.random() < probOfNoise):
			#mix with noise of same size
			noise = getRandomFadedNoise(len(normSignal))
			#calculate the random SNR
			randomSNR = snrMin + (snrMax-snrMin) * random.random()
			#amplify signal by reducing noise
			noise /= 10**(randomSNR/20) #normSignal *= 10**(randomSNR/20);
			normSignal += noise
		
		if (random.random() < probOfPocketMov):
			#mix with noise of same size
			noise = getRandomFadedPocketNoise(len(normSignal))
			#calculate the random SNR
			randomSNR = snrMin + (snrMax-snrMin) * random.random()
			noise /= 10**(randomSNR/20)
			normSignal += noise
		
		randPhone = random.randint(0, len(impResps)-1)
		#exclude target phone from training
		while (randPhone == irPhoneIndex):
			randPhone = random.randint(0, len(impResps)-1)
		
		randPos = random.randint(0, len(impResps[irPhoneIndex])-1)
		#exclude target position from training
		while (randPos == irTestPosIndex):
			randPos = random.randint(0, len(impResps[irPhoneIndex])-1)
		
		irTrain = impResps[randPhone][randPos]
		convolvedSignal1 = scipy.signal.fftconvolve(normSignal, irTrain)[:len(normSignal)]
		
		#(_,wavFileName) = os.path.split(wavFile)
		outputFolder1 = os.path.join(outputPath1, folder)
		
		if not os.path.exists(outputFolder1):
		    os.makedirs(outputFolder1)
		
		outputFile1 = os.path.join(outputFolder1, 'cleanNoiseMix_' + str(ind) + '.wav')
		#print 'Writing %s.' % (outputFile)
		scipy.io.wavfile.write(outputFile1, wantedFs, convolvedSignal1)
	
	print 'Speaker %s done' % (folder)


cacheNoiseFiles()
cacheImpulseResponses()

ref_rplgain = list(audiotools.calculate_replay_gain([ \
	audiotools.open(replay_gain_ref_path) \
	]))[0][1]

all_speaker_names = os.walk(rootPathCleanCorpus).next()[1]
print '%d speakers detected.' % (len(all_speaker_names))
results = Parallel(n_jobs=numJobs)(delayed(mixFilesInSpeakerPath)(speaker) for speaker in all_speaker_names)
print 'All done.'