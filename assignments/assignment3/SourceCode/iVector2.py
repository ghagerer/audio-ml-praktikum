import numpy as np
import bob
import sys
import math
import itertools
from Configuration import Config
from sklearn.cluster import KMeans
import random
from collections import Counter
import AudioProcessing

# UBM and ivec init part

def ubm_cuda(ubm_training_samples, path):
    num_utt = int(round(len(ubm_training_samples)/Config.frames_per_utterance))
    for utt_count in range(0, num_utt):
        #get values from array, normalize, then update array
        data = ubm_training_samples[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        tmp_std = np.std(data, 0)
        data -= np.mean(data, 0)
        data /= tmp_std
        ubm_training_samples[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance] = data
    ubm_training_samples = ubm_training_samples[:num_utt*Config.frames_per_utterance]
    training_samples_ubm = np.float32(ubm_training_samples)
    print 'Starting UBM training on the GPU...'
    print 'k-means init on the CPU...'
    initKMeans = KMeans(n_clusters=Config.num_mixtures_ubm, init='k-means++', n_init=1, max_iter=5, \
        verbose=0, random_state=1, n_jobs=Config.n_jobs_kmeans)
    initKMeans.fit(training_samples_ubm)
    init_mean = initKMeans.cluster_centers_
    #calc covariance matrix for each cluster/mixture
    init_cov = np.zeros((Config.num_mixtures_ubm,Config.num_features,Config.num_features))
    for i in range(Config.num_mixtures_ubm):
        cluster_index = i
        samples_of_cluster = training_samples_ubm[initKMeans.labels_ == cluster_index]
        if len(samples_of_cluster) > 0:
            cov_of_cluster = np.cov(samples_of_cluster.T)
            diag_cov = cov_of_cluster * np.identity(Config.num_features)
        else:
            diag_cov = np.identity(Config.num_features)
        init_cov[i,:,:] = diag_cov
    init_mean_ubm = np.array(init_mean,dtype=np.float32)
    init_cov_ubm =  np.array(init_cov,dtype=np.float32)
    # init_mean_ubm, init_cov_ubm = init_with_kmeans(Config.num_mixtures_ubm, Config.num_features, \
    #     Config.n_jobs_kmeans, training_samples_ubm)
    init_weight_ubm = np.ones(Config.num_mixtures_ubm)
    init_weight_ubm = init_weight_ubm * (1/np.float32(Config.num_mixtures_ubm))
    init_weight_ubm = np.array(init_weight_ubm,dtype=np.float32)
    from gmm_specializer.gmm import GMM
    gmm_fast_ubm = GMM(Config.num_mixtures_ubm, Config.num_features, cvtype=Config.gmm_cvtype, means=init_mean_ubm, \
        covars=init_cov_ubm, weights=init_weight_ubm)
    gmm_fast_ubm.train(training_samples_ubm,max_em_iters=Config.n_iter_em, min_em_iters=Config.n_iter_em)
    print 'UBM training done.'
    ubm = bob.machine.GMMMachine(Config.num_mixtures_ubm, Config.num_features)
    ubm.means = np.array(gmm_fast_ubm.get_all_component_means(), 'float64')
    ubm.weights = np.array(gmm_fast_ubm.get_all_component_weights(), 'float64')
    ubm.variances = np.array(gmm_fast_ubm.get_all_component_full_covariance(), \
        'float64').diagonal(axis1=1,axis2=2)
    ubm.save(bob.io.HDF5File(path, 'w' ))
    print 'UBM saved to disk.'
    return ubm


def init_machines(ubm,proj_train_features):
    print 'Init machines ...'
    proj_train_features_flat = [item for sublist in np.vstack(proj_train_features)[:,1] for item in sublist]
    print 'Init machines ivec ...'
    ivec_machine = bob.machine.IVectorMachine(ubm, Config.ivector_dim)
    ivec_machine.variance_threshold = 1e-5
    ivec_trainer = bob.trainer.IVectorTrainer(update_sigma=True, max_iterations=10)
    ivec_trainer.initialize(ivec_machine, proj_train_features_flat)
    ivec_trainer.train(ivec_machine, proj_train_features_flat)
    ivec_machine.save(bob.io.HDF5File(Config.model_dir+Config.set+'ivec_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    ivectors_matrix  = []
    for gmmstats in proj_train_features_flat:
        projected_ivector = ivec_machine.forward(gmmstats)
        ivectors_matrix.append(projected_ivector)
    ivectors_matrix = np.vstack(ivectors_matrix)
    print 'Init machines whitening ...'
    whitening_machine = bob.machine.LinearMachine(ivectors_matrix.shape[1],ivectors_matrix.shape[1])
    t = bob.trainer.WhiteningTrainer()
    t.train(whitening_machine, ivectors_matrix)
    whitening_machine.save(bob.io.HDF5File(Config.model_dir+Config.set+'white_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    whitened_ivectors = []
    for category, proj_train_features_speaker in proj_train_features:
        speaker_ivectors = []
        for gmmstats in proj_train_features_speaker:
            projected_ivector = ivec_machine.forward(gmmstats)
            whitened_ivector = whitening_machine.forward(projected_ivector)
            normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
            speaker_ivectors.append(normalized_ivector)
        whitened_ivectors.append([category, np.vstack(speaker_ivectors)])
    proj_train_features = whitened_ivectors
    print 'Init machines lda ...'
    t = bob.trainer.FisherLDATrainer(strip_to_rank=False)
    lda_machine, __eig_vals = t.train([x[1] for x in proj_train_features])
    lda_machine.resize(lda_machine.shape[0], Config.lda_subspace_dim)
    lda_machine.save(bob.io.HDF5File(Config.model_dir+Config.set+'lda_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    lda_projected_ivectors = []
    for category, whitened_ivectors_speaker in proj_train_features:
        speaker_lda_ivectors = []
        for ivector in whitened_ivectors_speaker:
            lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
            lda_machine(ivector, lda_ivector)
            speaker_lda_ivectors.append(lda_ivector)
        lda_projected_ivectors.append([category,np.vstack(speaker_lda_ivectors)])
    proj_train_features = lda_projected_ivectors
    print 'Init machines wccn ...'
    t = bob.trainer.WCCNTrainer()
    wccn_machine = t.train([x[1] for x in proj_train_features])
    wccn_machine.save(bob.io.HDF5File(Config.model_dir+Config.set+'wccn_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    return ivec_machine, whitening_machine, lda_machine, wccn_machine


# Training ivec part

def train_by_category_win(ubm, cleansamples,cleanannos,cleanvad):
    print 'Train by category ...'
    proj_train_features = []
    for category in set(cleanannos):
        print 'Train by category no:'+str(category)
        speaker_samples = cleansamples[cleanannos==category]
        proj_train_features_speaker = []
        num_utt = int(round(len(speaker_samples)/Config.frames_per_utterance))
        for utt_count in range(0, num_utt):
            data = speaker_samples[utt_count*Config.frames_per_utterance \
                : (utt_count+1)*Config.frames_per_utterance]
            data_ano = cleanvad[utt_count*Config.frames_per_utterance \
                : (utt_count+1)*Config.frames_per_utterance]
            #only process window if more than half the frames are voiced
            if float(sum(data_ano))/len(data_ano) >= 0.5:
                #CMVN:
                tmp_std = np.std(data[data_ano==1], 0)
                data -= np.mean(data[data_ano==1], 0)
                data /= tmp_std
            proj_data = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
            ubm.acc_statistics(data, proj_data)
            proj_train_features_speaker.append(proj_data)
        proj_train_features.append([category,proj_train_features_speaker])
    return proj_train_features

def train_by_category(ubm, cleansamples,cleanannos):
    print 'Train by category ...'
    proj_train_features = []
    for category in set(cleanannos):
        print 'Train by category no:'+str(category)
        speaker_samples = cleansamples[cleanannos==category]
        tmp_std = np.std(speaker_samples, 0)
        speaker_samples -= np.mean(speaker_samples, 0)
        speaker_samples /= tmp_std
        proj_data = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
        ubm.acc_statistics(speaker_samples, proj_data)
        proj_train_features.append([category,proj_data])
    return proj_train_features


def get_stats_from_adapted(emogmms, cleansamples,cleanannos):
    print 'Stats by gmm ...'
    proj_train_features = []
    for category, gmm in emogmms:
        print 'Stats by gmm no:'+str(category)
        speaker_samples = cleansamples[cleanannos==category]
        tmp_std = np.std(speaker_samples, 0)
        speaker_samples -= np.mean(speaker_samples, 0)
        speaker_samples /= tmp_std
        proj_data = bob.machine.GMMStats(gmm.dim_c, gmm.dim_d)
        gmm.acc_statistics(speaker_samples, proj_data)
        proj_train_features.append([category,proj_data])
    return proj_train_features

def ivecs_from_stats_win(emotion_stats,ivec_machine, whitening_machine):
    print 'Whitened Ivec from stats ...'
    whitened_ivectors = []
    for category, proj_train_features_speaker in emotion_stats:
        speaker_ivectors = []
        for gmmstats in proj_train_features_speaker:
            projected_ivector = ivec_machine.forward(gmmstats)
            whitened_ivector = whitening_machine.forward(projected_ivector)
            normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
            speaker_ivectors.append(normalized_ivector)
        whitened_ivectors.append([category,np.vstack(speaker_ivectors)])
    return whitened_ivectors

def ldas_from_ivecs_win(emotion_ivecs,lda_machine):
    print 'LDA from ivec ...'
    lda_projected_ivectors = []
    for category,whitened_ivectors_speaker in emotion_ivecs:
        speaker_lda_ivectors = []
        for ivector in whitened_ivectors_speaker:
            lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
            lda_machine(ivector, lda_ivector)
            speaker_lda_ivectors.append(lda_ivector)
        lda_projected_ivectors.append([category,np.vstack(speaker_lda_ivectors)])
    return lda_projected_ivectors

def wccn_from_lda_win(emotion_ivecs,wccn_machine):
    print 'WCCN from lda ...'
    wccn_lda_ivectors = []
    for category,lda_ivector in emotion_ivecs:
        speaker_wccn_ivectors = []
        for ivector in lda_ivector:
            wccn_lda_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
            wccn_machine(ivector, wccn_lda_ivector)
            speaker_wccn_ivectors.append(wccn_lda_ivector)
        wccn_lda_ivectors.append([category,np.vstack(speaker_wccn_ivectors)])
    return wccn_lda_ivectors

def average_by_emotion_win(emotion_wccns,name):
    print 'Average WCCN ...'
    average_ivecs= []
    wccns=[]
    categories=[]
    for category,speaker_ivecs in emotion_wccns:
        average_ivecs.append([category,np.mean(speaker_ivecs,0)])
        wccns.append(np.mean(speaker_ivecs,0))
        categories.append(category)
    np.savez(Config.model_dir+Config.set+name,wccns=wccns,categories=categories)
    return average_ivecs

def save_average_by_emotion(emotion_wccns,name):
    print 'Average WCCN ...'
    average_ivecs= []
    wccns=[]
    categories=[]
    for category,speaker_ivecs in emotion_wccns:
        wccns.append(speaker_ivecs)
        categories.append(category)
    np.savez(Config.model_dir+Config.set+name,wccns=wccns,categories=categories)
    return average_ivecs

def read_wccns(name):
    data_load = np.load(Config.model_dir +Config.set+ name+'.npz')
    categories=data_load['categories']
    data= data_load["wccns"]
    models=[]
    for i in range(len(categories)):
        models.append([categories[i],data[i]])
    return models

def ivecs_from_stats(emotion_stats,ivec_machine, whitening_machine):
    print 'Whitened Ivec from stats ...'
    whitened_ivectors = []
    for category, proj_train_features_speaker in emotion_stats:
        projected_ivector = ivec_machine.forward(proj_train_features_speaker)
        whitened_ivector = whitening_machine.forward(projected_ivector)
        normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
        whitened_ivectors.append([category,normalized_ivector])
    return whitened_ivectors

def ldas_from_ivecs(emotion_ivecs,lda_machine):
    print 'LDA from ivec ...'
    lda_projected_ivectors = []
    for category,whitened_ivectors_speaker in emotion_ivecs:
        lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
        lda_machine(whitened_ivectors_speaker, lda_ivector)
        lda_projected_ivectors.append([category,lda_ivector])
    return lda_projected_ivectors

def wccn_from_lda(emotion_ivecs,wccn_machine):
    print 'WCCN from lda ...'
    wccn_lda_ivectors = []
    for category,lda_ivector in emotion_ivecs:
        wccn_lda_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
        wccn_machine(lda_ivector, wccn_lda_ivector)
        wccn_lda_ivectors.append([category,wccn_lda_ivector])
    return wccn_lda_ivectors

# Preparation part

def split_train_test(samples, speakers, files,endtrain):
    trainsamples=samples[:endtrain,:Config.num_features]
    trainanonos=speakers[:endtrain]
    testsamples=samples[endtrain+1:,:Config.num_features]
    testanonos=speakers[endtrain+1:]
    testfiles =files[endtrain+1:]
    return trainsamples,trainanonos,testsamples,testanonos,testfiles

def split_test(names,samples,anos,speakers,vad,cuts,idx):
    testnames = names[cuts[idx]:cuts[idx+1]]
    testsamples = samples[cuts[idx]:cuts[idx+1],:Config.num_features]
    testanos = anos[cuts[idx]:cuts[idx+1]]
    testspeakers = speakers[cuts[idx]:cuts[idx+1]]
    test_vad = vad[cuts[idx]:cuts[idx+1]]
    return testnames,testsamples,testanos,testspeakers, test_vad

def split_train(names,samples,anos,speakers,vad,cuts,idx):
    if idx==0:
        trainnames = names[cuts[idx+1]:]
        trainsamples = samples[cuts[idx+1]:,:Config.num_features]
        trainanos = anos[cuts[idx+1]:]
        trainspeakers = speakers[cuts[idx+1]:]
        train_vad = vad[cuts[idx+1]:]
    elif idx==1 or idx==2:
        trainnames = np.concatenate((names[:cuts[idx]],names[cuts[idx+1]:]))
        trainsamples = np.concatenate((samples[:cuts[idx],:Config.num_features],samples[cuts[idx+1]:,:Config.num_features]))
        trainanos = np.concatenate((anos[:cuts[idx]],anos[cuts[idx+1]:]))
        trainspeakers = np.concatenate((speakers[:cuts[idx]],speakers[cuts[idx+1]:]))
        train_vad = np.concatenate((vad[:cuts[idx]],vad[cuts[idx+1]:]))
    elif idx==3:
        trainnames = names[:cuts[idx]]
        trainsamples = samples[:cuts[idx],:Config.num_features]
        trainanos = anos[:cuts[idx]]
        trainspeakers = speakers[:cuts[idx]]
        train_vad = vad[:cuts[idx]]
    else:
        raise Exception('Wrong cross-validation index passed (not within [0,3])')
    return trainnames,trainsamples,trainanos,trainspeakers, train_vad

def mix_data_by_file(files, samples, emos, speakers):
    shufflelist = []
    for file in set(files):
        shufflelist.append([samples[files==file],np.vstack(emos[files==file]),np.vstack(files[files==file]),np.vstack(speakers[files==file])])
    random.shuffle(shufflelist)
    samples = np.vstack([x[0] for x in shufflelist])
    emos = np.array(map(float,np.vstack([x[1] for x in shufflelist])))
    files = np.array([y[0] for y in np.vstack(x[2] for x in shufflelist)])
    speakers = np.array([y[0] for y in np.vstack(x[3] for x in shufflelist)])
    return samples,emos,files,speakers

def mixed_windows_for_test(testsamples,testanos, testfiles,test_vad):
    print 'Mix test windows ...'
    shufflelist = []
    for file in set(testfiles):
        shufflelist.append([testsamples[testfiles==file],np.vstack(testanos[testfiles==file]),np.vstack(testfiles[testfiles==file])])
    random.shuffle(shufflelist)
    testsamples= np.vstack([x[0] for x in shufflelist])
    testanos= np.array(map(float,np.vstack([x[1] for x in shufflelist])))
    test_sample_windows=[]
    test_label_windows = []
    num_utt = int(round(len(testsamples)/Config.frames_per_utterance))
    for utt_count in range(0, num_utt):
        data = testsamples[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        data_ano = test_vad[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        data_emo = testanos[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        #only process window if more than half the frames are voiced
        if float(sum(data_ano))/len(data_ano) >= 0.5:
            #CMVN:
            tmp_std = np.std(data[data_ano==1], 0)
            data -= np.mean(data[data_ano==1], 0)
            data /= tmp_std
            data_emo_vad =data_emo[data_ano==1]
            test_sample_windows.append(data)
            test_label_windows.append(data_emo_vad)
    return test_sample_windows, test_label_windows


# WCCN ivec Part

def wccn_from_testsamples(testsamples,ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine):
    print 'WCCN from single window ...'
    gmmstats = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
    ubm.acc_statistics(testsamples, gmmstats)
    projected_ivector = ivec_machine.forward(gmmstats)
    whitened_ivector = whitening_machine.forward(projected_ivector)
    normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
    lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
    lda_machine(normalized_ivector, lda_ivector)
    wccn_lda_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
    wccn_machine(lda_ivector, wccn_lda_ivector)
    return wccn_lda_ivector

def wccn_from_impostor(testsamples,ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine):
    print 'WCCN from single window ...'
    gmmstats = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
    ubm.acc_statistics(testsamples, gmmstats)
    projected_ivector = ivec_machine.forward(gmmstats)
    whitened_ivector = whitening_machine.forward(projected_ivector)
    normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
    lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
    lda_machine(normalized_ivector, lda_ivector)
    wccn_lda_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
    wccn_machine(lda_ivector, wccn_lda_ivector)
    np.savez(Config.model_dir+Config.set+'single_impostor_wccn',single_impostor_wccn=wccn_lda_ivector)
    return wccn_lda_ivector

def read_impostor_wccn():
    data=np.load(Config.model_dir+Config.set+'single_impostor_wccn'+'.npz')
    return data['single_impostor_wccn']


def test_wccn_on_models_against_impostor(test_wccn, test_label, impostor_model,average_training_data_by_emotion):
    print 'Test wccn against impostor ...'
    fp=0
    tp=0
    fn=0
    tn=0
    for i in range(len(average_training_data_by_emotion)):
        score = abs(cosine_distance(average_training_data_by_emotion[i][1], test_wccn))
        impostor_score = abs(cosine_distance(impostor_model, test_wccn))
        real_speaker = Counter(test_label).most_common(1)[0][0]
        print average_training_data_by_emotion[i][0], real_speaker
        if score>impostor_score:
            if average_training_data_by_emotion[i][0]==real_speaker:
                tp+=1
            else:
                fp+=1
        if score<=impostor_score:
            if average_training_data_by_emotion[i][0]==real_speaker:
                fn+=1
            else:
                tn+=1
    recall=float(tp)/(tp+fn)
    return np.asmatrix([[tp,fp],[fn,tn]]), recall

def test_wccn_against_all_models_cos (test_wccn, test_label,models_by_emotion):
    print 'Test wccn against all ...'
    fp=0
    tp=0
    fn=0
    tn=0
    scores=[]
    real_speaker = Counter(test_label).most_common(1)[0][0]
    for category, model in models_by_emotion:
        scores.append([category,abs(cosine_distance(model, test_wccn))])
    scores =np.array(scores)
    max_sc= np.amax(scores, axis=0)[1]
    calculated_speaker = scores[scores[:,1]==max_sc][0][0]
    print max_sc, calculated_speaker, real_speaker
    for cat,sco in scores:
        if sco==max_sc:
            if cat==real_speaker:
                tp+=1
            else:
                fp+=1
        if sco<max_sc:
            if category==real_speaker:
                fn+=1
            else:
                tn+=1
    if (tp+fn)>0:
        recall=float(tp)/(tp+fn)
    else:
        recall =0
    return (calculated_speaker==real_speaker), np.asmatrix([[tp,fp],[fn,tn]]), recall

def cosine_distance(a, b):
    if len(a) != len(b):
        raise ValueError, "a and b must be same length"
    numerator = sum(tup[0] * tup[1] for tup in itertools.izip(a,b))
    denoma = sum(avalue ** 2 for avalue in a)
    denomb = sum(bvalue ** 2 for bvalue in b)
    result = numerator / (math.sqrt(denoma)*math.sqrt(denomb))
    return result

def test_wccn_against_all_models_zno (test_wccn, test_label,models_by_emotion,impostor_ivectors ):
    print 'Test wccn against all ...'
    fp=0
    tp=0
    fn=0
    tn=0
    scores=[]
    real_speaker = Counter(test_label).most_common(1)[0][0]
    for category, model in models_by_emotion:
        score = cosine_distance(model, test_wccn)
        (z_impostor_mean, z_impostor_stdv) = get_impostor_distribution(impostor_ivectors,model)
        score_norm = (score-z_impostor_mean)/z_impostor_stdv
        scores.append([category,score_norm])# abs(cosine_distance(model, test_wccn))])
    scores =np.array(scores)
    max_sc= np.amax(scores, axis=0)[1]
    calculated_speaker = scores[scores[:,1]==max_sc][0][0]
    print max_sc, calculated_speaker, real_speaker
    for cat,sco in scores:
        if sco==max_sc:
            if cat==real_speaker:
                tp+=1
            else:
                fp+=1
        if sco<max_sc:
            if category==real_speaker:
                fn+=1
            else:
                tn+=1
    if (tp+fn)>0:
        recall=float(tp)/(tp+fn)
    else:
        recall =0
    return (calculated_speaker==real_speaker), np.asmatrix([[tp,fp],[fn,tn]]), recall

def test_wccn_against_all_models_sno (test_wccn, test_label,models_by_emotion,impostor_ivectors ):
    print 'Test wccn against all ...'
    fp=0
    tp=0
    fn=0
    tn=0
    scores=[]
    real_speaker = Counter(test_label).most_common(1)[0][0]
    for category, model in models_by_emotion:
        score = cosine_distance(model, test_wccn)
        (z_impostor_mean, z_impostor_stdv) = get_impostor_distribution(impostor_ivectors,model)
        (t_impostor_mean, t_impostor_stdv) = get_impostor_distribution(impostor_ivectors, test_wccn)
        score_norm = (score-z_impostor_mean)/z_impostor_stdv + (score-t_impostor_mean)/t_impostor_stdv
        scores.append([category,score_norm])# abs(cosine_distance(model, test_wccn))])
    scores =np.array(scores)
    max_sc= np.amax(scores, axis=0)[1]
    calculated_speaker = scores[scores[:,1]==max_sc][0][0]
    print max_sc, calculated_speaker, real_speaker
    for cat,sco in scores:
        if sco==max_sc:
            if cat==real_speaker:
                tp+=1
            else:
                fp+=1
        if sco<max_sc:
            if category==real_speaker:
                fn+=1
            else:
                tn+=1
    if (tp+fn)>0:
        recall=float(tp)/(tp+fn)
    else:
        recall =0
    return (calculated_speaker==real_speaker), np.asmatrix([[tp,fp],[fn,tn]]), recall

# Get a distribution of how impostors score against a given model. This serves for z-normalization.
def get_impostor_distribution(impostor_ivectors, speaker_wccn_lda_ivector):
    scores_impostors = []
    for category, wccn_lda_ivector in impostor_ivectors:
        score = cosine_distance(wccn_lda_ivector, speaker_wccn_lda_ivector)
        scores_impostors.append(score)
    impostor_mean = np.mean(scores_impostors)
    impostor_stdv = np.std(scores_impostors)
    return (impostor_mean, impostor_stdv)


# GMM MAP Part

def train_emotion_gmms(ubm, trainsamples,trainannos,train_vad,setname):
    cleansamples =[]
    cleanannos = []
    num_utt = int(round(len(trainsamples)/Config.frames_per_utterance))
    for utt_count in range(0, num_utt):
        #get values from array, normalize, then update array
        data = trainsamples[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        data_ano = train_vad[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        data_emo = trainannos[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        if float(sum(data_ano))/len(data_ano) >= 0.5:
            #CMVN:
            tmp_std = np.std(data[data_ano==1], 0)
            data -= np.mean(data[data_ano==1], 0)
            data /= tmp_std
            data_emo_vad =data_emo[data_ano==1]
            cleansamples.extend(data)
            cleanannos.extend(data_emo_vad)
    cleansamples = np.float64(cleansamples)
    emotion_models =[]
    for i in set(cleanannos):
        data= cleansamples[cleanannos == i]
        if len(data)>0:
            relevance_factor = 4.
            trainer = bob.trainer.MAP_GMMTrainer(relevance_factor, True, False, False)
            trainer.convergence_threshold = 1e-5
            trainer.max_iterations = Config.n_iter_em
            trainer.set_prior_gmm(ubm)
            gmmAdapted = bob.machine.GMMMachine(Config.num_mixtures_ubm,Config.num_features) # Create a new machine for the MAP estimate
            trainer.train(gmmAdapted, data)
            gmmAdapted.save(bob.io.HDF5File(Config.model_dir+str(float(i)) +setname+'gmm.hdf5', 'w' ))
            emotion_models.append([i,gmmAdapted])
            print gmmAdapted
    return emotion_models


def read_emotion_gmms(path,setname):
    emotion_models =[]
    for name, emotion in Config.emotionCodes.__members__.items():
        emo=emotion
        if not isinstance( emo, ( int, long ) ):
            emo=emotion.value
        emotion_models.append([emo,bob.machine.GMMMachine(bob.io.HDF5File(path+str(float(emo))+setname+'gmm.hdf5', 'r'))])
    return emotion_models


def test_gmm_on_models_against_impostor(testsamples, test_label, impostor_model, gmm_models):
    print 'Test gmm against impostor ...'
    fp=0
    tp=0
    fn=0
    tn=0
    for category, model in gmm_models:
        impostor_score=0
        score=0
        for i in range(len(testsamples)):
            impostor_score = impostor_score+ impostor_model.log_likelihood(testsamples[i])/len(testsamples)
            score = score+ model.log_likelihood(testsamples[i])/len(testsamples)
        real_speaker = Counter(test_label).most_common(1)[0][0]
        if isinstance( real_speaker, ( float, long ) ):
                    real_speaker=int(real_speaker)
        if score>impostor_score:
            if category==real_speaker:
                tp+=1
            else:
                fp+=1
        if score<=impostor_score:
            if category==real_speaker:
                fn+=1
            else:
                tn+=1
    if (tp+fn)>0:
        recall=float(tp)/(tp+fn)
    else:
        recall =0
    return np.asmatrix([[tp,fp],[fn,tn]]), recall


def test_gmm_against_all_models (testsamples, test_label,gmm_models):
    print 'Test gmm against all models ...'
    fp=0
    tp=0
    fn=0
    tn=0
    scores=[]
    real_speaker = Counter(test_label).most_common(1)[0][0]
    for category, model in gmm_models:
        score=0
        for i in range(len(testsamples)):
            score = score+ model.log_likelihood(testsamples[i])/len(testsamples)
        scores.append([category,score])
    scores =np.array(scores)
    max_sc= np.amax(scores, axis=0)[1]
    calculated_speaker = scores[scores[:,1]==max_sc][0][0]
    print max_sc, calculated_speaker, real_speaker
    for cat,sco in scores:
        if sco==max_sc:
            if cat==real_speaker:
                tp+=1
            else:
                fp+=1
        if sco<max_sc:
            if category==real_speaker:
                fn+=1
            else:
                tn+=1
    if (tp+fn)>0:
        recall=float(tp)/(tp+fn)
    else:
        recall =0
    return (calculated_speaker==real_speaker), np.asmatrix([[tp,fp],[fn,tn]]), recall


def testLive(path):
    print 'Cut test windows ...'
    frame_mfccs = AudioProcessing.process_speech(path, Config.winlen, Config.winstep)
    frame_mfccs =frame_mfccs[:,:Config.num_features]
    annos=[]
    files=[]
    for i in range(len(frame_mfccs)):
        annos.append('1.0')
        files.append(path)
    test_sample_windows=[]
    test_label_windows = []
    num_utt = int(round(len(frame_mfccs)/Config.frames_per_utterance))
    for utt_count in range(0, num_utt):
        data = frame_mfccs[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        data_ano = annos[utt_count*Config.frames_per_utterance \
            : (utt_count+1)*Config.frames_per_utterance]
        #CMVN:
        tmp_std = np.std(data, 0)
        data -= np.mean(data, 0)
        data /= tmp_std
        test_sample_windows.append(data)
        test_label_windows.append(data_ano)
    return test_sample_windows, test_label_windows


def balance_data(samples, emo_labels, files, speakers):
    """ Reduce data so that there are equal many samples for each emotion.
    Otherwise the scoring of the classifiers will also be unbalanced."""

    # get the number of samples to which all emotions have to be reduced
    # this is the number of samples of the smallest emotion (minimum)
    limit = sys.maxint
    for emo in set(emo_labels):
        quantity_per_emo = len(emo==emo_labels)
        if quantity_per_emo < limit:
            limit = quantity_per_emo

    # now reduce
    res_samples = np.array([])
    res_emo_labels = np.array([])
    res_files = np.array([])
    res_speakers = np.array([])
    for emo in set(emo_labels):
        sample_counter = 0
        idx = (emo==emo_labels)
        emo_files = files[idx]
        emo_samples = samples[idx]
        for f in set(emo_files):
            idx = (f==files)
            if (sample_counter+len(idx)) < limit:
                res_samples += samples[idx]
                res_files += files[idx]
                res_speakers += speakers[idx]
                res_emo_labels += emo_labels[idx]
                sample_counter += len(idx)

    return samples, emo_labels, files, res_speakers


def group_by_speakers(filenames, samples, emos, speakers,vad):
    res_filenames = []
    res_samples = []
    res_emos = []
    res_speakers = []
    res_vad=[]

    for s in set(speakers):
        idx = (s==speakers)
        res_filenames += list(filenames[idx])
        res_samples += list(samples[idx])
        res_emos += list(emos[idx])
        res_speakers += list(speakers[idx])
        res_vad +=list(vad[idx])

    res_filenames = np.array(res_filenames)
    res_samples = np.array(res_samples)
    res_emos = np.array(res_emos)
    res_speakers = np.array(res_speakers)
    res_vad = np.array(res_vad)

    return res_filenames,res_samples,res_emos,res_speakers, res_vad


def cv_cuts(filenames, samples, emos, speakers, vad):
    cuts = []
    n = float(len(samples))
    i = 0
    j = 0
    for s in set(speakers):
        k = sum(s==speakers)
        i += k
        j += k
        if i/n > 0.25:
            cuts += [j]
            i = 0
    return np.array([0]+cuts+[len(samples)])

#Anchor Models

def get_anchors_from_gmms_with_wccn(emo_gmms,cleansamples,cleanannos,cleanvad,name):
    print 'Train anchor by category ...'
    proj_train_features = []
    for category in set(cleanannos):
        speaker_samples = cleansamples[cleanannos==category]
        proj_train_features_speaker = []
        num_utt = int(round(len(speaker_samples)/Config.frames_per_utterance))
        for utt_count in range(0, num_utt):
            data = speaker_samples[utt_count*Config.frames_per_utterance \
                : (utt_count+1)*Config.frames_per_utterance]
            data_ano = cleanvad[utt_count*Config.frames_per_utterance \
                : (utt_count+1)*Config.frames_per_utterance]
            if float(sum(data_ano))/len(data_ano) >= 0.5:
                #CMVN:
                tmp_std = np.std(data[data_ano==1], 0)
                data -= np.mean(data[data_ano==1], 0)
                data /= tmp_std
                evc=[]
                for cat, gmm in emo_gmms:
                    score =0
                    for i in range(len(data)):
                        score = score+ gmm.log_likelihood(data[i])/len(data)
                    evc.append(score)
                proj_train_features_speaker.append(evc)
        proj_train_features.append([category,np.vstack(proj_train_features_speaker)])
    t = bob.trainer.WCCNTrainer()
    wccn_machine = t.train([x[1] for x in proj_train_features])
    wccns= []
    for category,evcs in proj_train_features:
        cat_wccns=[]
        for evec in evcs:
            wccn_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
            wccn_machine(evec, wccn_ivector)
            cat_wccns.append(wccn_ivector)
        wccns.append([category,np.vstack(cat_wccns)])
    average_ivecs=[]
    anchor_wccns=[]
    categories=[]
    for category,speaker_ivecs in wccns:
        average_ivecs.append([category,np.mean(speaker_ivecs,0)])
        anchor_wccns.append(np.mean(speaker_ivecs,0))
        categories.append(category)
    np.savez(Config.model_dir+Config.set+'anchor'+name,wccns=anchor_wccns,categories=categories)
    wccn_machine.save(bob.io.HDF5File(Config.model_dir+Config.set+'wccn_anchor'+name+'.hdf5', 'w' ))
    return average_ivecs, wccn_machine




def test_anchor_models(testsamples, testlabels, emogmms,anchors,wccn_machine):
    print 'Test anchor against all models ...'
    test_evc=[]
    for category, model in emogmms:
        score=0
        for i in range(len(testsamples)):
                score = score+ model.log_likelihood(testsamples[i])/len(testsamples)
        test_evc.append(score)
    test_wccn = np.ndarray(wccn_machine.shape[1], np.float64)
    wccn_machine(test_evc,test_wccn)
    min= sys.maxint
    for category,anchor in anchors:
        # TODO Cosine Distance instead of euclidiean
        dist =np.linalg.norm(test_wccn-anchor)
        if dist<min:
            min=dist
            result= category
    real_speaker = Counter(testlabels).most_common(1)[0][0]
    print result, real_speaker
    return result==real_speaker



def get_anchors_from_ivecs_with_wccn(emo_models,emotion_wccns,name):
    print 'Train anchor by category ...'
    proj_train_features = []
    for category, trainwccns in emotion_wccns:
        proj_train_features_speaker=[]
        for train_win in trainwccns:
            evc=[]
            for cat, model in emo_models:
                score = abs(cosine_distance(model, train_win))
                evc.append(score)
            proj_train_features_speaker.append(evc)
        proj_train_features.append([category,np.vstack(proj_train_features_speaker)])
    t = bob.trainer.WCCNTrainer()
    wccn_machine = t.train([x[1] for x in proj_train_features])
    wccns= []
    for category,evcs in proj_train_features:
        cat_wccns=[]
        for evec in evcs:
            wccn_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
            wccn_machine(evec, wccn_ivector)
            cat_wccns.append(wccn_ivector)
        wccns.append([category,np.vstack(cat_wccns)])
    average_ivecs=[]
    anchor_wccns=[]
    categories=[]
    for category,speaker_ivecs in wccns:
        average_ivecs.append([category,np.mean(speaker_ivecs,0)])
        anchor_wccns.append(np.mean(speaker_ivecs,0))
        categories.append(category)
    np.savez(Config.model_dir+Config.set+'anchor_ivec'+name,wccns=anchor_wccns,categories=categories)
    wccn_machine.save(bob.io.HDF5File(Config.model_dir+Config.set+'wccn_anchor_ivec'+name+'.hdf5', 'w' ))
    return average_ivecs, wccn_machine


def test_anchor_ivec_models(test_ivec, testlabels, emo_models, anchors,wccn_machine):
    print 'Test anchor against all models ...'
    test_evc=[]
    for category, model in emo_models:
        score = abs(cosine_distance(model, test_ivec))
        test_evc.append(score)
    test_wccn = np.ndarray(wccn_machine.shape[1], np.float64)
    wccn_machine(test_evc,test_wccn)
    min= sys.maxint
    for category,anchor in anchors:
        # TODO Cosine Distance instead of euclidiean
        dist =np.linalg.norm(test_wccn-anchor)
        if dist<min:
            min=dist
            result= category
    real_speaker = Counter(testlabels).most_common(1)[0][0]
    print result, real_speaker
    return result==real_speaker