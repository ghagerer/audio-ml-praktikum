import numpy as np
import bob
import logging
from copy import deepcopy as cp
import AudioProcessing
import MLProcessing
import ChannelProcessing
# from spear import feature_extraction
# from spear.preprocessing import Energy
# from spear.script import det
# from spear.toolchain import ToolChainIvector

# def iVectorSpear(windows):
#     tool =ToolChainIvector();
#     tool.project_ivector_features(windows)
#     projected_ubm = bob.machine.GMMStats(bob.io.HDF5File(str(projected_ubm_file)))
#
#     projected_ivector = tool.project_ivector(feature, projected_ubm)




def iVector(ubm, xtraining, ytraining, test):



    data = np.matrix(ubm, dtype='float64')
    kmeans = bob.machine.KMeansMachine(2, len(ubm[0])) # Create a machine with k=2 clusters with a dimensionality equal to 3
    kmeansTrainer = bob.trainer.KMeansTrainer()
    kmeansTrainer.max_iterations = 200
    kmeansTrainer.convergence_threshold = 1e-5
    kmeansTrainer.train(kmeans, data) # Train the KMeansMachine
    ubm = bob.machine.GMMMachine(2,len(ubm[0])) # Create a machine with 2 Gaussian and feature dimensionality 3
    ubm.means = kmeans.means # Set the means to the one obtained with k-means
    trainer = bob.trainer.ML_GMMTrainer(True, True, True) # update means/variances/weights at each iteration
    trainer.convergence_threshold = 1e-5
    trainer.max_iterations = 200
    trainer.train(ubm, data)
    print ubm
    gs = bob.machine.GMMStats(2,len(test[0]))
    ubm.acc_statistics(test, gs)
    print gs # map adapted resulting supervector


    # dataMAP = np.array(xtraining, dtype='float64')
    # relevance_factor = 4.
    # trainer = bob.trainer.MAP_GMMTrainer(relevance_factor, True, False, False) # mean adaptation only
    # trainer.convergence_threshold = 1e-5
    # trainer.max_iterations = 200
    # trainer.set_prior_gmm(ubm)
    # gmmAdapted = bob.machine.GMMMachine(2,len(xtraining[0])) # Create a new machine for the MAP estimate
    # trainer.train(gmmAdapted, dataMAP)
    # print gmmAdapted


    # m = bob.machine.IVectorMachine(gmmAdapted, 2)
    # # m.t = bob.learn.em. np.array([[1.,2],[4,1],[0,3],[5,8],[7,10],[11,1]])
    # # m.sigma = np.array([1.,2.,1.,3.,2.,4.])
    # m.variance_threshold = 1e-5
    # ivec_trainer = bob.trainer.IVectorTrainer(update_sigma=True, max_iterations=10)
    # # TRAINING_STATS_flatten = [gs11, gs12, gs21, gs22]
    # gs = bob.machine.GMMStats(2,len(xtraining[0]))
    # print 'gs1',gs
    # gmmAdapted.acc_statistics(test, gs)
    # print 'gs2', gs
    # w_ij = m.forward(gs)
    # print w_ij
    # print bob.trainer.IVectorTrainer.compute_likelihood(ivec_trainer, m)

    # score_mat = np.empty((len(xtraining), 1))
    # [model, ubm, num_dimension] = trainPerEmotionClass(xtraining,ytraining,ubm).get(1)
    #
    # #Adapt UBM to utterance and form GSV
    # gmmstats = bob.machine.GMMStats(2, len(xtraining[0]))
    # ubm.acc_statistics(test, gmmstats)
    # #Project GSV into TV space
    # projected_ivector = m.forward(gmmstats)
    # #Whitening & normalization
    # #Transform to get rid of channel effects
    #
    # #Compute score of projected vector against personal model
    # score = model.forward(projected_ivector)
    # score_mat[:, 1] = np.float64(score)
    # y_test_predicted = np.argmax(score_mat, axis=1)
    # return y_test_predicted

def trainPerEmotionClass(X_train, y_train, ubm):
    model_per_class = {}
    num_dimension = X_train.shape[1]
    n_classes = len(np.unique(y_train))
    # trainer_per_class = []
    # for emotion_class in range(n_classes):
    #     trainer_per_class[emotion_class] = deepcopy(trainer)

    for emotion_class in range(0, n_classes):
        x_emotion = X_train[y_train == emotion_class]
        #Train personal model (enrollment)
        plda_base = bob.machine.PLDABase(len(X_train[0]), 100, 200, 1e-5)
        model = bob.machine.PLDAMachine(plda_base)
        speaker_ivector_list = []
        gmmstats = bob.machine.GMMStats(2, num_dimension)
        ubm.acc_statistics(x_emotion, gmmstats)
        ivec_machine = bob.machine.IVectorMachine(ubm, 10)
        ivec_machine.variance_threshold = 1e-5
        ivec_trainer = bob.trainer.IVectorTrainer(update_sigma=True, max_iterations=10)
        proj_train_features = []
        for index, emotion_samples in enumerate(X_train, start=1):
            #create one GMM per impostor emotion.
            proj_train_features_emotion = []
            proj_train_features_emotion = bob.machine.GMMStats(2, num_dimension)
            ubm.acc_statistics(np.float64(emotion_samples), proj_train_features_emotion)
            proj_train_features.append([proj_train_features_emotion])

        proj_train_features_flat = [item for sublist in proj_train_features for item in sublist]
        ivec_trainer.initialize(ivec_machine, proj_train_features_flat)
        ivec_trainer.train(ivec_machine, proj_train_features_flat)
        projected_ivector = ivec_machine.forward(gmmstats)
        # whitened_ivector = whitening_machine.forward(projected_ivector)
        # normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
        # lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
        # lda_machine(normalized_ivector, lda_ivector)
        # wccn_lda_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
        # wccn_machine(lda_ivector, wccn_lda_ivector)
        # speaker_ivector_list.append(wccn_lda_ivector)


        # plda_trainer = bob.trainer.PLDATrainer(200)
        #
        # input_dimension = len(proj_train_features[0])

        # plda_trainer.init_f_method = bob.trainer.PLDATrainer.BETWEEN_SCATTER
        # plda_trainer.init_f_ratio = 1
        # plda_trainer.init_g_method = bob.trainer.PLDATrainer.WITHIN_SCATTER
        # plda_trainer.init_g_ratio = 1
        # plda_trainer.init_sigma_method = bob.trainer.PLDATrainer.VARIANCE_DATA
        # plda_trainer.init_sigma_ratio = 1
        #
        # plda_base = bob.machine.PLDABase(input_dimension, 100, 200, 1e-5)
        # plda_trainer.train(plda_base, proj_train_features)
        # plda_trainer.enrol(model,np.vstack(speaker_ivector_list))

        model_per_class.update({emotion_class: [model, ubm, num_dimension]})

    return model_per_class
