from Configuration import Config
import bob
import numpy as np


def init_machines(ubm,proj_train_features):
    print 'Init machines ...'
    proj_train_features_flat = [item for sublist in np.vstack(proj_train_features)[:,1] for item in sublist]
    print 'Init machines ivec ...'
    ivec_machine = bob.machine.IVectorMachine(ubm, Config.ivector_dim)
    ivec_machine.variance_threshold = 1e-5
    ivec_trainer = bob.trainer.IVectorTrainer(update_sigma=True, max_iterations=10)
    ivec_trainer.initialize(ivec_machine, proj_train_features_flat)
    ivec_trainer.train(ivec_machine, proj_train_features_flat)
    ivec_machine.save(bob.io.HDF5File(Config.model_dir+'ivec_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    ivectors_matrix  = []
    for gmmstats in proj_train_features_flat:
        projected_ivector = ivec_machine.forward(gmmstats)
        ivectors_matrix.append(projected_ivector)
    ivectors_matrix = np.vstack(ivectors_matrix)
    print 'Init machines whitening ...'
    whitening_machine = bob.machine.LinearMachine(ivectors_matrix.shape[1],ivectors_matrix.shape[1])
    t = bob.trainer.WhiteningTrainer()
    t.train(whitening_machine, ivectors_matrix)
    whitening_machine.save(bob.io.HDF5File(Config.model_dir+'white_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    whitened_ivectors = []
    for category, proj_train_features_speaker in proj_train_features:
        speaker_ivectors = []
        for gmmstats in proj_train_features_speaker:
            projected_ivector = ivec_machine.forward(gmmstats)
            whitened_ivector = whitening_machine.forward(projected_ivector)
            normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
            speaker_ivectors.append(normalized_ivector)
        whitened_ivectors.append([category, np.vstack(speaker_ivectors)])
    proj_train_features = whitened_ivectors
    print 'Init machines lda ...'
    t = bob.trainer.FisherLDATrainer(strip_to_rank=False)
    lda_machine, __eig_vals = t.train([x[1] for x in proj_train_features])
    lda_machine.resize(lda_machine.shape[0], Config.lda_subspace_dim)
    lda_machine.save(bob.io.HDF5File(Config.model_dir+'lda_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    lda_projected_ivectors = []
    for category, whitened_ivectors_speaker in proj_train_features:
        speaker_lda_ivectors = []
        for ivector in whitened_ivectors_speaker:
            lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
            lda_machine(ivector, lda_ivector)
            speaker_lda_ivectors.append(lda_ivector)
        lda_projected_ivectors.append([category,np.vstack(speaker_lda_ivectors)])
    proj_train_features = lda_projected_ivectors
    print 'Init machines wccn ...'
    t = bob.trainer.WCCNTrainer()
    wccn_machine = t.train([x[1] for x in proj_train_features])
    wccn_machine.save(bob.io.HDF5File(Config.model_dir+'wccn_'+str(Config.num_mixtures_ubm)+'.hdf5', 'w' ))
    return ivec_machine, whitening_machine, lda_machine, wccn_machine

def train_by_category_win(ubm, cleansamples,cleanannos):
    print 'Train by category ...'
    proj_train_features = []
    for category in set(cleanannos):
        print 'Train by category no:'+str(category)
        speaker_samples = cleansamples[cleanannos==category]
        proj_train_features_speaker = []
        num_utt = int(round(len(speaker_samples)/Config.frames_per_utterance))
        for utt_count in range(0, num_utt):
            data = speaker_samples[utt_count*Config.frames_per_utterance \
                : (utt_count+1)*Config.frames_per_utterance]
            #CMVN:
            tmp_std = np.std(data, 0)
            data -= np.mean(data, 0)
            data /= tmp_std
            proj_data = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
            ubm.acc_statistics(data, proj_data)
            proj_train_features_speaker.append(proj_data)
        proj_train_features.append([category,proj_train_features_speaker])
    return proj_train_features
