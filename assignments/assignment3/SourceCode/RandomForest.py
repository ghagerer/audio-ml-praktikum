import SpeakerData
import Configuration
import UBMData
import numpy as np
from Configuration import Config
from sklearn.ensemble import RandomForestClassifier
import cPickle


type='ssh'


def getVADRF(path, samples, targets):
    print 'Get Random forest for VAD annotation'
    try:
        with open(path, 'rb') as f:
            clf = cPickle.load(f)
    except:
        print 'Calculate Random forest for VAD annotation'
        clf = RandomForestClassifier(n_estimators=20)
        clf.fit(samples[:,:Config.num_features], targets[:])
        with open(path, 'wb') as f:
            cPickle.dump(clf, f)
    return clf

def getPitchRF(path, samples, targets):
    print 'Get Random forest for Pitch annotation'
    try:
        with open(path, 'rb') as f:
            clf = cPickle.load(f)
    except:
        print 'Calculate Random forest for Pitch annotation'
        clf = RandomForestClassifier(n_estimators=20)
        clf.fit(samples[:,:Config.num_features], targets[:])
        with open(path, 'wb') as f:
            cPickle.dump(clf, f)
    return clf