import BerlinData
import SemaineData
import FauData
import SpeakerData
import Configuration
import bob
import UBMData
import VoxforgeData
import numpy as np
from Configuration import Config
import iVector2
import sys
import random
import RandomForest
import logging
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor
import multiprocessing


type='ssh' # or local
name=''
ubm_list=['UBM','voxforge']
set_list= ['Berlin', 'Fau', 'Semaine']#['Berlin', 'Fau', 'Semaine']
algorithm_list = ['iVector_short','ivector_long','gmm_map','anchor','anchor_ivec']#['iVector_short','ivector_long','gmm_map','anchor','anchor_ivec']
norm_list=['euc','cos', 'zno', 'sno', 'cos_kernel']
ubm_retrain=False
data_reprocess=False
set_retrain=True
set_test=True
stats_from_adapted=False
test_live =False
logging_path = Config.code_dir.replace('SourceCode/','') +datetime.now().strftime('%y_%m_%d_%H_%M')+ '_debug.log'
logging.basicConfig(filename=logging_path, level=logging.DEBUG, format='%(asctime)s %(message)s')
logging.info('Start whole process ...')
print 'Start whole process ...'

def train_and_test_CV(channelednames,channeledsamples,channeledannos,channeled_speakers,cuts,index):
    print ""
    print ""
    print "Starting cross-validation run number "+str(index)
    print "--------------------------------------------------------"

    print "Creating test and train data from cuts"
    testnames,testsamples,testanos,testspeakers, test_vad = iVector2.split_test(channelednames,channeledsamples,channeledannos,channeled_speakers,channeled_vad,cuts,index)
    trainnames,trainsamples,trainanos,trainspeakers, train_vad = iVector2.split_train(channelednames,channeledsamples,channeledannos,channeled_speakers,channeled_vad,cuts,index)
    print "Finished creation of test & train data"

    if test_live:
        test_sample_windows, test_label_windows = iVector2.testLive('test.wav')
    else:
        test_sample_windows, test_label_windows = iVector2.mixed_windows_for_test(testsamples,testanos, testnames, test_vad)

    if set_retrain:
        logging.info('Retrain set'+settype+' CVrun'+str(index)+' ...')
        print 'Retrain set'+settype+' ...'
        print len(algorithm_list)
        for j in range(len(algorithm_list)):
            print algorithm_list[j] ,j
            if algorithm_list[j]=='iVector_short':
                print algorithm_list[j] ,j
                emotion_models = iVector2.train_by_category_win(ubm, trainsamples,trainanos, train_vad)
                emotion_ivecs = iVector2.ivecs_from_stats_win(emotion_models,ivec_machine, whitening_machine)
                emotion_ldas = iVector2.ldas_from_ivecs_win(emotion_ivecs,lda_machine)
                emotion_wccns = iVector2.wccn_from_lda_win(emotion_ldas,wccn_machine)
                average_training_data_by_emotion = iVector2.average_by_emotion_win(emotion_wccns, 'short_win_wccn'+str(index)+name)
            elif algorithm_list[j]=='ivector_long':
                print algorithm_list[j] ,j
                if stats_from_adapted:
                    emogmms=iVector2.train_emotion_gmms(ubm, trainsamples,trainanos,train_vad,Config.set+str(index)+name)
                    emotion_models = iVector2.get_stats_from_adapted(emogmms, trainsamples,trainanos)
                else:
                    emotion_models = iVector2.train_by_category(ubm, trainsamples,trainanos)
                emotion_ivecs = iVector2.ivecs_from_stats(emotion_models,ivec_machine, whitening_machine)
                emotion_ldas = iVector2.ldas_from_ivecs(emotion_ivecs,lda_machine)
                emotion_wccns = iVector2.wccn_from_lda(emotion_ldas,wccn_machine)
                average_training_data_by_emotion = iVector2.save_average_by_emotion(emotion_wccns,'long_win_wccn'+str(index)+name)
            elif algorithm_list[j]=='gmm_map':
                print algorithm_list[j] ,j
                emogmms=iVector2.train_emotion_gmms(ubm, trainsamples,trainanos,train_vad,Config.set+str(index)+name)
            elif algorithm_list[j]=='anchor':
                print algorithm_list[j] ,j
                emogmms=iVector2.train_emotion_gmms(ubm, trainsamples,trainanos,train_vad,Config.set+str(index)+name)
                anchors, anchor_wccn_machine = iVector2.get_anchors_from_gmms_with_wccn(emogmms,trainsamples,trainanos,train_vad,str(index)+name)
            elif algorithm_list[j]=='anchor_ivec':
                print algorithm_list[j] ,j
                emotion_models = iVector2.train_by_category_win(ubm, trainsamples,trainanos,train_vad)
                emotion_ivecs = iVector2.ivecs_from_stats_win(emotion_models,ivec_machine, whitening_machine)
                emotion_ldas = iVector2.ldas_from_ivecs_win(emotion_ivecs,lda_machine)
                emotion_wccns = iVector2.wccn_from_lda_win(emotion_ldas,wccn_machine)
                average_training_data_by_emotion = iVector2.average_by_emotion_win(emotion_wccns, 'short_win_wccn'+str(index)+name)
                anchors, anchor_wccn_machine = iVector2.get_anchors_from_ivecs_with_wccn(average_training_data_by_emotion,emotion_wccns,str(index)+name)
            else:
                print 'Wrong algorithm type'
                # break
    else:
        logging.info('Read set'+settype+' CVrun'+str(index)+' ...')
        print 'Read set'+settype+' ...'
        for alg in algorithm_list:
            if alg=='iVector_short':
                    average_training_data_by_emotion= iVector2.read_wccns('short_win_wccn'+str(index)+name)
            elif alg=='ivector_long':
                    average_training_data_by_emotion= iVector2.read_wccns('long_win_wccn'+str(index)+name)
            elif alg=='gmm_map':
                    emogmms = iVector2.read_emotion_gmms(Config.model_dir,Config.set+str(index)+name)
            elif alg=='anchor':
                emogmms = iVector2.read_emotion_gmms(Config.model_dir,Config.set+str(index)+name)
                anchors= iVector2.read_wccns('anchor'+str(index)+name)
                anchor_wccn_machine  = bob.machine.LinearMachine(bob.io.HDF5File(Config.model_dir+Config.set+'wccn_anchor'+str(index)+name+'.hdf5', 'r' ))
            elif alg=='anchor_ivec':
                average_training_data_by_emotion= iVector2.read_wccns('short_win_wccn'+str(index)+name)
                anchors= iVector2.read_wccns('anchor_ivec'+str(index)+name)
                anchor_wccn_machine  = bob.machine.LinearMachine(bob.io.HDF5File(Config.model_dir+Config.set+'wccn_anchor_ivec'+str(index)+name+'.hdf5', 'r' ))
            else:
                print 'Wrong algorithm type'
                # break


    if set_test:
        print 'Test set'+settype+' CVrun'+str(index)+' ...'
        logging.info('Test set'+settype+' CVrun'+str(index)+' ...')
        for alg in algorithm_list:
            if alg=='iVector_short' or alg =='ivector_long':
                confusionmatrix_impost_wccn=np.asmatrix([[0,0],[0,0]])
                confusionmatrix_cos=np.asmatrix([[0,0],[0,0]])
                confusionmatrix_zno=np.asmatrix([[0,0],[0,0]])
                confusionmatrix_sno=np.asmatrix([[0,0],[0,0]])
                uar_wccn_impost=0
                uar_cos=0
                uar_zno=0
                uar_sno=0
                true_rate_cos=0
                true_rate_zno=0
                true_rate_sno=0
                for i in range(len(test_sample_windows)):
                    test_wccn = iVector2.wccn_from_testsamples(test_sample_windows[i],ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine)
                    true_rate_cos_i, confusionmatrix_cos_i,r_cos = iVector2.test_wccn_against_all_models_cos (test_wccn, test_label_windows[i],average_training_data_by_emotion)
                    true_rate_zno_i, confusionmatrix_zno_i,r_zno = iVector2.test_wccn_against_all_models_zno (test_wccn, test_label_windows[i],average_training_data_by_emotion,impostor_ivectors )
                    true_rate_sno_i, confusionmatrix_sno_i,r_sno = iVector2.test_wccn_against_all_models_sno (test_wccn, test_label_windows[i],average_training_data_by_emotion,impostor_ivectors )
                    confusionmatrix_impost_wccn_i,r_wccn_impost_i = iVector2.test_wccn_on_models_against_impostor (test_wccn, test_label_windows[i],impostor,average_training_data_by_emotion)
                    true_rate_cos+=true_rate_cos_i
                    true_rate_zno+=true_rate_zno_i
                    true_rate_sno+=true_rate_sno_i
                    confusionmatrix_impost_wccn+=confusionmatrix_impost_wccn_i
                    confusionmatrix_cos+=confusionmatrix_cos_i
                    confusionmatrix_zno+=confusionmatrix_zno_i
                    confusionmatrix_sno+=confusionmatrix_sno_i
                    uar_wccn_impost= uar_wccn_impost+r_wccn_impost_i/len(test_sample_windows)
                    uar_cos= uar_cos+r_cos/len(test_sample_windows)
                    uar_zno= uar_zno+r_zno/len(test_sample_windows)
                    uar_sno= uar_sno+r_sno/len(test_sample_windows)
                logging.info('Results of  process'+settype+alg+' CVrun'+str(index)+' ...')
                logging.info(settype+' CVrun'+str(index)+' ...'+ 'iVec impost %s %f', confusionmatrix_impost_wccn,uar_wccn_impost)
                logging.info(settype+' CVrun'+str(index)+' ...'+ 'iVec cos %s %f %f',confusionmatrix_cos,uar_cos ,float(true_rate_cos)/len(test_sample_windows))
                logging.info(settype+' CVrun'+str(index)+' ...'+  'iVec zno %s %f %f',confusionmatrix_zno,uar_zno, float(true_rate_zno)/len(test_sample_windows))
                logging.info(settype+' CVrun'+str(index)+' ...'+  'iVec sno %s %f %f',confusionmatrix_sno, uar_sno,float(true_rate_sno)/len(test_sample_windows))
                print 'iVec impost', confusionmatrix_impost_wccn,uar_wccn_impost
                print 'iVec cos',confusionmatrix_cos,uar_cos ,float(true_rate_cos)/len(test_sample_windows)
                print 'iVec zno',confusionmatrix_zno,uar_zno, float(true_rate_zno)/len(test_sample_windows)
                print 'iVec sno',confusionmatrix_sno, uar_sno,float(true_rate_sno)/len(test_sample_windows)
            elif alg=='gmm_map':
                confusionmatrix_gmm=np.asmatrix([[0,0],[0,0]])
                confusionmatrix_impost_gmm=np.asmatrix([[0,0],[0,0]])
                uar_gmm=0
                uar_gmm_impost=0
                true_rate_gmm=0
                for i in range(len(test_sample_windows)):
                    true_rate_i, confusionmatrix_gmm_i,r_gmm =  iVector2.test_gmm_against_all_models (test_sample_windows[i], test_label_windows[i],emogmms)
                    confusionmatrix_impost_gmm_i,r_gmm_impost = iVector2.test_gmm_on_models_against_impostor (test_sample_windows[i], test_label_windows[i],ubm,emogmms)
                    confusionmatrix_impost_gmm+=confusionmatrix_impost_gmm_i
                    confusionmatrix_gmm+=confusionmatrix_gmm_i
                    true_rate_gmm+=true_rate_i
                    uar_gmm= uar_gmm+r_gmm/len(test_sample_windows)
                    uar_gmm_impost= uar_gmm+r_gmm_impost/len(test_sample_windows)
                logging.info('Results of  process'+settype+alg+' CVrun'+str(index)+' ...')
                logging.info(settype+' CVrun'+str(index)+' ...'+  'gmm_impost %s %f',confusionmatrix_impost_gmm,uar_gmm_impost)
                logging.info(settype+' CVrun'+str(index)+' ...'+  'gmm recog %s %f %f', confusionmatrix_gmm, uar_gmm, float(true_rate_gmm)/len(test_sample_windows))
                print 'gmm_impost',confusionmatrix_impost_gmm,uar_gmm_impost
                print 'gmm recog', confusionmatrix_gmm, uar_gmm, float(true_rate_gmm)/len(test_sample_windows)
            elif alg=='anchor':
                true_anchor_rate=0
                for i in range(len(test_sample_windows)):
                    true_anchor_rate_i=iVector2.test_anchor_models(test_sample_windows[i], test_label_windows[i], emogmms,anchors,anchor_wccn_machine)
                    true_anchor_rate +=true_anchor_rate_i
                logging.info('Results of  process'+settype+alg+' CVrun'+str(index)+' ...')
                logging.info(settype+' CVrun'+str(index)+' ...'+  'anchor %f', float(true_anchor_rate)/len(test_sample_windows))
                print 'anchor', float(true_anchor_rate)/len(test_sample_windows)
            elif alg=='anchor_ivec':
                true_anchor_rate=0
                for i in range(len(test_sample_windows)):
                    test_wccn = iVector2.wccn_from_testsamples(test_sample_windows[i],ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine)
                    true_anchor_rate_i=iVector2.test_anchor_ivec_models(test_wccn, test_label_windows[i], average_training_data_by_emotion, anchors,anchor_wccn_machine)
                    true_anchor_rate +=true_anchor_rate_i
                logging.info('Results of  process'+settype+alg+' CVrun'+str(index)+' ...')
                logging.info(settype+' CVrun'+str(index)+' ...'+  'anchor ivec %f', float(true_anchor_rate)/len(test_sample_windows))
                print 'anchor ivec', float(true_anchor_rate)/len(test_sample_windows)
            else:
                print 'Wrong algorithm type'
                # break



for ubmtype in ubm_list:
    if ubmtype=='UBM':
        Configuration.setConfiguration(type,'UBM') # local or ssh
        # UBMData.processAudioDataset()
        # UBMData.createChanneledLearningDataset()
        [ubm_speakers,ubm_files,ubm_samples, ubm_data_vad, ubm_pitch]=UBMData.readData()
        ubm_samples=ubm_samples[:,:Config.num_features]
        clf = RandomForest.getVADRF(Config.model_dir+'VAD',ubm_samples,ubm_data_vad)
        # pitchRF= RandomForest.getPitchRF(Config.model_dir+'pitch',ubm_samples,ubm_pitch)
    elif ubmtype=='voxforge':
        Configuration.setConfiguration(type,'voxforge') # local or ssh
        # VoxforgeData.processAudioDataset()
        # VoxforgeData.createChanneledLearningDataset()
        [ubm_speakers,ubm_files,ubm_samples]=VoxforgeData.readData()
        ubm_samples=ubm_samples[:,:Config.num_features]
        ubm_data_vad = clf.predict(ubm_samples[:,:Config.num_features])
        # ubm_pitch = pitchRF.predict(ubm_samples[:,:Config.num_features])
        if Config.set=='voxforge':
            name='vox'
    else:
        print 'Wrong ubm type'
        break
    if ubm_retrain:
        logging.info('Retrain ubm'+ubmtype+' ...')
        print 'Retrain ubm'+ubmtype+' ...'
        ubm= iVector2.ubm_cuda(ubm_samples, Config.model_dir +Config.set+ str(Config.num_mixtures_ubm) + '.hdf5')
        ubm_models = iVector2.train_by_category_win(ubm, ubm_samples,ubm_speakers)
        ivec_machine, whitening_machine, lda_machine, wccn_machine = iVector2.init_machines(ubm, ubm_models)
        impostor = iVector2.wccn_from_impostor(ubm_samples,ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine)
        ubm_ivecs = iVector2.ivecs_from_stats_win(ubm_models,ivec_machine, whitening_machine)
        ubm_ldas = iVector2.ldas_from_ivecs_win(ubm_ivecs,lda_machine)
        ubm_wccns = iVector2.wccn_from_lda_win(ubm_ldas,wccn_machine)
        impostor_ivectors = iVector2.average_by_emotion_win(ubm_wccns,'impostor_ivectors')
    else:
        logging.info('Read ubm'+ubmtype+' ...')
        print 'Read ubm'+ubmtype+' ...'
        ubm = bob.machine.GMMMachine(bob.io.HDF5File(Config.model_dir +Config.set+ str(Config.num_mixtures_ubm)+ '.hdf5', 'r'))
        ivec_machine = bob.machine.IVectorMachine(bob.io.HDF5File(Config.model_dir+Config.set+'ivec_'+str(Config.num_mixtures_ubm)+'.hdf5', 'r'))
        ivec_machine.ubm=ubm
        whitening_machine  = bob.machine.LinearMachine(bob.io.HDF5File(Config.model_dir+Config.set+'white_'+str(Config.num_mixtures_ubm)+'.hdf5', 'r'))
        lda_machine  = bob.machine.LinearMachine(bob.io.HDF5File(Config.model_dir+Config.set+'lda_'+str(Config.num_mixtures_ubm)+'.hdf5', 'r'))
        wccn_machine  = bob.machine.LinearMachine(bob.io.HDF5File(Config.model_dir+Config.set+'wccn_'+str(Config.num_mixtures_ubm)+'.hdf5', 'r'))
        impostor = iVector2.read_impostor_wccn()
        impostor_ivectors= iVector2.read_wccns('impostor_ivectors')



    for settype in set_list:
        Configuration.setConfiguration(type,settype)
        set_class = BerlinData
        if settype=='Berlin':
            set_class = BerlinData
        elif settype=='Fau':
            set_class=FauData
        elif settype=='Semaine':
            set_class=SemaineData
        else:
            print 'Wrong set type'
            break
        if data_reprocess:
            logging.info('Reprocess set'+settype+' ...')
            print 'Reprocess set'+settype+' ...'
            set_class.processAudioDataset()
            set_class.createCleanLearningDataset()
            set_class.createChanneledLearningDataset()
        else:
            logging.info('Read data'+settype+' ...')
            print 'Read data'+settype+' ...'
            [cleannames, cleansamples,cleanannos, cleanspeakers]=set_class.readClean()
            clean_vad = clf.predict(cleansamples[:,:Config.num_features])
            [channelednames, channeledsamples,channeledannos,channeled_speakers]=set_class.readChanneled()
            channeled_vad = clf.predict(channeledsamples[:,:Config.num_features])
            if settype=='Semaine':
                cleanannos= cleanannos[:,-1]
                channeledannos= channeledannos[:,-1]


        logging.info('Remix set'+settype+' ...')
        print 'Remix set'+settype+' ...'
        # print "Adding randomness"
        #chnldnames,chnldsamples,chnldannos,chnldspeakers = iVector2.mix_data_by_file(chnldnames,chnldsamples,chnldannos,chnldspeakers)
        print "Grouping data by speakers"
        channelednames,channeledsamples,channeledannos,channeled_speakers, channeled_vad = iVector2.group_by_speakers(channelednames,channeledsamples,channeledannos,channeled_speakers, channeled_vad)
        print "Getting 3 cuts for cross-validation"
        cuts = iVector2.cv_cuts(channelednames,channeledsamples,channeledannos,channeled_speakers, channeled_vad)



        for i in [0,1,2,3]:
            train_and_test_CV(channelednames,channeledsamples,channeledannos,channeled_speakers,cuts,i)

        # with ThreadPoolExecutor(multiprocessing.cpu_count()) as executor:
        #     for i in [0,1,2,3]:
        #         executor.submit(train_and_test_CV, channelednames,channeledsamples,channeledannos,channeled_speakers,cuts,i)

        print 'All CV finished'







# Split Data per emotion/ impulse response
# Create 4 75 Train/ 25 Test splits
# Convolve Train and Test according to impulse responses (von naboo)
# Use different speakers for test and training
# Train und Test sets auf Alderaan speichern
# Measure Unweighted Average Recall (UAR)
# Comoute models on ssh and save them. Also machines where python chrashes when reading
# CMN UBM testen ob noetig/ in Code schauen


# SPATER:
# Semaine nach emotion chunks spliten und in wavs specihern je 1 ordner pro emotion
# Dateiname von Semaine in Emotionodnern sollte speaker ids und emotion und session ID
#  Berlin und Fau in Ordner pro eemotion, und aber nciht umbenenne
# Features wie relative lautstaerke und pitch und energy aus mfccs
# Similarity swischen gleichen gmms (z.B. neutral) in berlin und semaine
# Tuerkische Daten mit englischen Modellen
# anchoring


# DONE:
# Integrate Aihams changes
# impulse responses (von naboo)
# Config: ubm path, ubm dimensions,
# Change: Convolve to scikit_fft
# Calculate ubm from timit buckeye gmm dimensions 265-512
# Create MFCCS and split into windows of 256 frames
# Use CMN for each window, save per emotion
# Create one map adapted gmm per emotion (acc_stats)
# Create Projection space for each ivector (with map gmm data)
# Create iVectors for each map adapted gmm (with map gmm data)
# Whiten and normalize ivec
# Create lda projection und vector
# Create wccn projection und vector
# For eacht test window:
# Create map adaption, Projection spaces and vectors
# cosine similarity: zwischen 5 ivecs und test ivec, choose larges similarity
# Windowing auch bei trainngsdaten und CMN short time dazu
# Schritt fuer schritt windows in gmmmap und ivect, dann ivectoren averagen (in code)
# windowing/averaging in einigen files andere nicht