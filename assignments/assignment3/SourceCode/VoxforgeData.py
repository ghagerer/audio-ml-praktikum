import logging
import AudioProcessing
import random
import os
import bob
from Configuration import Config
from concurrent.futures import ThreadPoolExecutor
import multiprocessing
import numpy as np
import math

def processAudioDataset():
    # Configure logging
    logging_path = Config.working_dir + 'debug.log'
    logging.basicConfig(filename=logging_path, level=logging.DEBUG, format='%(asctime)s %(message)s')

    logging.info('Starting normalizing speech files...')
    print('Starting normalizing speech files...')
    for subdir, dirs, files in os.walk(Config.speech_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        for w in wavs:
            speech_file = subdir+'/'+w
            result_file = speech_file.replace(Config.speech_dir, Config.normalize_dir)
            logging.debug('speech_file='+speech_file)
            print('speech_file='+speech_file)
            logging.debug('result_file='+result_file)
            print('result_file='+result_file)
            AudioProcessing.normalize_vs_noise(speech_file, Config.ref_noise, result_file)
    logging.info('Finished normalizing speech files!')
    print('Finished normalizing speech files!')

    logging.info('Starting transformation of 64bit float noise files to 16bit int...')
    print('Starting transformation of 64bit float noise files to 16bit int...')
    # Transform 64bit float noise files to 16bit int
    for subdir, dirs, files in os.walk(Config.noise_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        for w in wavs:
            float_wav_file = subdir+w
            int_wav_file = float_wav_file.replace(Config.noise_dir, Config.noise_int_dir)
            logging.debug('float_wav_file ='+float_wav_file)
            print('float_wav_file ='+float_wav_file)
            logging.debug('int_wav_file='+int_wav_file)
            print('int_wav_file='+int_wav_file)
            AudioProcessing.float2int(float_wav_file, int_wav_file)
    logging.info('Finished transformation of data type!')
    print('Finished transformation of data type!')


    logging.info('')
    print('')
    logging.info('')
    print('')
    logging.info('Starting mixing speech files with random noise...')
    print('Starting mixing speech files with random noise...')
    # Mix speech files with random noise
    for subdir, dirs, files in os.walk(Config.normalize_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        with ThreadPoolExecutor(multiprocessing.cpu_count()) as executor:
            for w in wavs:
                speech_file = subdir+'/'+w
                result_file = speech_file.replace(Config.normalize_dir, Config.noised_dir)
                # pick random noise file, must be 16bit int, 64bit int doesn't work!
                noise_candidates = AudioProcessing.filtr(os.listdir(Config.noise_int_dir),'.wav')
                noise_file = Config.noise_int_dir+random.choice(noise_candidates)
                logging.debug('speech_file='+speech_file)
                print('speech_file='+speech_file)
                logging.debug('noise_file ='+noise_file)
                print('noise_file ='+noise_file)
                logging.debug('result_file='+result_file)
                print('result_file='+result_file)
                executor.submit(AudioProcessing.add_noise,speech_file, noise_file, result_file)
    logging.info('Finished mixing speech files!')
    print('Finished mixing speech files!')

    logging.info('')
    print('')
    logging.info('')
    print('')
    logging.info('Starting convoluting noised speech with random IR...')
    print('Starting convoluting noised speech with random IR...')
    # Convolute noised speech files with random impulse reaction
    for subdir, dirs, files in os.walk(Config.noised_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        # parallelize
        with ThreadPoolExecutor(multiprocessing.cpu_count()) as executor:
            for w in wavs:
                executor.submit(execConvolution, subdir,w)
    logging.info('Finished convoluting speech!')
    print('Finished convoluting speech!')

def execConvolution(subdir, w):
    speech_file = subdir+'/'+w
    # pick random impulse reaction file
    ir_file = Config.ir_dir+random.choice(AudioProcessing.filtr(os.listdir(Config.ir_dir),'.wav'))
    result_file = speech_file.replace(Config.noised_dir, Config.convolute_dir)
    logging.debug('speech_file='+speech_file)
    logging.debug('ir_file    ='+ir_file)
    logging.debug('result_file='+result_file)
    AudioProcessing.convolute_ir(speech_file, ir_file, result_file)


def createChanneledLearningDataset():
    logging.info('')
    logging.info('')
    logging.info('Starting extraction of speech MFCCS & adaption of annotations...')

    type='ubm'
    data_names = []
    data_mfccs = []
    data_file =[]
    ubm_pitch=[]
    for subdir, dirs, files in os.walk(Config.convolute_dir):
        # iterate over all wavs in the subdirectories
        soundfiles = AudioProcessing.filtr(files, 'wav')
        for file in soundfiles:
            # Calculate MFCCS for file
            frame_mfccs = AudioProcessing.process_speech(subdir+'/'+file, Config.winlen, Config.winstep)
            # Append all data to array with filename as identifier
            name = subdir.split('/')[-1]

            for i in range(len(frame_mfccs)):
                data_mfccs.append(frame_mfccs[i,1:])
                data_file.append(file)
                data_names.append(name)

            if len(data_names)>6000000:
                break
        if len(data_names)>6000000:
            break

    logging.info('Finished copying speech annotations!')
    np.savez(Config.working_dir +'vox'+ Config.ubmData, ubm_names=data_names, data_files=data_file, ubm_mfccs=data_mfccs,ubm_pitch=ubm_pitch)



def readData():
    data = np.load(Config.working_dir+ 'vox'+ Config.ubmData+'.npz')
    files = data['data_files']
    speakers = data["ubm_names"]
    samples = data["ubm_mfccs"]
    return speakers,files,samples