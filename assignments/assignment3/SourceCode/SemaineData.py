import logging
from copy import deepcopy as cp
import AudioProcessing
import random
import os
import numpy as np
from scikits import audiolab
import scipy.io.wavfile as wav
import bob
from enum import Enum
from Configuration import Config
from concurrent.futures import ThreadPoolExecutor
import multiprocessing




def processAudioDataset():
    # Configure logging
    logging_path = Config.working_dir + 'debug.log'
    logging.basicConfig(filename=logging_path, level=logging.DEBUG, format='%(asctime)s %(message)s')
    logging.info('Starting normalizing speech files...')
    for subdir, dirs, files in os.walk(Config.speech_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        for w in wavs:
            if w.find('Audio')!=-1:
                speech_file = subdir+'/'+w
                mixfile= Config.working_dir+'wav/'+subdir.split('/')[-1]+'.wav'
                f = audiolab.sndfile(speech_file)
                sig = f.read_frames(f.get_nframes(), dtype=np.float64)
                f.close()  # use contextlib.closing in real code
                (rate,u)= wav.read(speech_file)
                signal = sig[:,0]/4+sig[:,1]/4+sig[:,2]/4+sig[:,3]/4
                f = audiolab.sndfile(mixfile, 'write', audiolab.formatinfo('wav', 'pcm16'), 1, rate)
                f.write_frames(signal)
                f.close()
                speech_file=subdir+'.wav'
                result_file = speech_file.replace(Config.speech_dir, Config.normalize_dir)
                speech_file=mixfile
                logging.debug('speech_file='+speech_file)
                logging.debug('result_file='+result_file)
                AudioProcessing.normalize_vs_noise(speech_file, Config.ref_noise, result_file)
    logging.info('Finished normalizing speech files!')
    logging.info('')
    logging.info('')
    logging.info('Starting transformation of 64bit float noise files to 16bit int...')
    # Transform 64bit float noise files to 16bit int
    for subdir, dirs, files in os.walk(Config.noise_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        for w in wavs:
            float_wav_file = subdir+w
            int_wav_file = float_wav_file.replace(Config.noise_dir, Config.noise_int_dir)
            logging.debug('float_wav_file ='+float_wav_file)
            logging.debug('int_wav_file='+int_wav_file)
            AudioProcessing.float2int(float_wav_file, int_wav_file)
    logging.info('Finished transformation of data type!')

    logging.info('')
    logging.info('')
    logging.info('Starting mixing speech files with random noise...')
    # Mix speech files with random noise
    for subdir, dirs, files in os.walk(Config.normalize_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        for w in wavs:
            speech_file = subdir+'/'+w
            result_file = speech_file.replace(Config.normalize_dir, Config.noised_dir)
            # pick random noise file, must be 16bit int, 64bit int doesn't work!
            noise_candidates = AudioProcessing.filtr(os.listdir(Config.noise_int_dir),'.wav')
            noise_file = Config.noise_int_dir+random.choice(noise_candidates)
            logging.debug('speech_file='+speech_file)
            logging.debug('noise_file ='+noise_file)
            logging.debug('result_file='+result_file)
            AudioProcessing.add_noise(speech_file, noise_file, result_file)
    logging.info('Finished mixing speech files!')

    logging.info('')
    logging.info('')
    logging.info('Starting convoluting noised speech with random IR...')
    # Convolute noised speech files with random impulse reaction
    for subdir, dirs, files in os.walk(Config.noised_dir):
        # iterate over all wavs in the subdirectories
        wavs = AudioProcessing.filtr(files, 'wav')
        # parallelize
        with ThreadPoolExecutor(multiprocessing.cpu_count()) as executor:
            for w in wavs:
                executor.submit(execConvolution, subdir,w)
    logging.info('Finished convoluting speech!')

def execConvolution(subdir, w):
    speech_file = subdir+'/'+w
    result_file = speech_file.replace(Config.noised_dir, Config.convolute_dir)
    # pick random impulse reaction file
    ir_file = Config.ir_dir+random.choice(AudioProcessing.filtr(os.listdir(Config.ir_dir),'.wav'))
    logging.debug('speech_file='+speech_file)
    logging.debug('ir_file    ='+ir_file)
    logging.debug('result_file='+result_file)
    AudioProcessing.convolute_ir(speech_file, ir_file, result_file)

def createCleanLearningDataset():
    logging.info('')
    logging.info('')
    logging.info('Starting extraction of speech MFCCS & adaption of annotations...')

    type='clean'
    data_files = []
    data_emos = []
    data_mfccs = []
    data_speakers=[]
    for subdir, dirs, files in os.walk(Config.noised_dir):
        # iterate over all wavs in the subdirectories
        soundfiles = AudioProcessing.filtr(files, 'wav')
        for file in soundfiles:
            filedata=[]
            # Calculate MFCCS for file
            frame_mfccs = AudioProcessing.process_speech(Config.noised_dir+file, Config.winlen, Config.winstep)
            # Append all data to array with filename as identifier
            speaker=file.replace('.wav', '').split('_')[-1]
            name = file.replace('.wav', '')


            valencefile = AudioProcessing.filtr(os.listdir(Config.speech_dir+name+'/'),'DV.txt')
            anotationsv=[]
            for valence in valencefile:
                anotationsv.append(AudioProcessing.iter_load_txt_col(Config.speech_dir+name+'/'+valence,1)[:len(frame_mfccs)-250])


            arousalfile = AudioProcessing.filtr(os.listdir(Config.speech_dir+name+'/'),'DA.txt')
            anotationsa=[]
            for arousal in arousalfile:
                anotationsa.append(AudioProcessing.iter_load_txt_col(Config.speech_dir+name+'/'+arousal,1)[:len(frame_mfccs)-250])

            if len(valencefile)>1 and len(arousalfile)>1:
                meanvalence= np.divide(np.sum(anotationsv,axis=0),len(valencefile))
                meanarousal= np.divide(np.sum(anotationsa,axis=0),len(arousalfile))
                emotion = getSemaineEmotion(meanvalence,meanarousal)

                for i in range(min(len(frame_mfccs),len(meanarousal),len(meanvalence))):
                    data_emos.append([meanvalence[i],meanarousal[i],emotion[i]])
                    data_mfccs.append(frame_mfccs[i,1:])
                    data_speakers.append(speaker)
                    data_files.append(name)
                    temparray=[int(name)]+np.ndarray.tolist(frame_mfccs[i,1:])+[meanvalence[i]]+[meanarousal[i]]+[emotion[i]]
                    filedata.append(temparray)


    logging.info('Finished copying speech annotations!')
    np.savez(Config.working_dir + Config.cleanData,data_speakers=data_speakers, data_files=data_files, data_emos=data_emos, data_mfccs=data_mfccs)
    logging.info('Saved speech MFCCS!')

def createChanneledLearningDataset():
    logging.info('')
    logging.info('')
    logging.info('Starting extraction of speech MFCCS & adaption of annotations...')

    type='channeled'
    data_files = []
    data_emos = []
    data_mfccs = []
    data_speakers=[]
    for subdir, dirs, files in os.walk(Config.convolute_dir):
        # iterate over all wavs in the subdirectories
        soundfiles = AudioProcessing.filtr(files, 'wav')
        for file in soundfiles:
            filedata=[]
            # Calculate MFCCS for file
            frame_mfccs = AudioProcessing.process_speech(Config.convolute_dir+file, Config.winlen, Config.winstep)
            # Append all data to array with filename as identifier
            speaker=file.replace('.wav', '').split('_')[-1]
            name = file.replace('.wav', '')

            valencefile = AudioProcessing.filtr(os.listdir(Config.speech_dir+name+'/'),'DV.txt')
            anotationsv=[]
            for valence in valencefile:
                anotationsv.append(AudioProcessing.iter_load_txt_col(Config.speech_dir+name+'/'+valence,1)[:len(frame_mfccs)-250])


            arousalfile = AudioProcessing.filtr(os.listdir(Config.speech_dir+name+'/'),'DA.txt')
            anotationsa=[]
            for arousal in arousalfile:
                anotationsa.append(AudioProcessing.iter_load_txt_col(Config.speech_dir+name+'/'+arousal,1)[:len(frame_mfccs)-250])

            if len(valencefile)>1 and len(arousalfile)>1:
                meanvalence= np.divide(np.sum(anotationsv,axis=0),len(valencefile))
                meanarousal= np.divide(np.sum(anotationsa,axis=0),len(arousalfile))
                emotion = getSemaineEmotion(meanvalence,meanarousal)

                for i in range(min(len(frame_mfccs),len(meanarousal),len(meanvalence))):
                    data_emos.append([meanvalence[i],meanarousal[i],emotion[i]])
                    data_mfccs.append(frame_mfccs[i,1:])
                    data_speakers.append(speaker)
                    data_files.append(name)
                    temparray=[int(name)]+np.ndarray.tolist(frame_mfccs[i,1:])+[meanvalence[i]]+[meanarousal[i]]+[emotion[i]]
                    filedata.append(temparray)

    logging.info('Finished copying speech annotations!')

    np.savez(Config.working_dir + Config.channeledData,data_speakers=data_speakers, data_files=data_files, data_emos=data_emos, data_mfccs=data_mfccs)
    logging.info('Saved speech MFCCS!')


def getSemaineEmotion(valence, arousal):
    emotion=[]
    emo=1
    for i in range(min(len(valence),len(arousal))):
        if abs(valence[i])<0.15 and abs(arousal[i])<0.15:
            emo=Config.emotionCodes.Neutral
        else:
            if valence[i]>0:
                if arousal[i]>0:
                    emo=Config.emotionCodes.Happy
                else:
                    emo=Config.emotionCodes.Relaxed
            else:
                if arousal[i]>0:
                    emo=Config.emotionCodes.Angry
                else:
                    emo=Config.emotionCodes.Sad
        if not isinstance( emo, ( int, long ) ):
            emo=emo.value
        emotion.append(emo)
    return emotion



def readClean():
    data_load = np.load(Config.working_dir + Config.cleanData+'.npz')
    data_files = data_load['data_files']
    data_speakers = data_load['data_speakers']
    data_emos = data_load['data_emos']
    samples = data_load['data_mfccs']
    return data_files, samples, data_emos, data_speakers

def readChanneled():
    data_load = np.load(Config.working_dir + Config.channeledData+'.npz')
    data_files = data_load['data_files']
    data_speakers = data_load['data_speakers']
    data_emos = data_load['data_emos']
    samples = data_load['data_mfccs']
    return data_files, samples, data_emos, data_speakers