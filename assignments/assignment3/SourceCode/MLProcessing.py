import numpy as np
from sklearn import cross_validation
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import time
from concurrent.futures import ThreadPoolExecutor
import multiprocessing


def crossval_split(data_dir, folds, max_samples=200000):
    """Get data and train test splits for cross-validation"""

    data_load = np.load(data_dir)
    labels = data_load['data_anos']
    samples = data_load['data_mfccs']

    final_test_data={
        'labels':labels[max_samples:],
        'samples':samples[max_samples:]
    }
    # Perform splits for cross validation
    kf = cross_validation.KFold(len(samples), n_folds=folds,shuffle=True)
    return samples[:max_samples], labels[:max_samples], kf, final_test_data


def apply_ml(kf, samples, labels, method, params=()):
    """Apply cross-validation according to kf (consecutive train test splitting)"""

    kf = list(kf)

    # define function for each step to parallelize it later
    def train_and_test(i):
        train_idx = kf[i][0]
        test_idx = kf[i][1]

        # init classificator, dependend on given method
        clf = {
            'random_forests': lambda: RandomForestClassifier(*params),
            'knn': lambda: KNeighborsClassifier(*params),
            'naive_bayes': lambda: GaussianNB(*params),
            'adaboost': lambda: AdaBoost(*params)
        }[method]()

        # split MFCCs = samples
        X_train, X_test = samples[train_idx], samples[test_idx]
        # split annotations = labels
        y_train, y_test = labels[train_idx], labels[test_idx]

        # measure time for training and classifying
        start = time.time()

        # train classificator
        clf.fit(X_train, y_train)
        train_time = time.time() - start

        # apply trained classificator on test data
        start = time.time()
        y_pred = clf.predict(X_test)
        test_time = time.time() - start

        # get confusion matrix
        cm = confusion_matrix(y_test, y_pred)
        # normalize confusion matrix
        cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        # write to protected shared resource, i.e. results array
        return {
            'accuracy': accuracy_score(y_test, y_pred),
            'confusion_matrix': cm_normalized,
            'train_time': train_time,
            'test_time': test_time,
            'train_samples': len(y_train),
            'test_samples': len(y_test),
            'clf':clf,
        }

    # parallelize
    executor = ThreadPoolExecutor(multiprocessing.cpu_count())
    tuple_mapper = lambda i: (i, train_and_test(i))

    # execute parallelization and save as dictionary
    results = dict(executor.map(tuple_mapper, range(len(kf))))

    return results


def AdaBoost(d=2, n=50):
    return AdaBoostClassifier(DecisionTreeClassifier(max_depth=d), n_estimators=n)

def test_performance(clf,finaldata):
    y_pred = clf.predict(finaldata['samples'])
    a= accuracy_score(finaldata['labels'], y_pred)
    # get confusion matrix
    cm = confusion_matrix(finaldata['labels'], y_pred)
    # normalize confusion matrix
    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    return a, cm_normalized

def getWindows(names, feats, annos):
    return [names[:250],feats[:250],annos[:250]]