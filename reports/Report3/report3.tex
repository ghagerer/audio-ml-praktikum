\documentclass[conference]{IEEEtran}
\ifCLASSINFOpdf
  \usepackage{graphicx,epsfig,epstopdf,float}
  \graphicspath{{../pdf/}{../jpeg/}}
  \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  \usepackage[dvips]{graphicx}
\fi
\usepackage{amssymb,amsmath,array,multirow,algpseudocode}
\hyphenation{op-tical net-works semi-conduc-tor}



% -----------------------------------------------------------------------------------



\begin{document}

\title{Report on Assignment 3: Emotion Recognition}

\author{\IEEEauthorblockN{
Hanna Sch{\"a}fer, Gerhard Hagerer} \IEEEauthorblockA{Technical University Munich\\
schaeferhannaj@gmail.com, ghagerer@posteo.de} }



% -----------------------------------------------------------------------------------



\maketitle
\begin{abstract}
The third assignment for the ML-Audio practical course is about detecting a specific emotion from an unknown speaker in a mixture of speech.  To do this, a speech dataset is created, that contains disturbances like noise and channel effects. The voiced parts are detected with a VAD model. Those parts are grouped into windows and trained on either GMMs, iVectors or anchor models. The full processing of speech and the classification with all approaches will be described in this report.
\end{abstract}
\IEEEpeerreviewmaketitle



% -----------------------------------------------------------------------------------



\section{Introduction}\label{introduction}

In the previous assignment 2 we applied state of the art techniques for speaker detection. Among these were models of universal speech (UBMs) and modifications of these to build speaker dependent models. The scorings of the universal impostor were compared to the speaker specific models, which told us what is more likely. For the present assignment we utilised the same techniques for a different use case: emotion recognition. Instead of building speaker dependent models, we built emotion dependent models from the UBM. Since the task is a recognition task, we also replaced the impostor scoring with a scoring against all emotion models, from which the best scoring candidate is chosen as the predicted emotion. The present work lays out how we applied this, how it worked and which conclusions we draw from it.

In the first section \ref{data} it is described, which dataset we used, followed by the preparation of the dataset in section \ref{ap}. In section \ref{classify} the general techniques used for classification are explained. Afterwards, in section \ref{testing}, it is shown how exactly we arranged cross-validation to get an good estimate about our implementation. In section \ref{results} the results thereof are presented and discussed for each classification process. Finally, in section \ref{conclusion}, the conclusion is outlined and further improvements are indicated.

%\input{Chapters/RelatedWork}



% -----------------------------------------------------------------------------------



\section{Dataset}\label{data}


\subsection{Emotion Data}

To train models for several emotions, we used the following corpora of emotional speech for the present work: Berlin Database of Emotional Speech\cite{berlinemo} (Berlin-EMO), SEMAINE Solid-SAL\cite{semaine} (Semaine) and FAU Aibo Emotion Corpus\cite{fauaibo} (FAU-Aibo). Their individual properties and consequences for the preparation will be discussed in this chapter. 

Berlin-Emo is an corpus consisting of emotionally spoken utterances in German language. 10 phrases with neutral emotional meaning regarding semantics were spoken by 10 speakers (50\% male, 50\% female). They had ages ranging from 21 to 35. Each phrase was spoken by each speaker with seven different emotional affects: anger, boredom, disgust, fear, happiness, sadness and neutral (i.e. no affect at all). For each utterance recording one emotion annotation is available. It should be mentioned, that the emotional utterances were acted. This means they did not happen spontaneously as it would occur in everyday life.

The FAU-Aibo corpus takes more account of spontaneous emotional utterances. Therefore, German speaking children played and interacted with Sony's pet robot Aibo. During this activity their voice was recorded. Afterwards emotionally coloured speech was annotated with several emotion categories by five human labellers. In this way, 9 hours of german speech of 51 children at the age of 10 to 13 years were conducted. For our experiments, we took advantage from utterance-wise annotations according to four emotion labels: angry, emphatic, neutral and motherese.

The Semaine corpus also has a focus on spontaneous verbal communication, but in natural conversational settings. This is the only corpus of those three, that is conducted in English. Each of 21 participants at ages from 22 to 60 years (38\% male) joined conversations with an actor playing several roles or characters. Of the given recordings only 73 sessions were conducted with the full annotations needed for this experiment and are therefor used in our work. The annotations of emotion labels lie at hand as feature vectors, one per 0.02 seconds. These contain among others valence and arousal values, which we mapped to 5 basis emotions, of which 4 correspond to the quadrants of the two dimensional valence arousal space (see figure \ref{fig:valencearousal}) -- describable in clockwise direction as happy/excited, contended/relaxed, sad/depressed, angry/anxious -- with some space at the center that is mapped to a neutral emotion label.

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.4\textwidth]{img/valencearousal.png}
 \caption{Circumplex model \cite{russell} mapping valence \& arousal to emotions}
\label{fig:valencearousal}
\end{center}
\end{figure}



\subsection{UBM}

We trained two UBM reference models reflecting emotionless speech, each based on a different corpus. One is based on Buckeye and TIMIT corpus. The Buckeye \cite{buckeye} corpus consists of 26 h of spontaneous speech from 40 speakers (20 male, 20 female) recorded in informal interview situations. The TIMIT contains broadband recordings of 630 speakers of eight major dialects of American English, each reading ten phonetically rich sentences. The TIMIT \cite{timit}  corpus includes time-aligned orthographic, phonetic and word transcriptions as well as a 16-bit, 16kHz speech waveform file for each utterance. Speech for the VAD test set is taken from the original TIMIT and Buckeye test sets. For each sample the dataset contains a VAD annotation.

For the second UBM we chose the Voxforge as data source. Voxforge is a open source speaker detection dataset, that consists of multiple languages and was originally created for use in Open Source Speech Recognition Engines such as such as ISIP, HTK, Julius and Sphinx. Due to the large amount of data available, we only used 6 million frames of the dataset to build our UBM.


\subsection{Noise}

The noise is given by different noise recordings from eight areas. For Indoor environments, sound from a kitchen or office were used. For outdoor environments street sounds or nature sounds were included. Other noises are bar and cafe sound, footsteps, pocket movement, super markets and public transport. The noise files are longer sequences, from which a small part is selected and used for each sound file.


\subsection{Channels}

In oder to add channel effects, the impulse responses of different cell phones and different positions were recorded. The four positions consist of the phone lying on a desk, inside a leather jacket and inside the pocket in either direction. For the cell phones HTC, LG, S1 and S3 were used.



% -----------------------------------------------------------------------------------



\section{Preprocessing}\label{ap}


\subsection{Audio preparation}

All audio files were mixed with a random noise including fade in and fade out to prevent sudden peaks. Afterwards the noised data was convoluted with an impulse response. For the VAD, which is applied before emotion recognition starts, a model was trained on the data of the universal background model. This model is then used to classify the voiced parts in the speaker dependent speech files. The used classifier was a random forest with 20 components.The accuracy of the predicted VAD in comparison with the TIMIT-Buckeye annotation is 94\%.


\subsection{Cross validation}

We wanted to get a good estimate for the validity of our emotion recognition implementation. Therefore, we performed 4-fold cross validation. Thereby the splits of training and validation data (3/1) where chosen so, that no speaker of the training set would occur in the validation set. We considered this as better, because our classifier should work on speakers which it does not know already. Thus, this aspect should be included in our measurements.

It should not be left unmentioned, that balancing of emotions was left out for training, i.e. there were not equal many features for all emotions provided. This decision was done due to the high complexity in the preparation step and questionable improvements, that would be achieved by this.

For testing a simulation of real life conversation seemed appropriate. So, the validation utterances were mixed and shuffled, while keeping the per file data together. In doing so the test consists of one big conversation, that will be tested for each speaker in turn.


\subsection{Windowing}

To gain more insightful instances for both training and testing a window of frames is used instead of each single frame. One window originally consists of 256 frames, which is around 2,5 seconds. All the non voiced frames are dropped. If the window contains less than 50\% speech, the whole window is dropped. Additionally a short term normalisation is used on every window by subtracting its mean from each frame. This step is supposed to help remove static noise and channel effects. For the testing the windowing results in windows of mixed emotions are being analysed together. In such a case, the majority emotion is selected as the target. For the training the windowing results in multiple small models for each emotion. Those models can be averaged into one model with better precision, than on a training step over the whole set.


% -----------------------------------------------------------------------------------



\section{Classification algorithms}\label{classify}

\subsection{Universal Background Model}
The universal background model is trained as a basis for all other models. It contains the largest amount of speech. Additionally the UBM data is used in two ways if using iVector classification. In order to initialise the spaces used for the iVector classification, all UBM data is mapped per speaker and window and transferred into each space needed for the iVectors. Since the UBM data signifies a general speech environment, its spaces should be sufficient for mapping all later samples. The second usage is as an impostor iVector. Similar to the UBM scoring against the speaker GMM this impostor iVector will be scored against the test iVectors. The UBM is initialised with a kMeans approach to make the calculation faster.

\subsection{GMM emotion detection with MAP adaption}
For the GMM based approach the UBM GMM is MAP adapted to each emotion training set. The resulting emotion GMMs are later scored for each test window. The highest score against all emotion models is supposed to be the correct emotion fitting.

\subsection{iVector based emotion detection}
For the iVector classification several steps are required. First the spaces needed to build an iVector from a set of samples, are initialised using the UBM samples. Those spaces are the Total Variability space, the LDA space and the WCCN space. Additionally, a whitening and normalisation is used on each iVector. After initialising the spaces, the training starts. There are two options for building the iVector model. Either multiple windows are projected step by step and the averaged per emotion, or the complete data for each emotion is projected once into the according spaces. In both cases a super vector is created for the according data, window or emotion set, by acquiring the statistics from the UBM. Then those emotion super vectors are projected into the iVector space, whitened and normalised. The resulting iVectors are projected first into the LDA space and then into the WCCN space. In case of a per window projection, all windows are averaged to build one WCCN vector per emotion. For the case using the complete emotional dataset, this step is not necessary. For testing, each test window is put through the same pipeline as the training data. Then the resulting WCCN test vector is compared with all the emotion WCCN vectors and the highest similarity score defines the recognised emotion.

\subsection{iVector score normalisation}
For comparing a cosine similarity is used. Additionally the score can be normalised in different ways, in order to reduce channel and noise effects. In this experiments the s- and z-norm were used.
The z-norm, or zero normalisation, compensates inter-speaker score variation. For estimating the normalisation statistics, a impostor set (from the UBM) is scored against the target model and the mean and the standard deviation of the scores are computed\\
$IM=impostorScoresOnTargetWCCN$\\
$score(testWCCN, targetWCCN)=$\\ 
$\frac{Loglikelihood(testWCCN, targetWCCN)- \mu (IM)}{\sigma (IM)}$\\
The s-norm, or symmetric normalisation, is reducing within session variability leading to improved performance, better calibration, and more reliable threshold setting. For estimating the normalisation statistics, again a impostor set (from the UBM) is scored against the target model and the mean and the standard deviation of the scores are computed. Additionally, the same procedure is used for scoring the impostor against the testWCCN. The final normalised score is then calculated as follows:\\
$IT=impostorScoresOnTestWCCN$\\
$IM=impostorScoresOnTargetWCCN$\\
$score(testWCCN, targetWCCN)=$\\ 
$\frac{Loglikelihood(testWCCN, targetWCCN)- \mu (IM)}{\sigma (IM)}+$\\
$\frac{Loglikelihood(testWCCN, targetWCCN)- \mu (IT)}{\sigma (IT)}$\\
 
\subsection{Anchor models}
Anchor models \cite{anchor} are an approach to improve the performance of another model. We used both anchor models based on the MAP adapted GMMs and anchor models based on the WCCN projected iVectors. In a anchor model the performance of the training data for each emotion is scored (log likelihood) on all models. This results in a Emotion Characterisation Vector (ECV) that summarises, how a certain emotional dataset behaves against all models. The resulting EVC is additionally projected into the WCCN space for both GMMs and iVectors. We use the whole procedure on each window and then average over all resulting EVCs of one emotion. For testing the test window is scored against all emotion models, resulting in a test EVC, and then compared in similarity to the anchor models of each emotion. For the comparison a euclidian distance is used, but any other similarity measure, like cosine distance, would also work. Again the most similar anchor determines the resulting emotion.


% -----------------------------------------------------------------------------------



\section{Results}\label{results}

We collected the results of each cross validation and classification method separately for TIMIT- and Voxforge-based UBM, see the attached spreadsheet therefore. To make this relatively big collection of data meaningful, the results are presented in a condensed manner here.


\subsection{Comparison of UBMs}

Regarding the selection of the UBM model, we found that utilisation of the TIMIT-based UBM model is better for emotion recognition purposes. This is visible in Table \ref{ubm_comparison}.

\begin{table}[ht!]
    \begin{center}
    \begin{tabular}{cccccc}
        & GMM
        & iVector
        & Anchor GMM
        & Anchor iVector
        & Avg\\
    \hline
        ACC TIMIT
        & 0,24 
        & 0,29 
        & 0,26 
        & 0,28 
        & 0,26\\
        
        ACC Voxforge
        & 0,25 
        & 0,26 
        & 0,23 
        & 0,26 
        & 0,25\\
    \hline
        UAR TIMIT
        & 0,22 
        & 0,26 
        & 0,25 
        & 0,25 
        & 0,20\\
    
        UAR Voxforge
        & 0,23 
        & 0,22 
        & 0,21 
        & 0,23 
        & 0,21\\
    \hline
    \end{tabular}
    \end{center}
    \caption{Accuracies (ACC) and unweighted average recalls, averaged over all data, i.e. Berlin-EMO, FAU and Semaine}
    \label{ubm_comparison}
\end{table}

Apparently, the accuracy averaged over all methods is better for TIMIT- than for Voxforge-based UBM, even though the difference is small. Nonetheless, it is possible to argue that the difference is too small to be significant besides the fact that Voxforges average UAR is slightly greater. Since the differences between both are small, we average over both of them in the next paragraphs. Later on we will see that with TIMIT better results can be achieved. 


\subsection{Comparison of Emotional Corpora}

If we average over all methods and the two UBMs, it is possible to compare for which dataset the accuracies were best. This can be seen in Table \ref{emodata_avg_comparison}.

\begin{table}[ht!]
    \begin{center}
    \begin{tabular}{ccccccc}
        & ACC AVG
        & UAR AVG\\
    \hline
        Berlin
        & 0,19
        & 0,17\\
    \hline
        FAU
        & 0,32
        & 0,27\\
    \hline
        Semaine
        & 0,25
        & 0,24\\
    \hline
    \end{tabular}
    \end{center}
    \caption{Average accuracies regarding emotional datasets; average is built over all methods.}
    \label{emodata_avg_comparison}
\end{table}

The table shows, that the best results could be achieved on the FAU dataset, since the accuracy and also the recall were best there. This is especially reflected, when the best cross validation run (averaged 4-fold) is taken into consideration, which is done in Table \ref{emodata_best_comparison}.

\begin{table}[ht!]
    \begin{center}
    \begin{tabular}{ccccccc}
        & Best ACC
        & Settings
        & Best UAR
        & Settings\\
    \hline
        Berlin
        & 0,26
        & TIMIT iVec
        & 0,28
        & TIMIT iVec\\
    \hline
        FAU
        & 0,46
        & TIMIT Anch GMM
        & 0,43
        & TIMIT Anch GMM\\
    \hline
        Semaine
        & 0,30
        & TIMIT iVec
        & 0,30
        & TIMIT iVec\\
    \hline
    \end{tabular}
    \end{center}
    \caption{Best methods and their accuracies \& recall for each dataset}
    \label{emodata_best_comparison}
\end{table}

The comparatively very high accuracy and recall of FAU's best cv-runs are striking and might be due to it being the largest dataset, or the similarity between speech of children, as opposed to the actors in the Berlin set. Interesting is also, that the TIMIT dataset appears to be more appropriate to achieve exceptionally good results, not only for FAU, but for all emotional corpora. The reason might be the restriction of the dataset to the first 6 million frames, since the whole set is too large to process.

Additionally, more sophisticated methods like iVectors perform seemingly better. This is also confirmed by the average accuracies and recalls from the attached spreadsheet and the following paragraphs.


\subsection{Comparison of Methods}

If we focus only on the comparison of the methods, a view as on Table \ref{methods_comparison} appears.

\begin{table}[ht!]
    \begin{center}
    \begin{tabular}{ccccccc}
        & GMM MapAd
        & iVector
        & Anchor GM MapAd
        & Anchor iVec\\
    \hline
        AVG ACC
        & 0,24
        & 0,27
        & 0,24
        & 0,26\\
    \hline
        AVG UAR
        & 0,23
        & 0,25
        & 0,23
        & 0,25\\
    \hline
    \end{tabular}
    \end{center}
    \caption{Average accuracies regarding classification methods; average is built over all emotional corpora \& UBMs.}
    \label{methods_comparison}
\end{table}

It can be seen that iVector was generally the best technique with respect to accuracy as well as to recall, since it holds the best values. Anchors based on iVectors perform nearly equally well. GMM Map adaption based methods cannot achieve that good accuracies and recall values.


\subsection{Remarks regarding Methods}

It shall not be left unmentioned, that more than the shown classification methods were used. For iVectors for example, several window sizes were applied. Since this resulted in the same numbers, it was no feasible information. Similarly, other norms besides the Cos-Norm for iVectors, namely S-Norm and Z-Norm, were calculated for every single cross validation run -- please refer to the attached spreadcheet for these numbers. They were not added to this document, since they always deviated from Cos-Norm by not more than 2\%.


\subsection{Remarks regarding Voice Activity Detection}

As for the different iVector norms, the data preprocessing with Voice Activity Detection did not vary the results noteworthy. That is why all results shown are only based on VAD-preprocessed data, since a comparison in that regard does not yield new knowledge.


\subsection{A Glance on Confusion Matrices}





% -----------------------------------------------------------------------------------



\section{Conclusion}\label{conclusion}
This report can conclude, that very simple methods for speaker detection already show very promising results. Due to yet unsolved problems, the more advanced methods could not be compared entirely. Overall the iVector model reached only a result of 50\% accuracy and low recall and precision. Although it ran bug free, the result suggests, that even this approach was corrupted in some way by the data.



% -----------------------------------------------------------------------------------



\bibliographystyle{plain}
\bibliography{bib/ref}{}


\end{document}

