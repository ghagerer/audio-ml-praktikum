def test(classifiers,mfccs,emos):
    
    # get scorings from GMMss
    scores = []
    for g in classifiers['gmms']:
        scores += [sum(g.score(mfccs))]
    
    # get best score/emo-pair
    maxidx = scores.index(max(scores))
    
    # predict with respective adaboost
    a = classifiers['adaboosts']
    predicted = a[maxidx].predict(mfccs)
    
    voted_emo = majority_of(predicted)
    real_emo = majority_of(emos)
    
    return real_emo,voted_emo
