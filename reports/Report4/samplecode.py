def calc_classifiers(pair):
  samples = pair[0]['samples']
           +pair[1]['samples']
  emolbls = pair[0]['emos']
           +pair[1]['emos']
  first_stage = GMM(10)
  first_stage.fit(samples)
  second_stage = AdaBoostClassifier()
  second_stage.fit(samples,emolbls)
  return first_stage,second_stage
