\documentclass[conference]{IEEEtran}
\ifCLASSINFOpdf
  \usepackage{graphicx,epsfig,epstopdf,float}
  \graphicspath{{../pdf/}{../jpeg/}}
  \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  \usepackage[dvips]{graphicx}
\fi
\usepackage{amssymb,amsmath,array,multirow,algpseudocode}
\hyphenation{op-tical net-works semi-conduc-tor}
\usepackage{listings}


% -----------------------------------------------------------------------------------



\begin{document}

\title{Report on Assignment 4: 2 Stage Emotion Recognition}

\author{\IEEEauthorblockN{
Hanna Sch{\"a}fer, Gerhard Hagerer} \IEEEauthorblockA{Technical University Munich\\
schaeferhannaj@gmail.com, ghagerer@posteo.de} }



% -----------------------------------------------------------------------------------



\maketitle
\begin{abstract}
The fourth assignment for the ML-Audio practical course is about detecting a specific emotion from an unknown speaker in a mixture of speech.  To do this, a speech dataset is created, that contains disturbances like noise and channel effects. The voiced parts are detected with a VAD model. Those parts are grouped into windows and trained on a two staged classifier. The full processing of speech and the classification will be described in this report.
\end{abstract}
\IEEEpeerreviewmaketitle



% -----------------------------------------------------------------------------------



\section{Introduction}\label{introduction}

In the last exercise we implemented speech- and audio-based emotion detection from recent research. In this exercise we experimented with completely own ideas regarding emotion recognition. Jitin Kumar Baghel could show in his Master's Thesis, that based on pure Gaussian Mixture Models emotional speech classifiers can be implemented, that work well under several constraints. He especially stated that his approach turns out to work very accurate for pre-classification into several groups of emotions, which share many common features, whereas separation of emotions within these groups fails very often with the same approach, since similar emotions get very often confused by the classifier. Based on these findings, we proofed in a previous project that a two stage classifier using GMMs for emotion pairs and AdaBoost for separating these pairs improves the results of Baghel. By that the accuracies on Berlin-EMO got better and applicability on new training and test sets was demonstrated strikingly. Consequently issues like overfitting for Berlin-EMO dataset can be excluded.

Nonetheless, the conducted studies were done under several ideal laboratory conditions. Firstly, the cross-validations used for assessing the quality of our techniques were performed such, that the same speakers appeared in training and validation datasets. As a consequence, the produced classifiers did only work for a speaker if they were trained for him or her, i.e. no speaker invariance. Additionally, the classification did heavily depend on microphone adjustments, since for example changes in the gain of the audio signal did influence the classification dramatically, not to speak about other microphone parameters (channel effects). Also the training did not include background noise making the classifiers unable for use in everyday conditions. To complete the list of challenges we did not face it is necessary to mention our use of technologies like OpenSmile, that to our knowledge are not proven to work in a energy-efficient way, even though processing times of around 10ms per frame on modern laptops seem to make it worth experimenting on Android-based smartphones.

The latter point is important against the background of context sensitive mobile devices, for which invariance to the mentioned conditions is a feasible aim, too. Therefore we experimented with the two stage approach based on MFCCs, to see if it can also perform well under these difficult conditions.

Additionally to the new two stage technique we tried to improve our previous work in its systematic approach by splitting the data into different channels for training and test. The updated results will be shown in a separate chapter.

In the first section \ref{data} it is described, which dataset we used, followed by the preparation of the dataset in section \ref{ap}. In section \ref{update} the updates for previous results are shown. After that in the main section section \ref{classify} the general techniques used for classification are explained. In section \ref{results} the results thereof are presented and discussed for each classification process. Finally, in section \ref{conclusion}, the conclusion is outlined and further improvements are indicated.

%\input{Chapters/RelatedWork}



% -----------------------------------------------------------------------------------



\section{Dataset}\label{data}


\subsection{Emotion Data}

To train models for several emotions, we used the following corpora of emotional speech for the present work: Berlin Database of Emotional Speech\cite{berlinemo} (Berlin-EMO), SEMAINE Solid-SAL\cite{semaine} (Semaine) and FAU Aibo Emotion Corpus\cite{fauaibo} (FAU-Aibo). Their individual properties and consequences for the preparation will be discussed in this chapter. 

Berlin-Emo is an corpus consisting of emotionally spoken utterances in German language. 10 phrases with neutral emotional meaning regarding semantics were spoken by 10 speakers (50\% male, 50\% female). They had ages ranging from 21 to 35. Each phrase was spoken by each speaker with seven different emotional affects: anger, boredom, disgust, fear, happiness, sadness and neutral (i.e. no affect at all). For each utterance recording one emotion annotation is available. It should be mentioned, that the emotional utterances were acted. This means they did not happen spontaneously as it would occur in everyday life.

The FAU-Aibo corpus takes more account of spontaneous emotional utterances. Therefore, German speaking children played and interacted with Sony's pet robot Aibo. During this activity their voice was recorded. Afterwards emotionally coloured speech was annotated with several emotion categories by five human labellers. In this way, 9 hours of german speech of 51 children at the age of 10 to 13 years were conducted. For our experiments, we took advantage from utterance-wise annotations according to four emotion labels: angry, emphatic, neutral and motherese.

The Semaine corpus also has a focus on spontaneous verbal communication, but in natural conversational settings. This is the only corpus of those three, that is conducted in English. Each of 21 participants at ages from 22 to 60 years (38\% male) joined conversations with an actor playing several roles or characters. Of the given recordings only 73 sessions were conducted with the full annotations needed for this experiment and are therefor used in our work. The annotations of emotion labels lie at hand as feature vectors, one per 0.02 seconds. These contain among others valence and arousal values, which we mapped to 5 basis emotions, of which 4 correspond to the quadrants of the two dimensional valence arousal space (see figure \ref{fig:valencearousal}) -- describable in clockwise direction as happy/excited, contended/relaxed, sad/depressed, angry/anxious -- with some space at the center that is mapped to a neutral emotion label.

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.4\textwidth]{img/valencearousal.png}
 \caption{Circumplex model \cite{russell} mapping valence \& arousal to emotions}
\label{fig:valencearousal}
\end{center}
\end{figure}


\subsection{Noise}

The noise is given by different noise recordings from eight areas. For Indoor environments, sound from a kitchen or office were used. For outdoor environments street sounds or nature sounds were included. Other noises are bar and cafe sound, footsteps, pocket movement, super markets and public transport. The noise files are longer sequences, from which a small part is selected and used for each sound file.


\subsection{Channels}

In oder to add channel effects, the impulse responses of different cell phones and different positions were recorded. The four positions consist of the phone lying on a desk, inside a leather jacket and inside the pocket in either direction. For the cell phones HTC, LG, S1 and S3 were used.



% -----------------------------------------------------------------------------------



\section{Preprocessing}\label{ap}


\subsection{Audio preparation}

All audio files were mixed with a random noise including fade in and fade out to prevent sudden peaks. Afterwards the noised data was convoluted with an impulse response. For the VAD, which is applied before emotion recognition starts, a model was trained on the data of the universal background model. This model is then used to classify the voiced parts in the speaker dependent speech files. The used classifier was a random forest with 20 components.The accuracy of the predicted VAD in comparison with the TIMIT-Buckeye annotation is 94\%.


\subsection{Windowing}

To gain more insightful instances for both training and testing a window of frames is used instead of each single frame. One window originally consists of 256 frames, which is around 2,5 seconds. All the non voiced frames are dropped. If the window contains less than 50\% speech, the whole window is dropped. Additionally a short term normalisation is used on every window by subtracting its mean from each frame. This step is supposed to help remove static noise and channel effects. For the testing the windowing results in windows of mixed emotions are being analysed together. In such a case, the majority emotion is selected as the target. For the training the windowing results in multiple small models for each emotion. Those models can be averaged into one model with better precision, than on a training step over the whole set.


\subsection{Balancing of Emotions}

In these experiments we chose to balance the emotions such, that for every training and validation set there are equal many MFCC feature vectors for each emotion. Thus bad classification results cannot originate from imbalanced emotional data.


% -----------------------------------------------------------------------------------
\section{Channel separation update}\label{update}
As an update on the previous work of the third assignment we introduced a splitting of the different channel effects to the classification. Now all tested channels are previously unknown to the models. Additionally we updated some metric to multi-classification and used uniformly the cosine similarity instead of different distance measures.

The averaged results over all cross validation runs are shown in the following table (\ref{channel}):

\begin{table}[h]
    \begin{center}
    \begin{tabular}{llllllll}
        Dataset & GMMs & iVector & Z-Norm & S-Norm & Anchor1 & Anchor2 & Vote\\
         \hline
           &&&&&&&\\

        Berlin    &  15,7 & 21,42 & 24,8	 & 25,12 & 13,92 & 21,68 & 26,10\\
        
        FAU       &  33,05 & 28,95 & 22,93 & 27,26 & 27,07 & 27,21 & 27,07\\
        
        Semaine   & 20,30 & 27,28 & 28,62 & 25,02 & 13,65 & 24,93 & 28,10\\
    \end{tabular}
    \end{center}
    \caption{Accuracies in percentage, averaged over 4-fold cross validation. Recall values are left out, since they were always equal to the accuracies.}
    \label{channel_split}
\end{table}

All algorithms are described in detail in the third assignment. The anchor 1 algorithm is based on GMMS while anchor 2 uses iVectors. The voting is done by choosing the majority between all previously named algorithms.

\section{Two Stage Classification Approach}\label{classify}


Out of the reasons initially mentioned, we implemented a two stage classifier. For the first stage, one GMM for each possible emotion pair was trained. Therefore all existing MFCC feature vectors of the regarding two emotions where taken as a basis for training the respective GMM. To classify an utterance frame, it is scored by all existing emotion pair GMMs. The highest score thereby tells which emotion pair can be assumed to be the most likely one. For each emotion pair, there is also one AdaBoost classifier specifically trained for deciding if the passed feature vector rather belongs to one or the other emotion. In consequence, the GMM with the highest scoring decides which AdaBoost classifier should be applied, which in turn yields the ultimate emotion label.

Below simplified Python example code is showing how the training for each emotion pair is performed. The resulting pair of classifiers is appended to a list, in which for each emotion pair one such two stage classifiers is saved:

\lstinputlisting[language=Python]{samplecode.py}

Afterwards MFCC features and their according emotion labels of several frames per window are passed to a function classifying which emotion it assesses to be most present in the actual window. This is again shown in the following code snippet:

\lstinputlisting[language=Python]{samplecode2.py}

% -----------------------------------------------------------------------------------



\section{Results}\label{results}

As stated in the Preprocessing Chapter, our two stage classification experiments were completely based upon noised and channeled data. Furthermore, since speaker invariance was declared as mandatory, 4-fold cross validation needed to take care of separating speakers, so that the speaker from training sets were not speaking in the validation sets and vice versa. To see to what extend speaker invariance made the results worse, additionally speaker dependent classifiers were conducted. So, the following chapters are divided according to whether the results belong to speaker invariant or speaker dependent classifiers.


\subsection{Speaker dependent classifier}

Table \ref{acc_speaker_dep} shows the accuracies of our two stage GMM AdaBoost classifier compared to random guessing.

\begin{table}[h]
    \begin{center}
    \begin{tabular}{lcccc}
        Dataset   &  64   &  10   &   8   &  Baseline   \\
    \hline
        Berlin    &  17\% &  18\% &  18\% &        14\% \\
        
        FAU       &  25\% &  26\% &  24\% &        25\% \\
        
        Semaine   &  33\% &  37\% &  31\% &        20\% \\
    \end{tabular}
    \end{center}
    \caption{Accuracies, averaged over 4-fold cross validation, compared with accuracy of uniform random guessing, for GMM with 64, 10 \& 8 Gaussians. Recall values are left out, since they were always equal to the accuracies.}
    \label{acc_speaker_dep}
\end{table}

Most of the results are generally a bit better than randomness. This holds especially for Semaine, where the classifier with 10 Gaussians was in 17\% of all cases better than complete randomness. Generally 10 Gaussians delivered the best accuracies compared with all other configurations. Nonetheless, it must be stated that the very promising results from ideal lab conditions could not be reproduced in the same settings regarding speaker dependence, but with noise and channel effects in the audio data.

It has to be noted, that the first cross validation run for each dataset was not included into the average calculation of Table \ref{acc_speaker_dep}. This was done due to consistent misbehaviour of the classifiers in every first cross validation run. Even though we can show, that the features are always perfectly balanced for every split -- i.e. the same amount of features for every emotion --, the classifier classified every window as the same emotion, but, again, only in the first cross validation run. Nonetheless, all other runs showed relatively expected behaviour within the confusion matrices.


\subsection{Speaker independent classifier}

For the following results we separated train and validation splits such, that speakers existent in the training data were not existent in the validation data. Table \ref{acc_speaker_inv} shows the results. Regarding these, only GMMs with 10 Gaussians were taken into consideration, since these were the most promising as seen before. This was in the same way confirmed by prior experiments, too.

\begin{table}[h]
    \begin{center}
    \begin{tabular}{lcc}
        Dataset   &     10   & Baseline \\
    \hline
        Berlin    &     14\% &     14\% \\
        
        FAU       &     34\% &     25\% \\
        
        Semaine   &     17\% &     20\% \\
    \end{tabular}
    \end{center}
    \caption{Same as in Table \ref{acc_speaker_dep}, but speaker invariant. Only 10 Gaussians were used for GMMs, since this is the most promising configuration as seen in Table \ref{acc_speaker_dep}.}
    \label{acc_speaker_inv}
\end{table}

As it can be seen, results on Berlin-EMO and Semaine are very poor, because they are equal or even worse than guessing. In contrast, the FAU results seem promising being even better than the ones from speaker dependent two stage emotion recognition. However, the averaged accuracies from the cross validation runs -- namely 23\%, 33\% and 48\% -- show a relative high variance, which is why these results have to be seen with caution. Unfortunately, we could not make sense of our recall values in that regard, since these were always the same as the accuracies. Even though we tried all possible parameter settings of the respective metrics library (sklearn.metrics.recall\_score), no improvements in that regards could be achieved. This is probably due to handlig a multi-class problem.


\section{Conclusion}\label{conclusion}

A priori we had good reasons to assume, that we had found a good emotion recognition approach working well under ideal lab conditions. Now, after the results were conducted based on noised, channeled and speaker invariant data, the approach turned out to be not applicable under non-ideal conditions. We additionally can state the methods from the last exercise are in most cases better under these difficult circumstances and do outperform the proposed two stage classification.






\bibliographystyle{plain}
\bibliography{bib/ref}{}


\end{document}

